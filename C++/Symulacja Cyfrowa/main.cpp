#include "Generator.h"
#include "siec.h"
#include <iostream>
#include <stdio.h>
#include <limits>
#include <time.h>
#include <stdlib.h>

void Opis_pliku();

extern Zmienne_globalne Dane=
{

	(20*1000*1000),//czas_symulacji= 20s
	true,//kanal_wolny=
	15000, //average = czas w milisekundach! dok�adnie 14,999999. bo:
	//pr�dko�� = 100kB/s, 66pakiet�w /sekund�, 
	//1s/66=15ms.
	16,	//D_max=maskymalny zakres D
	10, //BT_max=maskymalne fifo;
	5, // RT_max=maksyalna ilosc retransmisji
	0, //czas_sys= 1=1us, 1000=1ms, 1 000 000=1s
	0,//g_pierwsze_zdarzenie=
	0,//Stacja* g_wsk_na_pierwsza_stacje;
	false,
	2,//int ilosc_stacji;
	false,//test kolizji
	1,//nr pomairu
	1,//ilosc_powtorzen
	//zerowanie danych do zebrania
	0,//ilosc_traconych_pakietow
	0,//ilosc_teransmitowanych_pakiet�w;
	0,//ilosc_kolizji
	0,//ilsoc_oferowanych_pakietow
	0//ilsoc pakietow wyslanych

};


/////////////////////////////
//Planista
void Planista(Stacja* cel, int czas, int typ,int id )
{
	Zdarzenie *here;
	Zdarzenie *poprzednie;
	Zdarzenie *nowe_zdarzenie = new Zdarzenie(cel, Dane.czas_sys+czas, typ, id);
	//std::cout<<"Planista w akcji\n";
	if(Dane.g_pierwsze_zdarzenie==0)
	{
		Dane.g_pierwsze_zdarzenie=nowe_zdarzenie;
	}
	else{

		//szeregowanie od najmniejszego

		poprzednie = 0;
		here = Dane.g_pierwsze_zdarzenie;


		while(here) 
		{	
			if(here->czas_zdarzenia < nowe_zdarzenie->czas_zdarzenia)
			{
				if(here->nastepne==NULL)
				{
					here->nastepne=nowe_zdarzenie;
					break;
				}
				else
				{	
					poprzednie=here;
					here=here->nastepne;
					continue;
				}
			}
			else
			{
				if(poprzednie==0)
				{

					Dane.g_pierwsze_zdarzenie=nowe_zdarzenie; //aktualizacja pierwszego wskaznika

				}
				else
				{

					poprzednie->nastepne=nowe_zdarzenie;


				}

				nowe_zdarzenie->nastepne=here;
				here=here->nastepne;
				break;			
			}


		};	


	}

};

void Stworz_stacje(void)
{
	srand(time(0));
	unsigned int  tab_jader[200] = {
		198991104,
		233344512,
		1520791296,
		822521472,
		514641152,
		49989696,
		1687766272,
		228251392,
		786705408,
		1014761216,
		35239680,
		538183168,
		2079941888,
		470441472,
		41779008,
		698008576,
		1033456128,
		459571456,
		1095674112,
		514743104,
		622993664,
		1794089728,
		476584960,
		1867744512,
		991010304,
		2134177536,
		1673264000,
		678326528,
		815160320,
		1803707904,
		943772416,
		1247480832,
		1709170304,
		1072293376,
		1244960832,
		499180544,
		1086293504,
		224928128,
		1529459712,
		1049699904,
		1067159616,
		140777984,
		753281024,
		444868096,
		272987904,
		1585820480,
		276372032,
		1416198400,
		1342335744,
		339496960,
		1810407424,
		484566592,
		1587936000,
		431655936,
		1534703104,
		1997447936,
		1165582848,
		1251882496,
		173158144,
		2121160960,
		930906624,
		665926144,
		960836096,
		303317568,
		205840448,
		279263232,
		816765952,
		635141696,
		1393710336,
		749942528,
		407369216,
		59392768,
		1100327680,
		581784576,
		1458278912,
		1318901056,
		1680060416,
		1137708800,
		992386816,
		430318904,
		1307940864,
		1174624512,
		2077791232,
		982680576,
		1828710912,
		1545332736,
		1860477440,
		955738432,
		851905792,
		1961665088,
		1418771968,
		687984128,
		1029356672,
		1398102272,
		596649984,
		951273472,
		1657687168,
		969282048,
		20585472,
		1000593984,
		1037143808,
		301094720,
		1346866752,
		1140391680,
		166019072,
		2142208768,
		1739228672,
		1859708608,
		1982314752,
		1288488448,
		1259557632,
		852271680,
		1592936512,
		2126996024,
		2053380984,
		1729222464,
		538342656,
		1927036928,
		1437371904,
		2120887296,
		1305936640,
		886481920,
		1546146304,
		745462208,
		1202763264,
		1119028992,
		1990196224,
		1195708928,
		1050092032,
		172472576,
		2066506752,
		1616611072,
		1904791360,
		1687478272,
		573193216,
		1724362496,
		403329856,
		2119093760,
		1152968576,
		1689851904,
		391101760,
		421765952,
		325737472,
		841838336,
		1567848704,
		50558464,
		1347716096,
		1020644864,
		1524154112,
		1724152576,
		94165504,
		473254400,
		1409714432,
		1019890432,
		2103940352,
		601913408,
		934364672,
		454965504,
		883890944,
		468448320,
		31030848,
		1303744960,
		14888448,
		26017536,
		204736064,
		1837190912,
		264644608,
		2082174528,
		1897571328,
		1662752768,
		438466368,
		440933632,
		1694970368,
		935271424,
		1037171968,
		146987264,
		1702415616,
		967843136,
		66057216,
		1322546552,
		867257600,
		1500781056,
		1698077440,
		834679296,
		414874368,
		2002142776,
		1550989568,
		945354240,
		1905815552,
		1821613312,
		220479488,
		474290752,
		423444992,
		6690816,
		1919501568,
		2121572864,
		866389504,
		1589832448,
		1633122872,
		168692992
	};



	int liczba_losowa=rand();

	Opis_pliku();
	Dane.g_wsk_na_pierwsza_stacje=new Stacja(tab_jader[0+liczba_losowa%200], 1);
	Stacja* temp=Dane.g_wsk_na_pierwsza_stacje;
	for(int i=2;i<=Dane.ilosc_stacji;i+=1)
	{

		temp->nastepna=new Stacja(tab_jader[(i-1+liczba_losowa)%200], i);
		temp=temp->nastepna;
	}



};

void Metoda_ABC(void)
{

	bool flaga_zdarzenia; //flaga Event_trig
	int czas_zliczania=0;
	while(Dane.g_pierwsze_zdarzenie==0?0:Dane.g_pierwsze_zdarzenie->czas_zdarzenia<(Dane.czas_symulacji))
	{
		//A
		Dane.test_kolizji=false;
		Dane.czas_sys=int (Dane.g_pierwsze_zdarzenie->czas_zdarzenia); //przyjmij czas symulacji jako czas 1 zdarzenia
		//B
		Zdarzenie *ten_sam_czas;
		ten_sam_czas=Dane.g_pierwsze_zdarzenie;
		while(Dane.czas_sys==ten_sam_czas->czas_zdarzenia) //Uruchamiam zdarzenia o tym samym czasie najpierw planuje nastepny pakiet
		{
			if(ten_sam_czas->typ_zdarzenia==ZDA_GEN_PAKIET)	ten_sam_czas->Uruchom();
			ten_sam_czas=ten_sam_czas->nastepne;
		}
		ten_sam_czas=Dane.g_pierwsze_zdarzenie;
		while(Dane.czas_sys==ten_sam_czas->czas_zdarzenia) //Uruchamiam zdarzenia o tym samym czasie potem wykonuje pakeity
		{

			if(ten_sam_czas->typ_zdarzenia==ZDA_WYS_PAKIET)	ten_sam_czas->Uruchom();
			ten_sam_czas=ten_sam_czas->nastepne;
		}	
		//C
		flaga_zdarzenia=false;
		Stacja * sprawdzanie_stacji;
		sprawdzanie_stacji=Dane.g_wsk_na_pierwsza_stacje;
		while(flaga_zdarzenia==false)
		{
			flaga_zdarzenia=true;
			while(!(sprawdzanie_stacji==0))
			{

				if(sprawdzanie_stacji->flaga_gotowosci==false)
				{
					sprawdzanie_stacji->tick(ZDA_WYS_PAKIET);
					flaga_zdarzenia=false;		

				}
				sprawdzanie_stacji=sprawdzanie_stacji->nastepna;
			}

			sprawdzanie_stacji=Dane.g_wsk_na_pierwsza_stacje;


		}
		if(Dane.test_kolizji==true) 
		{


			Dane.ilosc_kolizji++;
		}

		if(czas_zliczania<=Dane.czas_sys)
		{
			czas_zliczania+=20*1000;//20ms
			FILE *stacjio_pakiety_bufor= fopen(  "stacjio_pakiety_bufor.txt", "a");
			Stacja *test_stacja;
			test_stacja=Dane.g_wsk_na_pierwsza_stacje;
			int ilosc_pakietow_pozostalych_w_fifo=0;
			while(test_stacja)
			{

				ilosc_pakietow_pozostalych_w_fifo+=test_stacja->IloSC_w_buforze();

				test_stacja=test_stacja->nastepna;
			}
			fprintf(stacjio_pakiety_bufor,"\nnr_pomiaru\t%d\tIlsoc stacji\t%d\tCzas_symulacji\t%d\tCas_systemowy\t%d\t\tilosc_pominietych pakietow\t%d",Dane.nr_pomiaru,Dane.ilosc_stacji,Dane.czas_symulacji,Dane.czas_sys, ilosc_pakietow_pozostalych_w_fifo);
			fclose(stacjio_pakiety_bufor);
		}




		ten_sam_czas=Dane.g_pierwsze_zdarzenie;
		while(Dane.czas_sys==ten_sam_czas->czas_zdarzenia) //Uruchamiam zdarzenia o tym samym czasie
		{
			ten_sam_czas->Usun_Zarzenie();
			ten_sam_czas=Dane.g_pierwsze_zdarzenie; // po usunieciu kolejen zdarzenie jest pierwsze bo lsita posortowana
		}

	}
};

void intro()
{
	printf( "-----------------------------------------------------------------------------------------------------\n");
	printf( "\nSYMULACJA CYFROWA 2012/2013\t\tAUTOR: Piotr Teresiak Student EiT na Politechnice Poznanskiej\n");
	printf("\nPodaj czas symulacji\t");
	std::cin>>Dane.czas_symulacji;
	printf("\nPodaj ilsoc stacji\t");
	std::cin>>Dane.ilosc_stacji;
	printf("\nPodaj ilsoc powtorzen dla stacji\t");
	std::cin>>Dane.ilosc_powtorzen;
	printf( "\n-----------------------------------------------------------------------------------------------------\n");


};

void Opis_pliku()
{
	FILE *komunikaty = fopen(  "Komunikaty.txt", "a");
	fprintf(komunikaty, "-----------------------------------------------------------------------------------------------------\n");
	fprintf(komunikaty, "\nSYMULACJA CYFROWA 2012/2013\t\tAUTOR: Piotr Teresiak Student EiT na Politechnice Poznanskiej\n");
	fprintf(komunikaty, "Dane Zebrane dla ilosc stacji =\t%d i czas symulacji = \t%d\t\n us",(Dane.ilosc_stacji),(Dane.czas_symulacji));
	fprintf(komunikaty, "\n-----------------------------------------------------------------------------------------------------\n");
	fclose(komunikaty);

	FILE *zdarzenia = fopen( "Zdarzenia.txt", "a");
	fprintf(zdarzenia, "-----------------------------------------------------------------------------------------------------\n");
	fprintf(zdarzenia , "\nSYMULACJA CYFROWA 2012/2013\t\tAUTOR: Piotr Teresiak Student EiT na Politechnice Poznanskiej\n");
	fprintf(zdarzenia, "Dane Zebrane dla ilosc stacji =\t%d i czas symulacji = \t%d\t\n us",(Dane.ilosc_stacji),(Dane.czas_symulacji));
	fprintf(zdarzenia, "\n-----------------------------------------------------------------------------------------------------\n");

	fclose(zdarzenia);
};
void Zapis_zebranych_danych(void)
{
	FILE *Zebrane_dane = fopen( "Zebrane_dane_SYM_CYFROWA_2012_2013_AUTOR_Piotr_Teresiak_Student_EiT_na_Politechnice_Poznanskiej.txt", "a");
	fprintf(Zebrane_dane, "nr_pomiaru\t%d\tIlosc_Stacji\t%d\tCzas_Symulacji\t%d\tIlosc_pakietow_oferowanycht\t%d\tIlsocs_pakietow_wyslanych\t%d\tIlosc_Pakietow_straconcyh\t%d\tIlosc_kolizji\t%d\tIlosc_Retransmisji\t%d\t\n",Dane.nr_pomiaru,(Dane.ilosc_stacji),(Dane.czas_symulacji),Dane.ilosc_oferowanych_pakietow,Dane.ilosc_pakietow_wyslanych,Dane.ilosc_traconych_pakietow,Dane.ilosc_kolizji,Dane.ilosc_retransmitowanych_pakietow);
	fclose(Zebrane_dane);
};

int main()
{
	intro();

	for(Dane.nr_pomiaru=1;Dane.nr_pomiaru<=Dane.ilosc_powtorzen;Dane.nr_pomiaru++)
	{

		//przygotowanie stacji
		Stworz_stacje();
		printf("Start:\n\tilsoc_stacji\t%d\t\tCzas_symulacji\t%d us\n.",Dane.ilosc_stacji,Dane.czas_symulacji);
		// tworzenie nowego zdarznie

		Stacja * wsk_stacja;
		wsk_stacja=Dane.g_wsk_na_pierwsza_stacje;
		int czas_pojawienia_sie_pakietu;
		while(wsk_stacja)
		{

			Planista(wsk_stacja,(czas_pojawienia_sie_pakietu=wsk_stacja->Generator_stacji->Wykladniczny(Dane.average)), ZDA_GEN_PAKIET,wsk_stacja->id);
			Planista(wsk_stacja,(czas_pojawienia_sie_pakietu), ZDA_WYS_PAKIET ,wsk_stacja->id);
			wsk_stacja=wsk_stacja->nastepna;
		};

		Zdarzenie * test;
		test=Dane.g_pierwsze_zdarzenie;

		FILE *zdarzenia = fopen( "Zdarzenia.txt", "a");
		std::cout<<"\n\tSprawdzanie lsity zdarzen\n";

		fprintf(zdarzenia,"\n----------------------------------------------------------------------------\n\nTestowanie listy zdarzen\n\n---------------------------------------------------",test->czas_zdarzenia,test->id_obiektu,test->typ_zdarzenia);

		while(test)
		{
			fprintf(zdarzenia,"\nCzasz zdarzenia\t%d\tID_STACJI\t%d\tTyp zdarzenia\t%d",test->czas_zdarzenia,test->id_obiektu,test->typ_zdarzenia);
			test=test->nastepne;
		}

		fclose(zdarzenia);


		std::cout<<"\n-----------------------------------------------";
		std::cout<<"\nGLowny program\n\n";


		Metoda_ABC();
		Zapis_zebranych_danych();
		FILE *pakiety_fifo = fopen( "sprawdzanie_buforow.txt", "a");
		Stacja *test_stacja;
		test_stacja=Dane.g_wsk_na_pierwsza_stacje;
		int ilosc_pakietow_pozostalych_w_fifo=0;
		while(test_stacja)
		{

			ilosc_pakietow_pozostalych_w_fifo+=test_stacja->IloSC_w_buforze();

			test_stacja=test_stacja->nastepna;
		}
		fprintf(pakiety_fifo,"\nnr_pomiaru\t%d\tIlsoc stacji\t%d\tCzas_symulacji\t%d\t\tilosc_pominietych pakietow\t%d",Dane.nr_pomiaru, Dane.ilosc_stacji,Dane.czas_symulacji, ilosc_pakietow_pozostalych_w_fifo);
		fclose(pakiety_fifo);
		// zerowanie danych przed zmiana ilsoci stacji
		Dane.czas_sys=0;
		Dane.g_pierwsze_zdarzenie=0;
		Dane.g_wsk_na_pierwsza_stacje=0;
		Dane.ilosc_kolizji=0;
		Dane.ilosc_oferowanych_pakietow=0;
		Dane.ilosc_retransmitowanych_pakietow=0;
		Dane.ilosc_traconych_pakietow=0;
		Dane.kolizja=false;
		Dane.kanal_wolny=true;
		Dane.ilosc_pakietow_wyslanych=0;
		Dane.test_kolizji=false;
		printf("\n...");

	}

	return 0;
};
