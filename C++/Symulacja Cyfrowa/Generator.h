#pragma once
#include <math.h>
#include <iostream>
class Generator
{
private:
	double		_X0, //j�dro dla generatora
	 			_c,  // sta�a
					_m,  // okres
					_a,  // mno�nik
					_Xn; // nastepna wartosc jadra

	/*
	c i m powinny byc wzjamnie pierwszez
	a = 1 mod p dla kazdego czynika pierwszego liczby p
	a = 1 mod 4, jesli 4 jest dzielnikiem liczby m

	wzor
	Xnext=(a*Xn+C) mod m
	*/
public:
	Generator(int);
	Generator(void);
	~Generator(void);

	double Rownomierny(void);
	double Rownomierny(double , double );
	int  Wykladniczny(double );
};

