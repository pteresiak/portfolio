#include "siec.h"

extern Zmienne_globalne Dane;

//////////////////////////////////////
//Zdarzenie
Zdarzenie::Zdarzenie(Stacja *a_cel,unsigned int a_czas_zdarzenia, int a_typ_zdarzenia, int a_id_obiektu)
{
	id_obiektu=a_id_obiektu;
	cel=a_cel;
	czas_zdarzenia=a_czas_zdarzenia;
	typ_zdarzenia=a_typ_zdarzenia;
	nastepne=0;
};

int Zdarzenie::Uruchom()
{

	FILE *komunikaty = fopen(  "Komunikaty.txt", "a");
	fprintf(komunikaty, "Czas\t%d\tID\t%d\ttyp\t%d  -  Uruchomienie zdarzenia\n", czas_zdarzenia,id_obiektu,typ_zdarzenia);
	fclose(komunikaty);
	//	printf("Czas\t%d\tID\t%d\ttyp\t%d  -  Uruchomienie zdarzenia\n", czas_zdarzenia,id_obiektu,typ_zdarzenia);
	return (cel->tick(typ_zdarzenia));

};

void Zdarzenie::Usun_Zarzenie()
{
	Dane.g_pierwsze_zdarzenie=this->nastepne;
	delete this;
};

Zdarzenie::~Zdarzenie()
{
	FILE *komunikaty = fopen(  "Komunikaty.txt", "a");
	fprintf(komunikaty, "Czas\t%d\tID\t%d\ttyp\t%d  -  usuwania zdarzenia\n", czas_zdarzenia,id_obiektu,typ_zdarzenia);
	fclose(komunikaty);
	//	printf("Czas\t%d\tID\t%d\ttyp\t%d  -  usuwania zdarzenia\n", czas_zdarzenia,id_obiektu,typ_zdarzenia);


};

//////////////////////////////////////
// Stacje

Stacja::Stacja(int jadro, int id_stacja)
{
	id=id_stacja;
	nastepna=0;
	stan=0;
	D_licznik=-1; // -1 nie uruchomiaony licznik
	KL_licznik=0;
	RT_licznik=0; // licznik retrasmisji
	FIFO_licznik=0; // ilosc pakietow w fifo
	Generator_stacji = new Generator(jadro);
	wysylam=false;
	flaga_gotowosci=true;
	proba_transmisji=false;
	czas_poczatkowy_zajetosci_stacji=0;
	for (int i=0;i<=10;i++) tablica_czasow_pojawienia_sie_pakietu[i]=0;
		FILE *komunikaty = fopen(  "Komunikaty.txt", "a");
	fprintf(komunikaty, "\tID_Stacja\t%d\tJadro\t%d\t  -  Tworzenie stacji\n",id_stacja,jadro );
	fclose(komunikaty);

};

void Stacja::Przesuwanie_tablicy()
{
	for(int i=0; i<10;i++)
	{
		tablica_czasow_pojawienia_sie_pakietu[i]=tablica_czasow_pojawienia_sie_pakietu[i+1];
	}
	tablica_czasow_pojawienia_sie_pakietu[10]=0;

};


void Stacja::Losuj_D()
{

	D_licznik=Generator_stacji->Rownomierny(0,17);
};

int Stacja::tick(int type)
{
	
	FILE *komunikaty = fopen(  "czas_generacji.txt", "a");
	int czas_kolejnej_obslugi;
	switch(type)
	{
	case ZDA_GEN_PAKIET:
		Planista(this,(czas_kolejnej_obslugi= this->Generator_stacji->Wykladniczny(Dane.average)), ZDA_GEN_PAKIET,id);
		FIFO_licznik+=1;
		Dane.ilosc_oferowanych_pakietow+=1;
		if(FIFO_licznik>Dane.BT_max)
		{	
			Dane.ilosc_traconych_pakietow++;
			FIFO_licznik-=1;
		}
		else
		{
			tablica_czasow_pojawienia_sie_pakietu[FIFO_licznik]=Dane.czas_sys;


		}
		fprintf(komunikaty, "\nCZas\t%d\t\t\tGenerwoanie pakietu dla stacji id\t%d \n\t Stan fifo%d\t",Dane.czas_sys,id,FIFO_licznik );
		std::cout<<"\nCzas\t"<<Dane.czas_sys<<"\tGeneruje";
	
		fclose(komunikaty);

		Planista(this,czas_kolejnej_obslugi, ZDA_WYS_PAKIET,id);
		break;
	case ZDA_WYS_PAKIET:

		std::cout<<"\nCzas\t"<<Dane.czas_sys<<"\twysylam";

		fprintf(komunikaty, "\nCZas\t%d\t\t\twysylanie pakietu dla stacji id\t%d \n\t Stan fifo%d\tD:\t%d\t",Dane.czas_sys,id,FIFO_licznik,D_licznik);
		Procedura_dostepu();
		fclose(komunikaty);
		return 3;
		break;
	default:
		return 0;
	}
	fclose(komunikaty);
	return 0;
};


void Stacja::Procedura_dostepu()
{	
	int szansa_zaklocenia;
	FILE *zdarzenia = fopen( "Zdarzenia.txt", "a");
	flaga_gotowosci=false;
	if((czas_poczatkowy_zajetosci_stacji+100)<=Dane.czas_sys||czas_poczatkowy_zajetosci_stacji==0) //czas kiedy stacja jest zajeta iw ysyla
	{	
		//std::cout<<"\ntest\n";
		wysylam=false;
		switch(stan)
		{
		case SPRAW_FIFO:
			if(FIFO_licznik<=0)
			{	
				Komunikaty();
				flaga_gotowosci=true;
			}
			else
			{
				Komunikaty();
				stan=PROCEDURA_DOSTEPU;
			}
			break;
		case PROCEDURA_DOSTEPU:

			if(RT_licznik>Dane.RT_max)
			{
				Komunikaty();
				Dane.ilosc_traconych_pakietow++;
				stan=ZERUJ_LICZNIKI;
				Przesuwanie_tablicy();
			}
			else
			{

			
				Losuj_D();
				Komunikaty();
				stan = TESUTJE_KANAL;
flaga_gotowosci=true;
				Planista(this,1,ZDA_WYS_PAKIET,id); // poduje testowanie kanalu po 1 us 
			
			}
			
			break;
		case TESUTJE_KANAL:
			//testuje kanal co 1us i jesli kanal wolny zmiejsza D o 1
			if(Dane.kanal_wolny==true) 
			{

				if(D_licznik>0)
				{
					D_licznik-=1;
					Komunikaty();

					Planista(this,1,ZDA_WYS_PAKIET,id); // poduje testowanie kanalu po 1 us 
					flaga_gotowosci=true;
				}
				else  //jesli D ==0
				{
					Komunikaty();
					flaga_gotowosci=false;
					stan=TRANSMITUJE;
					// przejscie bezposrednei do nastepnego stanu

				}
			}
			else //jesli kanal zajety
			{

				Komunikaty();
				Planista(this,1,ZDA_WYS_PAKIET,id); // poduje testowanie kanalu po 1 us 


				flaga_gotowosci=true;


			}

			break;
		case TRANSMITUJE:

			
			if(proba_transmisji==false)
			{ 
				if(Dane.kanal_wolny==true)
				{
					proba_transmisji=true;
					Dane.kanal_wolny=false;
					Komunikaty();
					flaga_gotowosci=false;

				}
				else
				{
					Dane.kolizja=true;
					Komunikaty();
					stan=WYKRYCIE_KOLIZJI;
					Dane.test_kolizji=true;
					czas_poczatkowy_zajetosci_stacji=Dane.czas_sys;
					Planista(this,(100),ZDA_WYS_PAKIET,id);
					flaga_gotowosci=true;
					wysylam=true;
				}
			}
			else
			{
				if(Dane.kolizja==false)
				{
					proba_transmisji=false;
					Komunikaty();
					stan= ZAKLOCENIA_W_KANALE;
					wysylam=true;
					Planista(this,(100),ZDA_WYS_PAKIET,id); // Transmisja kanal zajety na  100 us
					czas_poczatkowy_zajetosci_stacji=Dane.czas_sys;
					flaga_gotowosci=true;
					Dane.kanal_wolny=false;
				}
				else
				{
					Dane.kolizja=false;
					proba_transmisji=false;
					Komunikaty();
					Dane.test_kolizji=true;
					wysylam=true;
					stan=WYKRYCIE_KOLIZJI;
					Dane.kanal_wolny=false;
					czas_poczatkowy_zajetosci_stacji=Dane.czas_sys;
					Planista(this,(100),ZDA_WYS_PAKIET,id);

					flaga_gotowosci=true;
				}

			}

			break;
		case WYKRYCIE_KOLIZJI:
			Dane.test_kolizji=false;
			Dane.kanal_wolny=true;
			++RT_licznik;
			++KL_licznik;
			Komunikaty();
			stan=PROCEDURA_DOSTEPU;
			break;
		case ZAKLOCENIA_W_KANALE:

			Dane.kanal_wolny=true;
			szansa_zaklocenia=Generator_stacji->Rownomierny(0,1001);
			if(szansa_zaklocenia==500)
			{
				RT_licznik++;
				Komunikaty();
				stan=PROCEDURA_DOSTEPU;

			}
			else
			{
				FILE *zliczenie_dostarczonych_pakietow = fopen(  "zliczenie_dostarczonych_pakietow.txt", "a");
				fprintf(zliczenie_dostarczonych_pakietow, "\nnr_pomiaru\t%d\tIlosc_Stacji\t%\d\tCzas_symulacji\t%d us\tCzas_dostarczenia_pakietu\t%d us\tCzas_wyslania_pakietu\t%d us\tCzas_przebywania_pakietu_w_stacji\t%d us",Dane.nr_pomiaru,Dane.ilosc_stacji,Dane.czas_symulacji,tablica_czasow_pojawienia_sie_pakietu[1],Dane.czas_sys,(Dane.czas_sys-tablica_czasow_pojawienia_sie_pakietu[1]) );
				//for (int i=0;i<10;i++)fprintf(zliczenie_dostarczonych_pakietow, "\t%d",tablica_czasow_pojawienia_sie_pakietu[i]);
				fclose(zliczenie_dostarczonych_pakietow);
				Przesuwanie_tablicy();
				Komunikaty();
				stan=ZERUJ_LICZNIKI;
				Dane.ilosc_pakietow_wyslanych+=1;// ilsoc poprawnie dostarczonych pakietow		

			}
			break;
		case ZERUJ_LICZNIKI:
			Dane.ilosc_retransmitowanych_pakietow+=RT_licznik;
			D_licznik=-1;
			RT_licznik=0;
			KL_licznik=0;
			wysylam=false;
			proba_transmisji=false;
			Dane.kanal_wolny=true;
			FIFO_licznik-=1;
			Komunikaty();
			stan=SPRAW_FIFO;
			flaga_gotowosci=false;
			break;
		}
	}
	else
	{
		flaga_gotowosci=true;

	}
	fclose(zdarzenia);
};

void Stacja::Komunikaty()
{
	FILE *komunikaty = fopen(  "Komunikaty.txt", "a");
	switch(stan)
	{
	case SPRAW_FIFO:
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji SPRAW_FIFO\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	case TESUTJE_KANAL:	
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji TESUTJE_KANAL\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	case TRANSMITUJE:			
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji TRANSMITUJE\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	case WYKRYCIE_KOLIZJI:	
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji WYKRYCIE_KOLIZJI\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	case ZAKLOCENIA_W_KANALE: 
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji ZAKLOCENIA_W_KANALE\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	case ZERUJ_LICZNIKI:		
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji ZERUJ_LICZNIKI\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	case PROCEDURA_DOSTEPU:	
		fprintf(komunikaty, "\tTime:\t%d\tStacja_ID\t%d\tKanal_wolny\t%d\tWysylam\t%d\tProba_transmisji\t%d\tLicznik_D\t%d\tLicznik_KL\t%d\tLicznik_RT\t%d\tLicznik_FIFO\t%d  -  status staccji PROCEDURA_DOSTEP\n",Dane.czas_sys,id,Dane.kanal_wolny,wysylam,proba_transmisji,D_licznik,KL_licznik,RT_licznik,FIFO_licznik);
		break;
	default:
		break;
	}	
	fclose(komunikaty);
};
