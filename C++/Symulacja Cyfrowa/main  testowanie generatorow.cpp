#include <stdio.h>
#include <iostream>
#include "Generator.h"
int main ()
{
	int choose;
	do{
		int jadro;
		printf(	"\tPROGRAM DO TESTOWANIA GENERATOROW\n\n");
		printf("\nWprowadz wartosc jadra\t");
		std::cin>>jadro;	
		printf("\nmenu:\n"
			"\t1\tGenerator rownomierny\n"
			"\t2\tGenerator wykladniczy\n"
			"\t3\tZakoncz program"
			"\n\n");
		Generator Rownom(jadro);	
		std::cin>>choose;
		//czyszczenie pliku
		FILE *_gen_num = fopen("Generator liczb rownomiernych.txt", "w");
		FILE *_gen_wykl = fopen("Generator liczb wykladniczych.txt", "w");
		fclose(_gen_num);
		fclose(_gen_wykl);
		//
		switch (choose)
		{
		case 1:
			printf("\n\tTestuje\n\n");
			int number;

			for (int i=0; i<300; i++)
			{
				number=Rownom.Rownomierny(0,101);
				printf("%d\n",number);
				//std::cout<<number;
				fopen("Generator liczb rownomiernych od 0 do 100.txt", "a"); //dopisywanie na koniec pliku
				fprintf(_gen_num, "\njadro\t%d\tWartosc:\t%d",jadro, number);
				fclose(_gen_num);

			}
			break;
		case 2:printf("\n\tTestuje\n\n");
			double number_wykl;

			for (int i=0; i<300; i++)
			{
				number_wykl=Rownom.Wykladniczny(15000);
				printf("%f\n",number_wykl);
				//std::cout<<number;
				fopen("Generator liczb Wykladniczy z srenia 15000 ilosc prob 1000.txt", "a"); //dopisywanie na koniec pliku
				fprintf(_gen_wykl, "\njadro\t%d\tWartosc:\t%f",jadro, number_wykl);
				fclose(_gen_wykl);

			}
			break;
		case 3:
			break;
		default:
			printf("\n\tZLY WYBOR... wpisz ponownie");
		}	

	}while(choose!=3);


	return 0;
}