#include "Generator.h"

Generator::Generator(){
	_X0 = 11;
	_a = 11103515245;
	_m = (pow( 2,31)-1);
	_Xn = _X0;
	_c = 12346;
	
};
Generator::Generator( int _core_gen)
{
	_X0 = _core_gen;
	_a = 11103515245;
	_m = (pow( 2,31)-1);
	_Xn = _X0;
	_c = 12346;
};
double  Generator::Rownomierny(double min,  double max)
{

_Xn= fmod((_a*_Xn+_c),_m);  
return(( _Xn / _m)*(max - min) + min);
};
int  Generator::Wykladniczny(double srednia)
{
return -srednia*log(Rownomierny(0,1));
};
Generator::~Generator(void)
{
}
