#pragma once
#include "Generator.h"


#define ZDA_GEN_PAKIET (1) //zdarzenie generuj pakiet
#define ZDA_WYS_PAKIET (2)	//zdarzenie wyslij pakiet

// stany procedury dostepu do kanalu
#define SPRAW_FIFO			(0)
#define TESUTJE_KANAL		(1)
#define TRANSMITUJE			(2)
#define WYKRYCIE_KOLIZJI	(3)
#define ZAKLOCENIA_W_KANALE (4)
#define ZERUJ_LICZNIKI		(5)
#define PROCEDURA_DOSTEPU	(6)

class Stacja;

class Zdarzenie{
public:
	Stacja *cel;
	Zdarzenie *nastepne;
	unsigned  int czas_zdarzenia;
	int typ_zdarzenia; // 1. Generowanie_pakietu, 2. Wyslanie_pakietu
	int id_obiektu;
	Zdarzenie(Stacja* ,unsigned int, int,int);
	~Zdarzenie();
	void Usun_Zarzenie();
	int Uruchom();
};


struct Zmienne_globalne 
{


	//Stale  i zmienne globalne
	unsigned int  czas_symulacji;// 20s
	bool kanal_wolny;
	const double average ; //czas w milisekundach! dok�adnie 14,999999. bo:
	//pr�dko�� = 100kB/s, 66pakiet�w /sekund�, 
	//1s/66=15ms.
	const int D_max;	//maskymalny zakres D
	const int BT_max; //maskymalne fifo;
	const int RT_max; // maksyalna ilosc retransmisji
	int  czas_sys; // 1=1us, 1000=1ms, 1 000 000=1s
	Zdarzenie  *g_pierwsze_zdarzenie;
	Stacja* g_wsk_na_pierwsza_stacje;
	bool kolizja;
	int ilosc_stacji;
	bool test_kolizji;
	int nr_pomiaru;
	int ilosc_powtorzen;
	//dane do zebrania
	int ilosc_traconych_pakietow;
	int ilosc_retransmitowanych_pakietow;
	int ilosc_kolizji;
	int ilosc_oferowanych_pakietow;
	int ilosc_pakietow_wyslanych;
};



class Stacja {
private:	
	int stan;
	int D_licznik; // liczba losowana od 0 do 16
	int KL_licznik; // licznik kolizji
	int RT_licznik; // licznik retrasmisji
	int FIFO_licznik; // ilosc pakietow w fifo
	bool proba_transmisji;
	int czas_poczatkowy_zajetosci_stacji;
public:
	bool wysylam;
	int id;
	Generator *Generator_stacji;
	Stacja *nastepna;
	Stacja(int,int); //konstruktor
	void Losuj_D(void);
	int tick(int typ);
	void Procedura_dostepu();
	bool flaga_gotowosci;
	int tablica_czasow_pojawienia_sie_pakietu[10];
	void Komunikaty(void);
	//indeks tablicy odpowiada numerowi pakeituw  kolejce
	void Przesuwanie_tablicy(void);
	int IloSC_w_buforze(void){return FIFO_licznik;};

};
void Planista(Stacja*,  int, int,int );
