#ifndef FUNCTION_H
#define FUNCTION_H
#include <vector>
#include <SFML/Graphics.hpp>

void Test();


enum TypeLine { Player,Road,Block,Point,Change};

struct section
{
	TypeLine color1;
	float posY1;
	TypeLine color2;
	float posY2;
};

std::vector<section> RandomSection(int lenght_px  );
void  RandomSection(int lenght_px,std::vector<section>* contener );


sf::Color ColorRoad(TypeLine typeLine);
#endif // FUNCTION.H
