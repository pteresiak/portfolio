package application;

public class Statistics {
	private int score;
	private int level;
	private boolean online;
	private String nick;

	public Statistics() {
		this.score = 0;
		this.level = 1;
		this.online = false;
		this.nick = "Test";
	}

	public Statistics(int score, int level, boolean online, String nick) {
		super();
		this.score = score;
		this.level = level;
		this.online = online;
		this.nick = nick;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public void addScore (int score) {
		this.score += score;
	}
	
	public void seeScore (int score) {
		this.score = score;
	}

}
