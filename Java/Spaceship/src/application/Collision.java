package application;

import javafx.geometry.Rectangle2D;

public interface Collision {
	public Rectangle2D getBoundary() ;

	public boolean intersects(Collision s) ;
}
