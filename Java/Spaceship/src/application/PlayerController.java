package application;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class PlayerController implements EventHandler<KeyEvent> {
	int speed = 300;
	int whatSpriteChoose = 1;
	private Sprite stay;
	private Sprite up;
	private Sprite down;
	private Sprite right;
	private Sprite left;
	private Player p;

	public PlayerController(Player p) {
		this.p = p;
		this.stay = new Sprite(10);
		this.up = new Sprite(10);
		this.down = new Sprite(10);
		this.right = new Sprite(10);
		this.left = new Sprite(10);
		this.initialize();
	}

	private void initialize() {
		this.stay.addImage("/res/player/ships.png");

		this.up.addImage("/res/player/ships.png");
		this.up.addImage("/res/player/shipUp.png");
		this.up.addImage("/res/player/ships.png");
		
		this.down.addImage("/res/player/ships.png");
		this.down.addImage("/res/player/shipDown.jpg");
		this.down.addImage("/res/player/ships.png");
		
		this.right.addImage("/res/player/ships.png");
		this.right.addImage("/res/player/shipRight.png");
		this.right.addImage("/res/player/ships.png");
		
		
		this.left.addImage("/res/player/ships.png");
		this.left.addImage("/res/player/shipLeft.png");
		this.left.addImage("/res/player/ships.png");
		
		
		p.setWidth((int) this.stay.getFirstFrame().getWidth());
		p.setHeight((int) this.stay.getFirstFrame().getHeight());
	}

	public Image getPlayerFrame(double time) {
		switch (whatSpriteChoose) {
		case 1:
			return this.stay.getFrame(time);
		case 2:
			return this.left.getFrame(time);
		case 3:
			return this.right.getFrame(time);
		case 4:
			return this.up.getFrame(time);
		case 5:
			return this.down.getFrame(time);
		default:
			break;
		}
		return null;
	}

	@Override
	public void handle(KeyEvent event) {

		if (event.getEventType() == event.KEY_PRESSED) {
			if (event.getCode().equals(KeyCode.LEFT)) {
				int playerPosX = p.getPositionX();
				if (playerPosX > 0 + speed) {
					playerPosX -= speed;

				} else {
					playerPosX = 0;
				}
				p.setDestinationPosWidth(playerPosX);
				whatSpriteChoose = 2;
			}
			if (event.getCode().equals(KeyCode.RIGHT)) {
				int playerPosX = p.getPositionX();
				if (playerPosX + this.right.getFirstFrame().getWidth() < Main.WIDTH - speed) {
					playerPosX += speed;

				} else {
					playerPosX = Main.WIDTH - (int) this.right.getFirstFrame().getWidth();
				}
				p.setDestinationPosWidth(playerPosX);
				whatSpriteChoose = 3;
			}
			if (event.getCode().equals(KeyCode.UP)) {
				int playerPosY = p.getPositionY();
				if (playerPosY > 0 + speed) {
					playerPosY -= speed;

				} else {
					playerPosY = 0;
				}

				p.setDestinationPosHeight(playerPosY);
				whatSpriteChoose = 4;
			}
			if (event.getCode().equals(KeyCode.DOWN)) {
				int playerPosY = p.getPositionY();
				if (playerPosY + this.down.getFirstFrame().getHeight() < Main.HEIGHT - speed) {
					playerPosY += speed;

				} else {
					playerPosY = Main.HEIGHT - (int) this.right.getFirstFrame().getHeight();
				}
				p.setDestinationPosHeight(playerPosY);
				whatSpriteChoose = 5;
			}
			if (event.getCode().equals(KeyCode.ESCAPE)) {
				
				//pozniej tutaj bedzie ustawiania w setingu zmienna do wyswietlenia menu gry
				System.exit(0);

			}
			if (event.getCode().equals(KeyCode.C)) {
				p.setStopMove(true);

			}

		}
		if (event.getEventType() == event.KEY_RELEASED) {
			whatSpriteChoose = 1;
			
			p.setStopMove(false);

		}

	}

}
