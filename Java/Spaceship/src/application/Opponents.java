package application;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;

public class Opponents implements Collision {
	int speed = 100;

	private double positionX;
	private double positionY;
	private double width;
	private double height;
	private Sprite stayOpponent;
	private int score;

	public Opponents(int speed, double positionX, double positionY) {

		this.speed = speed;
		this.positionX = positionX;
		this.positionY = positionY;
		this.score = 100;
		this.stayOpponent = new Sprite(10);
		this.initialize();
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void opponentMove() {

	}

	private void initialize() {
		this.stayOpponent.addImage("res/opponents/asteroid1.png");
	}

	public Image getOpponentsFrame(double time) {
		return this.stayOpponent.getFrame(time);
	}

	public void setImage(Image i) {
		width = i.getWidth();
		height = i.getHeight();
	}

	public void setImage(String filename) {
		Image i = new Image(filename);
		setImage(i);
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public Rectangle2D getBoundary() {
		return new Rectangle2D(positionX, positionY, width, height);
	}

	@Override
	public boolean intersects(Collision s) {
		return s.getBoundary().intersects(this.getBoundary());
	}

	public void move() {
		this.positionX -= speed;
	}

}
