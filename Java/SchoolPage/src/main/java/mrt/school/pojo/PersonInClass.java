package mrt.school.pojo;

public class PersonInClass {
	private Long idClass;

	private Long idOldPerson;

	private Long idNewPerson;

	public Long getIdClass() {
		return idClass;
	}

	public void setIdClass(Long idClass) {
		this.idClass = idClass;
	}

	public Long getIdOldPerson() {
		return idOldPerson;
	}

	public void setIdOldPerson(Long idOldPerson) {
		this.idOldPerson = idOldPerson;
	}

	public Long getIdNewPerson() {
		return idNewPerson;
	}

	public void setIdNewPerson(Long idNewPerson) {
		this.idNewPerson = idNewPerson;
	}

}
