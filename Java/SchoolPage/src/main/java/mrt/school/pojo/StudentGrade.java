package mrt.school.pojo;

import java.util.List;

import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.school.grade.SchoolGrade;

public class StudentGrade {
	private PersonalData student;
	private List<SchoolGrade> schoolGradeList;

	public PersonalData getStudent() {
		return student;
	}

	public void setStudent(PersonalData student) {
		this.student = student;
	}

	public List<SchoolGrade> getSchoolGradeList() {
		return schoolGradeList;
	}

	public void setSchoolGradeList(List<SchoolGrade> schoolGradeList) {
		this.schoolGradeList = schoolGradeList;
	}

	@Override
	public String toString() {
		return "StudentGrade [student=" + student + ", schoolGrade=" + schoolGradeList + "]";
	}

}
