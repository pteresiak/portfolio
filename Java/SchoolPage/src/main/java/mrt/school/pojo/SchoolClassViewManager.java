package mrt.school.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.school.group.SchoolClass;

@Repository
public class SchoolClassViewManager {

	private SchoolClass schoolClass;

	private PersonalData educator;

	private Long numberOfStudents;

	public SchoolClass getSchoolClass() {
		return schoolClass;
	}

	public void setSchoolClass(SchoolClass schoolClass) {
		this.schoolClass = schoolClass;
	}

	public PersonalData getEducator() {
		return educator;
	}

	public void setEducator(PersonalData educator) {
		this.educator = educator;
	}

	public Long getNumberOfStudents() {
		return numberOfStudents;
	}

	public void setNumberOfStudents(Long numberOfStudents) {
		this.numberOfStudents = numberOfStudents;
	}

	@Override
	public String toString() {
		return "SchoolClassViewManager [schoolClass=" + schoolClass + ", educator=" + educator + ", numberOfStudents="
				+ numberOfStudents + "]";
	}

}
