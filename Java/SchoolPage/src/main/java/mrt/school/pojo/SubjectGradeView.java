package mrt.school.pojo;

import java.util.List;

import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.school.grade.SchoolGrade;
import mrt.school.core.subject.actual.ActualSubject;

public class SubjectGradeView {
	private ActualSubject subject;
	private List<SchoolGrade> schoolGradeList;

	public ActualSubject getSubject() {
		return subject;
	}

	public void setSubject(ActualSubject subject) {
		this.subject = subject;
	}

	public List<SchoolGrade> getSchoolGradeList() {
		return schoolGradeList;
	}

	public void setSchoolGradeList(List<SchoolGrade> schoolGradeList) {
		this.schoolGradeList = schoolGradeList;
	}

	@Override
	public String toString() {
		return "SubjectGradeView [subject=" + subject + ", schoolGradeList=" + schoolGradeList + "]";
	}

}
