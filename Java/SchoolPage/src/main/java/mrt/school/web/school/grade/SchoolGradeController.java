package mrt.school.web.school.grade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import mrt.school.core.account.AccountProvider;
import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.personal.data.PersonalDataDao;
import mrt.school.core.school.grade.SchoolGrade;
import mrt.school.core.school.grade.SchoolGradeDao;
import mrt.school.core.school.grade.TypeOfGrade;
import mrt.school.core.school.grade.TypeOfGradeDao;
import mrt.school.core.subject.actual.ActualSubject;
import mrt.school.core.subject.actual.ActualSubjectDao;
import mrt.school.pojo.StudentGrade;

@Controller
public class SchoolGradeController {

	@Autowired
	private AccountProvider acountProvider;

	@Autowired
	private ActualSubjectDao actualSubjectDao;

	@Autowired
	private PersonalDataDao personalDataDao;

	@Autowired
	private TypeOfGradeDao typeOfGradeDao;

	@Autowired
	private SchoolGradeDao schoolGradeDao;

	@RequestMapping("/teacher/grade/manager")
	public ModelAndView getGradeManager() {

		ModelAndView mav = new ModelAndView("teacher/gradeManager");
		mav.addObject("login", acountProvider.getloggedAccount().getLogin());

		List<ActualSubject> subjectList = this.getSubjecetListWhenPersonTeach();
		if (subjectList != null) {
			mav.addObject("subjectList", this.getSubjecetListWhenPersonTeach());
			mav.addObject("isSubjectListFlag", true);
		} else {
			mav.addObject("isSubjectListFlag", false);
		}
		return mav;
	}

	@RequestMapping(value = "/teacher/grade/{subjectId}/edit", method = RequestMethod.GET)
	public ModelAndView getGradeEdit(@PathVariable Long subjectId) {

		ActualSubject aS = actualSubjectDao.getById(subjectId);
		ModelAndView mav = new ModelAndView("teacher/gradeEdit");
		mav.addObject("login", acountProvider.getloggedAccount().getLogin());
		mav.addObject("subject", aS);
		mav.addObject("studentsGrade", this.getStudentGrade(aS.getSchoolClass().getId(), subjectId));
		mav.addObject("titleTypeOfGrade", typeOfGradeDao.getAll());
		mav.addObject("titleTypeOfGradeMap", this.getTypeOfGradeMap());
		mav.addObject("newGrade", new SchoolGrade());
		mav.addObject("gradeMap", this.getGradeMap());
		return mav;
	}
	

	@RequestMapping("/student/grade/view")
	public ModelAndView getStudentGradeView() {

		ModelAndView mav = new ModelAndView("student/gradeView");
		mav.addObject("login", acountProvider.getloggedAccount().getLogin());


		return mav;
	}	

	@RequestMapping(value = "/teacher/grade/addGrade", method = RequestMethod.POST)
	public String saveGrade(@ModelAttribute SchoolGrade newGrade) {
		SchoolGrade sGrade = new SchoolGrade();
		sGrade.setTypeOfGrade(typeOfGradeDao.getById(newGrade.getTypeOfGrade().getId()));
		sGrade.setInfo(newGrade.getInfo());
		sGrade.setGrade(newGrade.getGrade());
		sGrade.setStudent(personalDataDao.getById(newGrade.getStudent().getId()));
		sGrade.setSubject(actualSubjectDao.getById(newGrade.getSubject().getId()));

		schoolGradeDao.saveOrUpdate(sGrade);
		return "redirect:/teacher/grade/" + newGrade.getSubject().getId() + "/edit";
	}

	private List<ActualSubject> getSubjecetListWhenPersonTeach() {
		PersonalData pD = acountProvider.getloggedAccount().getPersonalData();

		if (pD != null) {
			List<ActualSubject> actualSubjectList = actualSubjectDao.getAll().stream()
					.filter(p -> p.getTeacher().getId() == pD.getId()).collect(Collectors.toList());
			return actualSubjectList;
		}

		return null;
	}

	private List<StudentGrade> getStudentGrade(Long schoolClassId, Long subjectId) {
		List<PersonalData> pD = personalDataDao.getStudentsListBySchoolClassId(schoolClassId);
		List<StudentGrade> sG = new ArrayList<>();
		for (PersonalData personalData : pD) {
			StudentGrade studentGrade = new StudentGrade();
			studentGrade.setStudent(personalData);
			SchoolGrade[] schoolGradeTable = new SchoolGrade[typeOfGradeDao.getAll().size()];

			List<SchoolGrade> actualSchoolGrade = schoolGradeDao
					.getSchoolGradeByStudentByActualSubject(personalData.getId(), subjectId);

			for (SchoolGrade schoolGrade : actualSchoolGrade) {
				schoolGradeTable[(int) (schoolGrade.getTypeOfGrade().getId() - 1)] = schoolGrade.clone();
			}

			studentGrade.setSchoolGradeList(Arrays.asList(schoolGradeTable));
			sG.add(studentGrade);

		}

		return sG;
	}

	private Map<Long, String> getTypeOfGradeMap() {
		Map<Long, String> typeOfGradeMap = new HashMap<>();
		List<TypeOfGrade> typeOfGradeList = typeOfGradeDao.getAll();
		for (TypeOfGrade typeOfGrade : typeOfGradeList) {
			typeOfGradeMap.put(typeOfGrade.getId(), typeOfGrade.getType());
		}

		return typeOfGradeMap;
	}

	private Map<Long, String> getGradeMap() {
		Map<Long, String> gradeMap = new HashMap<>();

		gradeMap.put(1L, "6");
		gradeMap.put(2L, "6-");
		gradeMap.put(3L, "5+");
		gradeMap.put(4L, "5");
		gradeMap.put(5L, "5-");
		gradeMap.put(6L, "4+");
		gradeMap.put(7L, "4");
		gradeMap.put(8L, "4-");
		gradeMap.put(9L, "3+");
		gradeMap.put(10L, "3");
		gradeMap.put(11L, "2");
		gradeMap.put(12L, "1");

		return gradeMap;
	}

}
