package mrt.school.web.school.group;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mrt.school.core.account.Account;
import mrt.school.core.account.AccountProvider;
import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.personal.data.PersonalDataDao;
import mrt.school.core.school.group.SchoolClass;
import mrt.school.core.school.group.SchoolClassDao;
import mrt.school.core.subject.actual.ActualSubject;
import mrt.school.core.subject.actual.ActualSubjectDao;
import mrt.school.pojo.PersonInClass;
import mrt.school.pojo.SchoolClassViewManager;

@Controller
public class SchoolClassController {

	@Autowired
	private SchoolClassDao schoolClassDao;

	@Autowired
	private PersonalDataDao personalDataDao;

	@Autowired
	private AccountProvider accountProvider;
	
	@Autowired
	private ActualSubjectDao actualSubjectDao;
	

	@RequestMapping("/secretariat/class/manager")
	public ModelAndView classManager() {
		ModelAndView mav = new ModelAndView("secretariat/schoolClassManager");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("classes", this.getSchoolView());
		mav.addObject("newSchoolClass", new SchoolClass());
		mav.addObject("cantDelete", this.getListSchoolClassIdCantDelete());
		mav.addObject("CanDeleteFlag", true);

		return mav;
	}

	
	@RequestMapping("/teacher/class/view")
	public ModelAndView classView() {
		ModelAndView mav = new ModelAndView("teacher/classView");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		SchoolClass schoolClass = this.getSchoolCLassForLoggedUser();
		if(schoolClass!=null){
			mav.addObject("schoolClass", schoolClass);
			mav.addObject("students", personalDataDao.getStudentsListBySchoolClassId(schoolClass.getId()) );
			mav.addObject("isUserEducatorFlag", true);
		}
		else{
			mav.addObject("isUserEducatorFlag", false);
		}
		return mav;
	}
	
	
	@RequestMapping(value = "/secretariat/class/add", method = RequestMethod.POST)
	public String addSchoolClass(@ModelAttribute SchoolClass newSchoolClass) {
		schoolClassDao.saveOrUpdate(newSchoolClass);
		return "redirect:/secretariat/class/manager";
	}

	@RequestMapping(value = "/secretariat/class/update", method = RequestMethod.POST)
	public String updateSchoolClass(@ModelAttribute SchoolClass newSchoolClass) {

		SchoolClass schoolclass = schoolClassDao.getById(newSchoolClass.getId());
		schoolclass.setName(newSchoolClass.getName());
		schoolclass.setInfo(newSchoolClass.getInfo());
		schoolclass.setSpecialization(newSchoolClass.getSpecialization());

		schoolClassDao.saveOrUpdate(schoolclass);
		return "redirect:/secretariat/class/manager";
	}

	@RequestMapping(value = "/secretariat/class/person/add", method = RequestMethod.POST)
	public String addPersonToSchoolClass(@ModelAttribute PersonInClass pID) {
		if (pID.getIdNewPerson() > 0) {

			PersonalData pD = personalDataDao.getById(pID.getIdNewPerson());
			pD.setSchoolClass(schoolClassDao.getById(pID.getIdClass()));
			personalDataDao.saveOrUpdate(pD);
		}
		String redirect = "redirect:/secretariat/class/" + pID.getIdClass() + "/editView";
		return redirect;
	}

	@RequestMapping(value = "/secretariat/class/person/update", method = RequestMethod.POST)
	public String updatePersonInSchoolClass(@ModelAttribute PersonInClass pID) {
		if (pID.getIdNewPerson() > 0) {
			this.deletePersonFromSchoolClass(pID);
			return this.addPersonToSchoolClass(pID);
		}
		String redirect = "redirect:/secretariat/class/" + pID.getIdClass() + "/editView";
		return redirect;
	}

	@RequestMapping(value = "/secretariat/class/person/delete", method = RequestMethod.POST)
	public String deletePersonFromSchoolClass(@ModelAttribute PersonInClass pID) {
		if (pID.getIdOldPerson() > 0) {
			PersonalData oldPerson = personalDataDao.getById(pID.getIdOldPerson());
			oldPerson.setSchoolClass(null);
			personalDataDao.saveOrUpdate(oldPerson);
		}
		String redirect = "redirect:/secretariat/class/" + pID.getIdClass() + "/editView";
		return redirect;
	}

	@RequestMapping(value = "/secretariat/class/{schoolClassId}/delete", method = RequestMethod.GET)
	public String deleteSchoolClass(@PathVariable Long schoolClassId) {
		try {
			schoolClassDao.deleteById(schoolClassId);
		} catch (Exception e) {

			System.out.println(e.getMessage());
		}

		return "redirect:/secretariat/class/manager";
	}

	@RequestMapping(value = "/secretariat/class/{schoolClassId}/editView", method = RequestMethod.GET)
	public ModelAndView editViewSchoolClass(@PathVariable Long schoolClassId) {

		ModelAndView mav = new ModelAndView("secretariat/schoolClassEdit");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("status", accountProvider.getloggedAccount().getRole().getStatus());
		mav.addObject("newSchoolClass", new SchoolClass());
		mav.addObject("schoolClassView", this.getSchoolViewBySchoolClassID(schoolClassId));
		mav.addObject("freeEducator", this.getFreeEduccator());
		mav.addObject("freeStudents", this.getFreeStudents());
		mav.addObject("personPOJO", new PersonInClass());
		mav.addObject("listOfStudents", personalDataDao.getStudentsListBySchoolClassId(schoolClassId));
		mav.addObject("newSchoolClass", new SchoolClass());

		return mav;
	}

	private SchoolClassViewManager getSchoolViewBySchoolClassID(Long schoolClassID) {
		PersonalData pD;
		SchoolClass schoolClass = schoolClassDao.getById(schoolClassID);

		SchoolClassViewManager schoolClassView = new SchoolClassViewManager();
		schoolClassView.setSchoolClass(schoolClass);

		pD = personalDataDao.getTeacherBySchoolClassId(schoolClass.getId());
		if (pD != null) {
			schoolClassView.setEducator(pD);
		} else {
			schoolClassView.setEducator(null);
		}

		schoolClassView
				.setNumberOfStudents((long) personalDataDao.getStudentsListBySchoolClassId(schoolClass.getId()).size());

		return schoolClassView;
	}

	private List<SchoolClassViewManager> getSchoolView() {

		List<SchoolClass> sc = schoolClassDao.getAll();
		List<SchoolClassViewManager> listSCVM = new ArrayList<>();
		PersonalData pD;

		for (SchoolClass schoolClass : sc) {
			SchoolClassViewManager schoolClassView = new SchoolClassViewManager();
			schoolClassView.setSchoolClass(schoolClass);

			pD = personalDataDao.getTeacherBySchoolClassId(schoolClass.getId());
			if (pD != null) {
				schoolClassView.setEducator(pD);
			} else {
				schoolClassView.setEducator(null);
			}

			schoolClassView.setNumberOfStudents(
					(long) personalDataDao.getStudentsListBySchoolClassId(schoolClass.getId()).size());
			listSCVM.add(schoolClassView);
		}

		return listSCVM;
	}

	private List<Long> getListSchoolClassIdCantDelete() {

		List<Long> returnListId = new ArrayList<Long>();
		Set<Long> listId = new HashSet<>();
		List<PersonalData> PersonalDataList = personalDataDao.getAll();
		for (PersonalData person : PersonalDataList) {
			if (person.getSchoolClass() != null) {
				listId.add(person.getSchoolClass().getId());
			}
		}

		List<ActualSubject> subjects= actualSubjectDao.getAll();
		for (ActualSubject actualSubject : subjects) {
			if(actualSubject.getSchoolClass()!=null){
				listId.add(actualSubject.getSchoolClass().getId());
			}
		}
		
		returnListId.addAll(listId);
		return returnListId;

	}

	private Map<Long, String> getFreeEduccator() {

		Map<Long, String> mapEducator = new HashMap<>();
		List<PersonalData> pD = personalDataDao.getAll();
		// List<PersonalData> freeEducators= pD;
		List<PersonalData> freeEducators = pD.stream()
				.filter(person -> (person.getSchoolClass() == null
						&& person.getPersonalStatus().getStatus().equalsIgnoreCase("TEACHER")))
				.collect(Collectors.toList());

		for (PersonalData personalData : freeEducators) {
			mapEducator.put(personalData.getId(), personalData.getSurname() + " " + personalData.getName());
		}
		mapEducator.put(-1L, "NULL");

		return mapEducator;
	}

	private Map<Long, String> getFreeStudents() {

		Map<Long, String> mapStudents = new HashMap<>();
		List<PersonalData> pD = personalDataDao.getAll();

		List<PersonalData> freeStudents = pD.stream()
				.filter(person -> (person.getSchoolClass() == null
						&& person.getPersonalStatus().getStatus().equalsIgnoreCase("student")))
				.collect(Collectors.toList());

		for (PersonalData personalData : freeStudents) {
			mapStudents.put(personalData.getId(), personalData.getSurname() + " " + personalData.getName());
		}
		mapStudents.put(-1L, "NULL");

		return mapStudents;
	}

	private SchoolClass getSchoolCLassForLoggedUser(){
		
		PersonalData pD=accountProvider.getloggedAccount().getPersonalData();
		if(pD!=null){
			SchoolClass sc = pD.getSchoolClass();
			return sc;
		}
		
		
		return null;
	}
	
}
