package mrt.school.web.subject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mrt.school.core.account.AccountProvider;
import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.personal.data.PersonalDataDao;
import mrt.school.core.school.group.SchoolClass;
import mrt.school.core.school.group.SchoolClassDao;
import mrt.school.core.subject.actual.ActualSubject;
import mrt.school.core.subject.actual.ActualSubjectDao;

import mrt.school.core.subject.list.SubjectList;
import mrt.school.core.subject.list.SubjectListDao;

@Controller
public class SubjectController {

	@Autowired
	private SubjectListDao subjectDao;

	@Autowired
	private ActualSubjectDao actualSubjectDao;

	@Autowired
	private AccountProvider accountProvider;

	@Autowired
	private PersonalDataDao personalDataDao;

	@Autowired
	private SchoolClassDao schoolClassDao;

	@RequestMapping("/secretariat/subject/manager")
	public ModelAndView subjectManager() {
		ModelAndView mav = new ModelAndView("secretariat/subjectManager");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("actualSubjectList", actualSubjectDao.getAll());
		mav.addObject("newActualSubject", new ActualSubject());
		mav.addObject("subjectMap", this.getSubjectMap());
		mav.addObject("teacherMap", this.getTeacherMap());
		mav.addObject("schoolClassMap", this.getSchoolClassMap());

		return mav;
	}

	@RequestMapping("/secretariat/subject/addView")
	public ModelAndView subjectListView() {
		ModelAndView mav = new ModelAndView("secretariat/subjectListView");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("subjects", subjectDao.getAll());
		mav.addObject("newSubject", new SubjectList());

		return mav;
	}

	@RequestMapping(value = "/secretariat/subject/add", method = RequestMethod.POST)
	public String addSubject(@ModelAttribute SubjectList newSubjet) {
		subjectDao.saveOrUpdate(newSubjet);
		return "redirect:/secretariat/subject/addView";
	}

	@RequestMapping(value = "/secretariat/subject/addActualSubject", method = RequestMethod.POST)
	public String addAcatualSubject(@ModelAttribute ActualSubject newActualSubjet) {

		if (newActualSubjet.getSchoolClass().getId() > 0 && newActualSubjet.getSubject().getId() > 0
				&& newActualSubjet.getTeacher().getId() > 0) {

			if (actualSubjectDao.getActualSubject(newActualSubjet.getSubject().getId(),
					newActualSubjet.getTeacher().getId(), newActualSubjet.getSchoolClass().getId()) == null) {

				actualSubjectDao.saveOrUpdate(newActualSubjet);
			}
		}
		return "redirect:/secretariat/subject/manager";
	}

	private Map<Long, String> getTeacherMap() {
		Map<Long, String> mapTeacher = new HashMap<>();
		List<PersonalData> teachers = personalDataDao.getAll().stream()
				.filter(p -> p.getPersonalStatus().getStatus().equalsIgnoreCase("teacher"))
				.collect(Collectors.toList());

		mapTeacher.put(-1L, "NULL");
		for (PersonalData personalData : teachers) {
			mapTeacher.put(personalData.getId(), personalData.getSurname() + "\t" + personalData.getName());
		}
		return mapTeacher;
	}

	private Map<Long, String> getSubjectMap() {
		Map<Long, String> mapSubject = new HashMap<>();
		List<SubjectList> subjects = subjectDao.getAll();
		mapSubject.put(-1L, "NULL");
		for (SubjectList subject : subjects) {
			mapSubject.put(subject.getId(), subject.getName());
		}
		return mapSubject;
	}

	private Map<Long, String> getSchoolClassMap() {
		Map<Long, String> mapSchoolClass = new HashMap<>();
		List<SchoolClass> schoolClassList = schoolClassDao.getAll();
		mapSchoolClass.put(-1L, "NULL");
		for (SchoolClass schoolClass : schoolClassList) {
			mapSchoolClass.put(schoolClass.getId(), schoolClass.getName());
		}
		return mapSchoolClass;
	}

}
