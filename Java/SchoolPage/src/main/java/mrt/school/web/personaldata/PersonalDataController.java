package mrt.school.web.personaldata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mrt.school.core.account.Account;
import mrt.school.core.account.AccountDao;
import mrt.school.core.account.AccountProvider;
import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.personal.data.PersonalDataDao;
import mrt.school.core.personal.status.PersonalStatus;
import mrt.school.core.personal.status.PersonalStatusDao;
import mrt.school.core.subject.actual.ActualSubject;
import mrt.school.core.subject.actual.ActualSubjectDao;

@Controller
public class PersonalDataController {

	@Autowired
	private PersonalDataDao personalDataDao;

	@Autowired
	private PersonalStatusDao personalStatusDao;

	@Autowired
	private AccountProvider accountProvider;
	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private ActualSubjectDao actualSubjectDao; 
	
	

	@RequestMapping("/secretariat/personalData/manager")
	public ModelAndView listPersonalData() {
		ModelAndView mav = new ModelAndView("secretariat/personalDataManager");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("status", accountProvider.getloggedAccount().getRole().getStatus());
		mav.addObject("personalDatas", personalDataDao.getAll());
		mav.addObject("newPersonalData", new PersonalData());
		mav.addObject("personalStatusMap", this.getPersonalStatusMap());
		mav.addObject("cantDelete", this.getListPersonalDataIdCantDelete());
		mav.addObject("CanDeleteFlag", true);
		return mav;
	}

	@RequestMapping(value = "/secretariat/personalData/add", method = RequestMethod.POST)
	public String addPersonalData(@ModelAttribute PersonalData newPersonalData) {
		personalDataDao.saveOrUpdate(newPersonalData);
		return "redirect:/secretariat/personalData/manager";
	}

	@RequestMapping(value = "/secretariat/personalData/{personalDataId}/delete", method = RequestMethod.GET)
	public String deletePersonalData(@PathVariable Long personalDataId) {
		try {
			personalDataDao.deleteById(personalDataId);
		} catch (Exception e) {

		}

		return "redirect:/secretariat/personalData/manager";
	}

	@RequestMapping(value = "/secretariat/personalData/{personalDataId}/editView", method = RequestMethod.GET)
	public ModelAndView editViewPersonalData(@PathVariable Long personalDataId) {
		ModelAndView mav = new ModelAndView("secretariat/personalDataEdit");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("status", accountProvider.getloggedAccount().getRole().getStatus());
		mav.addObject("newPersonalData", new PersonalData());
		mav.addObject("personalData", personalDataDao.getById(personalDataId));
		mav.addObject("personalStatusMap", this.getPersonalStatusMap());

		return mav;
	}

	@RequestMapping(value = "/secretariat/personalData/edit", method = RequestMethod.POST)
	public String editPersonalData(@ModelAttribute PersonalData newPersonalData) {

		PersonalData actualPersonalData = personalDataDao.getById(newPersonalData.getId());
		actualPersonalData.setName(newPersonalData.getName());
		actualPersonalData.setSurname(newPersonalData.getSurname());
		actualPersonalData.setEmail(newPersonalData.getEmail());
		actualPersonalData.setPersonalStatus(newPersonalData.getPersonalStatus());
		personalDataDao.saveOrUpdate(actualPersonalData);
		return "redirect:/secretariat/personalData/manager";
	}

	private Map<Long, String> getPersonalStatusMap() {
		Map<Long, String> personalStatusMap = new HashMap<>();
		List<PersonalStatus> listPersonalStatus = personalStatusDao.getAll();

		for (PersonalStatus personalStatus : listPersonalStatus) {
			personalStatusMap.put(personalStatus.getId(), personalStatus.getStatus());
		}

		return personalStatusMap;
	}

	private List<Long> getListPersonalDataIdCantDelete() {

		List<Long> returnListId = new ArrayList<Long>();
		Set<Long> listId = new HashSet<>();
		List<Account> accountList = accountDao.getAll();
		for (Account account : accountList) {
			if (account.getPersonalData() != null) {
				listId.add(account.getPersonalData().getId());
			}
		}

		
		List<ActualSubject> subjects= actualSubjectDao.getAll();
		for (ActualSubject actualSubject : subjects) {
			if(actualSubject.getTeacher()!=null){
				listId.add(actualSubject.getTeacher().getId());
			}
		}
		
		returnListId.addAll(listId);
		return returnListId;

	}
}
