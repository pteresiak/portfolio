package mrt.school.web.account;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import mrt.school.core.account.Account;
import mrt.school.core.account.AccountDao;
import mrt.school.core.account.AccountProvider;
import mrt.school.core.password.IPassword;
import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.personal.data.PersonalDataDao;
import mrt.school.core.role.Role;
import mrt.school.core.role.RoleDao;

@Controller
public class AccountController {

	@Autowired
	private AccountProvider accountProvider;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private PersonalDataDao personsDao;

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private IPassword iPassword;

	@RequestMapping(value = "admin/account/manager", method = RequestMethod.GET)
	public ModelAndView accountList() {

		ModelAndView mav = new ModelAndView("admin/accountManager");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("status", accountProvider.getloggedAccount().getRole().getStatus());
		mav.addObject("accounts", accountDao.getAll());
		mav.addObject("account", new Account());
		mav.addObject("roles", this.rolesMap());
		mav.addObject("persons", this.personsMap());
		return mav;

	}

	@RequestMapping(value = "/admin/account/{accountId}/delete", method = RequestMethod.GET)
	public String deleteAccount(@PathVariable Long accountId) {
		accountDao.deleteById(accountId);
		return "redirect:/admin/account/manager";
	}

	@RequestMapping(value = "/admin/account/{accountId}/editView", method = RequestMethod.GET)
	public ModelAndView editAccount(@PathVariable Long accountId) {
		ModelAndView mav = new ModelAndView("admin/accountEdit");
		mav.addObject("login", accountProvider.getloggedAccount().getLogin());
		mav.addObject("status", accountProvider.getloggedAccount().getRole().getStatus());
		mav.addObject("newAccount", new Account());
		mav.addObject("account", accountDao.getById(accountId));
		mav.addObject("roles", this.rolesMap());
		mav.addObject("persons", this.personsMap());
		return mav;
	}

	@RequestMapping(value = "/admin/account/add", method = RequestMethod.POST)
	public String addAccount(@ModelAttribute Account newAccount) {
		if (newAccount.getPassword().equalsIgnoreCase("true")) {
			newAccount.setPassword(iPassword.generatePassword());
		}
		accountDao.saveOrUpdate(newAccount);
		return "redirect:/admin/account/manager";
	}

	@RequestMapping(value = "/admin/account/edit", method = RequestMethod.POST)
	public String editAccount(@ModelAttribute Account newAccount) {

		Account actualAccount = accountDao.getById(newAccount.getId());
		actualAccount.setActiveAccount(newAccount.isActiveAccount());
		actualAccount.setLogin(newAccount.getLogin());
		actualAccount.setRole(newAccount.getRole());
		if (newAccount.getPassword() != null && newAccount.getPassword().equalsIgnoreCase("true")) {
			actualAccount.setPassword(iPassword.generatePassword());
		}
		accountDao.saveOrUpdate(actualAccount);
		return "redirect:/admin/account/manager";
	}

	private Map<Long, String> rolesMap() {
		List<Role> roles = roleDao.getAll();
		Map<Long, String> rolesMap = new HashMap<>();

		for (Role role : roles) {
			if (!role.getStatus().equalsIgnoreCase("ROLE_SUPERADMIN")) {
				if (accountProvider.getloggedAccount().getRole().getStatus().equalsIgnoreCase("ROLE_ADMIN")
						&& role.getStatus().equalsIgnoreCase("ROLE_ADMIN")) {
					continue;
				}

				rolesMap.put(role.getId(), role.getStatus());
			}
		}
		return rolesMap;
	}

	private Map<Long, String> personsMap() {
		List<PersonalData> persons = personsDao.getAll();
		Map<Long, String> personsMap = new HashMap<>();

		for (PersonalData personalData : persons) {
			personsMap.put(personalData.getId(),(
					personalData.getId() + ": " + personalData.getName() + " " + personalData.getSurname()));
		}

		return personsMap;
	}
}
