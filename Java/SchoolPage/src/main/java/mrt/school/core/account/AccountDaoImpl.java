package mrt.school.core.account;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;


@Repository
public class AccountDaoImpl extends AbstractBaseDao<Account, Long> implements AccountDao {

	@Override
	protected Class<Account> supports() {
		// TODO Auto-generated method stub
		return Account.class;
	}

	@Override
	public Account getByLogin(String login) {

		return (Account) currentSession().createCriteria(Account.class).add(Restrictions.eq("login", login))
				.uniqueResult();
	}

	@Override
	public void deleteById(Long id) {
		currentSession()
		.createQuery("delete from Account where id = :accountId")
		.setParameter("accountId", id)
		.executeUpdate();	
		
	}

}
