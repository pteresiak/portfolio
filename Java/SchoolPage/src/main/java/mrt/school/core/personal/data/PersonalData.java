package mrt.school.core.personal.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mrt.school.core.personal.status.PersonalStatus;
import mrt.school.core.school.group.SchoolClass;

@Entity
@Table(name = "personal_data")
public class PersonalData {
	@Id
	@GeneratedValue(generator = "personal_seq")
	@SequenceGenerator(name = "personal_seq", sequenceName = "personal_seq")
	private Long id;
	@Column
	private String name;
	@Column
	private String surname;
	@Column
	private String email;

	@ManyToOne
	@JoinColumn(name = "status")
	private PersonalStatus personalStatus;

	@ManyToOne
	@JoinColumn(name = "class")
	private SchoolClass schoolClass;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public PersonalStatus getPersonalStatus() {
		return personalStatus;
	}

	public void setPersonalStatus(PersonalStatus personalStatus) {
		this.personalStatus = personalStatus;
	}

	
	
	
	public SchoolClass getSchoolClass() {
		return schoolClass;
	}

	public void setSchoolClass(SchoolClass schoolClass) {
		this.schoolClass = schoolClass;
	}

	@Override
	public String toString() {
		return "PersonalData [id=" + id + ", name=" + name + ", secondName=" + ", surname=" + surname + ", email="
				+ email + "]";
	}
}
