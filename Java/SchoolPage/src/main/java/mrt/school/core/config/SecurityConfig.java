package mrt.school.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import mrt.school.core.account.*;



@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService springUserDetailsService;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(springUserDetailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

	    http
	    	.authorizeRequests()
	    		.antMatchers("/login/**").permitAll()
	    		.antMatchers("/admin/**").access(" hasRole('ROLE_ADMIN') or hasRole('ROLE_SUPERADMIN') ")
	    		.antMatchers("/director/**").access(" hasRole('ROLE_DIRECTOR') or hasRole('ROLE_SUPERADMIN') ")
	    		.antMatchers("/secretariat/**").access(" hasRole('ROLE_SUPERADMIN') or hasRole('ROLE_SECRETARIAT') or hasRole('ROLE_DIRECTOR')")    		
	    		.antMatchers("/teacher/**").access(" hasRole('ROLE_TEACHER') or hasRole('ROLE_SUPERADMIN') ")
	    		.antMatchers("/student/**").access(" hasRole('ROLE_STUDENT') or hasRole('ROLE_SUPERADMIN') ")
	
	    		.anyRequest().authenticated()	    	
	    	.and()
	    		.formLogin()
	    		.loginPage("/login").failureUrl("/login?error")
	    		.usernameParameter("username")
	    		.passwordParameter("password")
	    		.successHandler(successHandler())
	    	.and()
	    		.logout()
	    		.logoutSuccessUrl("/login?logout")
	    	.and()
	    		.exceptionHandling()
	    		.accessDeniedPage("/403")
	    	.and()
	    		.csrf().disable();
	}

	@Bean
	AuthenticationSuccessHandler successHandler() {
		return new AccountLoginSuccessHandler();
	}
	


	
	
}