package mrt.school.core.personal.status;

import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;
import mrt.school.core.personal.data.PersonalDataDao;

@Repository
public class PersonalStatusDaoImpl extends AbstractBaseDao<PersonalStatus, Long> implements PersonalStatusDao{

	@Override
	protected Class<PersonalStatus> supports() {
		// TODO Auto-generated method stub
		return PersonalStatus.class;
	}

}
