package mrt.school.core.personal.status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "personal_status")
public class PersonalStatus {
	@Id
	@GeneratedValue(generator = "personal_status_seq")
	@SequenceGenerator(name = "personal_status_seq", sequenceName = "personal_status_seq")
	private Long id;

	@Column
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PersonalStatus [id=" + id + ", status=" + status + "]";
	}

}
