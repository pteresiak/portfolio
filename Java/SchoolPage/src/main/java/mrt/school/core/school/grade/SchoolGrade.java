package mrt.school.core.school.grade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.subject.actual.ActualSubject;

@Entity
@Table(name = "school_grade")
public class SchoolGrade {
	@Id
	@GeneratedValue(generator = "school_grade_seq")
	@SequenceGenerator(name = "school_grade_seq", sequenceName = "school_grade_seq")
	private Long id;

	@Column
	private Double grade;

	@ManyToOne
	@JoinColumn(name = "type_of_grade")
	private TypeOfGrade typeOfGrade;

	@ManyToOne
	@JoinColumn(name = "student")
	private PersonalData student;

	@ManyToOne
	@JoinColumn(name = "subject")
	private ActualSubject subject;

	@Column
	private String info;

	private String symbolGrade;
	private int symbolId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getGrade() {

		return grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
		convertFromGrade();
	}

	public TypeOfGrade getTypeOfGrade() {
		return typeOfGrade;
	}

	public void setTypeOfGrade(TypeOfGrade typeOfGrade) {
		this.typeOfGrade = typeOfGrade;
	}

	public PersonalData getStudent() {
		return student;
	}

	public void setStudent(PersonalData student) {
		this.student = student;
	}

	public ActualSubject getSubject() {
		return subject;
	}

	public void setSubject(ActualSubject subject) {
		this.subject = subject;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getSymbolId() {
		return symbolId;
	}

	public void setSymbolId(int symbolId) {
		this.symbolId = symbolId;
		this.convertFromSymbolId();
	}

	public String getSymbolGrade() {
		return symbolGrade;
	}

	public SchoolGrade clone() {

		SchoolGrade cloneSchoolGrade = new SchoolGrade();
		cloneSchoolGrade.setGrade(this.grade);
		cloneSchoolGrade.setId(this.id);
		cloneSchoolGrade.setStudent(this.student);
		cloneSchoolGrade.setSubject(this.subject);
		cloneSchoolGrade.setTypeOfGrade(this.typeOfGrade);

		return cloneSchoolGrade;
	}

	@Override
	public String toString() {
		return "SchoolGrade [id=" + id + ", grade=" + grade + ", typeOfGrade=" + typeOfGrade + ", student=" + student
				+ ", subject=" + subject + ", info=" + info + "]";
	}

	private void convertFromSymbolId() {
		switch (this.symbolId) {
		case 1:
			this.grade = 6.0;
			this.symbolGrade = "6";
			break;
		case 2:
			this.grade = 5.75;
			this.symbolGrade = "6-";
			break;
		case 3:
			this.grade = 5.5;
			this.symbolGrade = "5+";
			break;
		case 4:
			this.grade = 5.0;
			this.symbolGrade = "5";
			break;
		case 5:
			this.grade = 4.75;
			this.symbolGrade = "5-";
			break;
		case 6:
			this.grade = 4.5;
			this.symbolGrade = "4+";
			break;
		case 7:
			this.grade = 4.0;
			this.symbolGrade = "4";
			break;
		case 8:
			this.grade = 3.75;
			this.symbolGrade = "4-";
			break;
		case 9:
			this.grade = 3.5;
			this.symbolGrade = "3+";
			break;
		case 10:
			this.grade = 3.0;
			this.symbolGrade = "3";
			break;
		case 11:
			this.grade = 2.0;
			this.symbolGrade = "2";
			break;
		case 12:
			this.grade = 1.0;
			this.symbolGrade = "1";
			break;
		default:
			this.grade = 0.0;
			this.symbolGrade = "";
			break;
		}
	}

	private void convertFromGrade() {
		switch (this.grade.toString()) {
		case "6.0":
			this.symbolId = 1;
			this.symbolGrade = "6";
			break;
		case "5.75":
			this.symbolId = 2;
			this.symbolGrade = "6-";
			break;
		case "5.5":
			this.symbolId = 3;
			this.symbolGrade = "5+";
			break;
		case "5.0":
			this.symbolId = 4;
			this.symbolGrade = "5";
			break;
		case "4.75":
			this.symbolId = 5;
			this.symbolGrade = "5-";
			break;
		case "4.5":
			this.symbolId = 6;
			this.symbolGrade = "4+";
			break;
		case "4.0":
			this.symbolId = 7;
			this.symbolGrade = "4";
			break;
		case "3.75":
			this.symbolId = 8;
			this.symbolGrade = "4-";
			break;
		case "3.5":
			this.symbolId = 9;
			this.symbolGrade = "3+";
			break;
		case "3.0":
			this.symbolId = 10;
			this.symbolGrade = "3";
			break;
		case "2.0":
			this.symbolId = 11;
			this.symbolGrade = "2";
			break;
		case "1.0":
			this.symbolId = 12;
			this.symbolGrade = "1";
			break;
		default:
			this.symbolId = 0;
			this.symbolGrade = this.grade.toString();
			break;
		}
	}
}
