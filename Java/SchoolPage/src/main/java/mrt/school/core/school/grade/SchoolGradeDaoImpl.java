package mrt.school.core.school.grade;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;
import mrt.school.core.subject.actual.ActualSubject;

@Repository
public class SchoolGradeDaoImpl extends AbstractBaseDao<SchoolGrade, Long> implements SchoolGradeDao {

	@Override
	protected Class<SchoolGrade> supports() {
		// TODO Auto-generated method stub
		return SchoolGrade.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchoolGrade> getSchoolGradeByStudentByActualSubject(Long studentId, Long actualSubjectId) {

		return currentSession().createCriteria(SchoolGrade.class).add(Restrictions.eq("student.id", studentId))
				.add(Restrictions.eq("subject.id", actualSubjectId)).list();

	}

}
