package mrt.school.core.school.group;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "school_class")
public class SchoolClass {
	@Id
	@GeneratedValue(generator = "school_class_seq")
	@SequenceGenerator(name = "school_class_seq", sequenceName = "school_class_seq")
	private Long id;

	@Column
	private String name;

	@Column
	private String specialization;

	@Column
	private String info;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "SchoolClass [id=" + id + ", name=" + name + ", specialization=" + specialization + ", info=" + info
				+ "]";
	}

}
