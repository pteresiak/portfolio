package mrt.school.core.school.grade;

import mrt.school.core.basedao.BaseDao;

public interface TypeOfGradeDao   extends BaseDao<TypeOfGrade, Long>{

}
