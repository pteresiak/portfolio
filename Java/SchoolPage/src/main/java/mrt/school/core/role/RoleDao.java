package mrt.school.core.role;

import mrt.school.core.account.Account;
import mrt.school.core.basedao.BaseDao;

public interface RoleDao  extends BaseDao<Role, Long>{

}
