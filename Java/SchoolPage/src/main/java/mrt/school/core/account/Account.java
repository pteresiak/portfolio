package mrt.school.core.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.role.Role;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(generator = "account_seq")
	@SequenceGenerator(name = "account_seq", sequenceName = "account_seq")
	private Long id;
	@Column
	private String  login;
	@Column
	private String 	password;
	@Column(name = "active_account")
	private boolean activeAccount;
	
	@ManyToOne
	@JoinColumn(name = "status")
	private Role role;
	
	@ManyToOne
	@JoinColumn(name = "person")
	private PersonalData personalData;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
//	public void setPassword(boolean passwordBoolean) {
//		this.password = String.valueOf(passwordBoolean) ;
//	}

	public boolean isActiveAccount() {
		return activeAccount;
	}

	public void setActiveAccount(boolean activeAccount) {
		this.activeAccount = activeAccount;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public PersonalData getPersonalData() {
		return personalData;
	}

	public void setPersonalData(PersonalData personalData) {
		this.personalData = personalData;
	}




	
	
	
	
}
