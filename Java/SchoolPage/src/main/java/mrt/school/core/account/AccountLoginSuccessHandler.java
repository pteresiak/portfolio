package mrt.school.core.account;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class AccountLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler{

	
	@Autowired
	private AccountProvider accountProvider;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		accountProvider.saveLoggedAccount(authentication.getName());		
		super.onAuthenticationSuccess(request, response, authentication);
		
	}
	
}
