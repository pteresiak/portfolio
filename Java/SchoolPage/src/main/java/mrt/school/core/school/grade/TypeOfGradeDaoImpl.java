package mrt.school.core.school.grade;

import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;

@Repository
public class TypeOfGradeDaoImpl extends AbstractBaseDao<TypeOfGrade, Long> implements TypeOfGradeDao {

	@Override
	protected Class<TypeOfGrade> supports() {
		// TODO Auto-generated method stub
		return TypeOfGrade.class;
	}

}
