package mrt.school.core.school.group;

import mrt.school.core.basedao.BaseDao;

public interface SchoolClassDao extends BaseDao<SchoolClass, Long>{
	
void deleteById(long id);
}
