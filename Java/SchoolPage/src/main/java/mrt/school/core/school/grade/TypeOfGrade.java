package mrt.school.core.school.grade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "type_of_grade")
public class TypeOfGrade {

	@Id
	@GeneratedValue(generator = "type_of_grade_seq")
	@SequenceGenerator(name = "type_of_grade_seq", sequenceName = "type_of_grade_seq")
	private Long id;
	@Column(name="type_grade")
	private String type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "TypeOfGrade [id=" + id + ", type=" + type + "]";
	}

}
