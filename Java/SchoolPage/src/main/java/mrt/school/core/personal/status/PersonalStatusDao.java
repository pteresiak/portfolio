package mrt.school.core.personal.status;

import mrt.school.core.basedao.BaseDao;

public interface PersonalStatusDao extends BaseDao<PersonalStatus, Long>{

}
