package mrt.school.core.subject.actual;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import mrt.school.core.personal.data.PersonalData;
import mrt.school.core.school.group.SchoolClass;
import mrt.school.core.subject.list.SubjectList;

@Entity
@Table(name="actual_subject")
public class ActualSubject {
	@Id
	@GeneratedValue(generator = "actual_subject_seq")
	@SequenceGenerator(name = "actual_subject_seq", sequenceName = "actual_subject_seq")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "subject")
	private SubjectList subject;

	@ManyToOne
	@JoinColumn(name = "teacher")
	private PersonalData teacher;

	@ManyToOne
	@JoinColumn(name = "class")
	private SchoolClass schoolClass;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectList getSubject() {
		return subject;
	}

	public void setSubject(SubjectList subject) {
		this.subject = subject;
	}



	public PersonalData getTeacher() {
		return teacher;
	}

	public void setTeacher(PersonalData teacher) {
		this.teacher = teacher;
	}

	public SchoolClass getSchoolClass() {
		return schoolClass;
	}

	public void setSchoolClass(SchoolClass schoolClass) {
		this.schoolClass = schoolClass;
	}

	@Override
	public String toString() {
		return "ActualSubject [id=" + id + ", subject=" + subject + ", teacher=" + teacher + ", schoolClass="
				+ schoolClass + "]";
	}

}
