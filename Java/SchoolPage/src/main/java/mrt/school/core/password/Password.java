package mrt.school.core.password;

import java.util.ArrayList;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class Password implements IPassword {
	private static List<String> charList;

	private static void initCharList() {
		Password.charList = new ArrayList<String>();

		for (char a = 'a'; a < 'z'; a++) {
			Password.charList.add(String.valueOf(a));
		}
		for (char a = 'A'; a < 'Z'; a++) {
			Password.charList.add(String.valueOf(a));
		}
		for (int a = 0; a < 10; a++) {
			Password.charList.add(String.valueOf(a));
		}
	}

	@Override
	public String generatePassword() {
		if (Password.charList == null) {
			Password.initCharList();
		}
		StringBuilder sb = new StringBuilder("");
		int randomCharIndex = 0;
		for (int i = 0; i <= 10; i++) {
			randomCharIndex = (int) (Math.random() * Password.charList.size());
			sb.append(Password.charList.get(randomCharIndex));
		}
		return sb.toString();
	}

}
