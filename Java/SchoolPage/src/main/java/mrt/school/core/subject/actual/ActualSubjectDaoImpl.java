package mrt.school.core.subject.actual;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;
import mrt.school.core.personal.data.PersonalData;

@Repository
public class ActualSubjectDaoImpl extends AbstractBaseDao<ActualSubject, Long> implements ActualSubjectDao {

	@Override
	protected Class<ActualSubject> supports() {

		return ActualSubject.class;
	}

	@Override
	public ActualSubject getActualSubject(Long subjectId, Long teacherId, Long schoolClassId) {
		List<ActualSubject> aS = currentSession().createCriteria(ActualSubject.class)
				.add(Restrictions.eq("subject.id", subjectId)).add(Restrictions.eq("teacher.id", teacherId))
				.add(Restrictions.eq("schoolClass.id", schoolClassId)).list();

		if (aS.size() == 0) {
			return null;
		}

		return aS.get(0);

	}

}
