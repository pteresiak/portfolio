package mrt.school.core.personal.data;

import java.util.List;

import mrt.school.core.basedao.BaseDao;

public interface PersonalDataDao extends BaseDao<PersonalData, Long> {
	void deleteById(Long id);
	
	PersonalData getTeacherBySchoolClassId(Long schoolClassId);
	List<PersonalData> getStudentsListBySchoolClassId(Long schoolClass);

}
