package mrt.school.core.account;

public interface AccountProvider {

	Account getloggedAccount();
	
	void saveLoggedAccount(String login);
}
