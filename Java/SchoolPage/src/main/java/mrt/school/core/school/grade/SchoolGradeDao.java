package mrt.school.core.school.grade;

import java.util.List;

import mrt.school.core.basedao.BaseDao;

public interface SchoolGradeDao extends BaseDao<SchoolGrade, Long>{

	List<SchoolGrade> getSchoolGradeByStudentByActualSubject(Long studnetId, Long actualSubjectId);
	
}
