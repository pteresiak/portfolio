package mrt.school.core.role;


import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;

@Repository
public class RoleDaoImpl extends AbstractBaseDao<Role, Long> implements RoleDao{

	@Override
	protected Class<Role> supports() {
		// TODO Auto-generated method stub
		return Role.class;
	}


}
