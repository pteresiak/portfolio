package mrt.school.core.subject.list;

import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;

@Repository
public class SubjectListDaoImpl extends AbstractBaseDao<SubjectList, Long> implements SubjectListDao {

	@Override
	protected Class<SubjectList> supports() {
		// TODO Auto-generated method stub
		return SubjectList.class;
	}

}
