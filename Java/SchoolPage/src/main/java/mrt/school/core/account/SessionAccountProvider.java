package mrt.school.core.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class SessionAccountProvider implements AccountProvider {

	@Autowired
	private AccountDao accountDao;

	private Account account;

	@Override
	public Account getloggedAccount() {

		return account;
	}

	@Override
	@Transactional
	public void saveLoggedAccount(String login) {
		account = accountDao.getByLogin(login);

	}

}
