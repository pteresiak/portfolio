package mrt.school.core.personal.data;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;
import mrt.school.pojo.SchoolClassViewManager;

@Repository
public class PersonalDataDaoImpl extends AbstractBaseDao<PersonalData, Long> implements PersonalDataDao {

	@Override
	protected Class<PersonalData> supports() {
		// TODO Auto-generated method stub
		return PersonalData.class;
	}

	@Override
	public void deleteById(Long id) {
		currentSession().createQuery("delete FROM PersonalData WHERE id = :personId").setParameter("personId", id)
				.executeUpdate();

	}

	@Override
	public PersonalData getTeacherBySchoolClassId(Long schoolClassId) {
		// return (PersonalData) currentSession()
		// .createQuery("FROM PersonalData WHERE 'class' = :classId")
		// .setLong("classId", schoolClassId).uniqueResult();

		List<PersonalData> pD = currentSession().createCriteria(PersonalData.class)
				.add(Restrictions.eq("schoolClass.id", schoolClassId)).add(Restrictions.eq("personalStatus.id", 2L))
				.list();

		if (pD.size() == 0) {
			return null;
		}

		return pD.get(0);

	}

	@Override
	public List<PersonalData> getStudentsListBySchoolClassId(Long schoolClass) {

		return currentSession().createCriteria(PersonalData.class).add(Restrictions.eq("schoolClass.id", schoolClass))
				.add(Restrictions.eq("personalStatus.id", 3L)).list();
	}

}
