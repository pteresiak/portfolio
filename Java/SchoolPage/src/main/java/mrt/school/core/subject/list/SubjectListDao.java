package mrt.school.core.subject.list;

import mrt.school.core.basedao.BaseDao;

public interface SubjectListDao extends BaseDao<SubjectList, Long> {

}
