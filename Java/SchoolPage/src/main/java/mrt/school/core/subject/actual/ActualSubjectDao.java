package mrt.school.core.subject.actual;

import mrt.school.core.basedao.BaseDao;

public interface ActualSubjectDao extends BaseDao<ActualSubject, Long> {
	ActualSubject getActualSubject(Long subjectId, Long teacherId, Long schoolClassId);
}
