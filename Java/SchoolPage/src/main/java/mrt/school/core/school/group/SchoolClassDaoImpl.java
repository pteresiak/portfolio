package mrt.school.core.school.group;

import org.springframework.stereotype.Repository;

import mrt.school.core.basedao.AbstractBaseDao;

@Repository
public class SchoolClassDaoImpl extends AbstractBaseDao<SchoolClass, Long> implements SchoolClassDao{

	@Override
	protected Class<SchoolClass> supports() {
		// TODO Auto-generated method stub
		return SchoolClass.class;
	}

	@Override
	public void deleteById(long id) {
		currentSession().createQuery("delete FROM SchoolClass WHERE id = :schoolClassId").setParameter("schoolClassId", id)
		.executeUpdate();
		
	}
	

}
