package mrt.school.core.account;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mrt.school.core.role.Role;

@Component
public class AccountDetailService implements UserDetailsService {

	@Autowired
	private AccountDao accountDao;

	@Transactional
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Account account = accountDao.getByLogin(login);



		if (account == null) {
			throw new UsernameNotFoundException("There is no user with login " + login);
		}
		List<GrantedAuthority> authorities = buildUserAuthority(account.getRole());
		return buildUserForAuthentication(account, authorities);
	}

	private org.springframework.security.core.userdetails.User buildUserForAuthentication(Account account,
			List<GrantedAuthority> authorities) {
		return new org.springframework.security.core.userdetails.User(account.getLogin(), account.getPassword(),
				account.isActiveAccount(), true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Role accountRole) {
		Set<GrantedAuthority> roles = new HashSet<>();
		roles.add(new SimpleGrantedAuthority(accountRole.getStatus()));

		return new ArrayList<>(roles);
	}

}
