package mrt.school.core.account;

import mrt.school.core.basedao.BaseDao;

public interface AccountDao extends BaseDao<Account, Long>{

	Account getByLogin(String login);
	void deleteById(Long id);
	
}
