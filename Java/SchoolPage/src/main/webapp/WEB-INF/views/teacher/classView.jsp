<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>



<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Class View</title>
</head>
<body>

	<h1>${login}</h1>
	<h1>ClassView</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>

	<c:choose>
		<c:when test="${isUserEducatorFlag==false}">
			<div>
				<h1>You are not any Educator Class</h1>
			</div>
		</c:when>
		<c:otherwise>
			<div>
				<h3>School class:${schoolClass.name}</h3>
				<h3>Specialization: ${schoolClass.specialization}</h3>
				<h3>Info: ${schoolClass.info}</h3>
				<br>
				<br>


				<table border="3" bordercolor="red">
					<tr>
						<th colspan="4"><h4>List of Students</h4></th>
					</tr>

					<tr>
						<th><h4>Id</h4></th>
						<th><h4>Name</h4></th>
						<th><h4>Surname</h4></th>
						<th><h4>E-mail</h4></th>
					</tr>


					<c:forEach var="student" items="${students}">
						<tr>
							<td><h4>${student.id}</h4></td>
							<td><h4>${student.name}</h4></td>
							<td><h4>${student.surname}</h4></td>
							<td><h4>${student.email}</h4></td>
						</tr>

					</c:forEach>

				</table>

			</div>

		</c:otherwise>
	</c:choose>


</body>
</html>