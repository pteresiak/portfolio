<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Grade Edit</title>
</head>
<body>

	<h1>${login}</h1>

	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>
	<h1>${subject.subject.name}</h1>
	<h2>Class:&nbsp;&nbsp; ${subject.schoolClass.name}</h2>
	<br>
	<br>

	<div>
		<table border="6" bordercolor="red">
			<tr>
				<th><h3>Id</h3></th>
				<th><h3>Student</h3></th>
				<c:forEach var="typeOfGrade" items="${titleTypeOfGrade}">
					<th><h3>${typeOfGrade.type}</h3></th>
				</c:forEach>
				<th><h3>Grade</h3></th>
				<th><h3>Info</h3></th>
				<th><h3>Operations</h3></th>

			</tr>
			<c:forEach var="studentGradePOJO" items="${studentsGrade}">
				<tr>
					<td><h4>${studentGradePOJO.student.id}</h4></td>
					<td><h4>${studentGradePOJO.student.name}&nbsp;${studentGradePOJO.student.surname}</h4></td>


					<c:forEach var="studentGrade"
						items="${studentGradePOJO.schoolGradeList}">
						<c:choose>
							<c:when test="${studentGrade==null}">
								<td></td>
							</c:when>
							<c:otherwise>
								<td><h4>${studentGrade.symbolGrade}</h4></td>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:url var="addGradeURL" value="/teacher/grade/addGrade" />
					<form:form method="POST" action="${addGradeURL}"
						modelAttribute="newGrade">
						<form:hidden path="student.id"
							value="${studentGradePOJO.student.id}" />
						<form:hidden path="subject.id" value="${subject.id}" />
						<td><form:select path="typeOfGrade.id">
								<form:options items="${titleTypeOfGradeMap}" />
							</form:select> <br> <form:select path="symbolId">
								<form:options items="${gradeMap}" />
							</form:select></td>
						<td><form:textarea path="info" /></td>
						<td><input type="submit" value="Add" /></td>
					</form:form>



				</tr>
			</c:forEach>
		</table>

	</div>



</body>
</html>