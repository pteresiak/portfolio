<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Grade Manager</title>
</head>
<body>
	<h1>${login}</h1>
	<h1>Grade Manager</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>


	<c:choose>
		<c:when test="${isSubjectListFlag==false}">

			<div>
				<h4>You do not teach any subject</h4>
			</div>
		</c:when>
		<c:otherwise>
			<div>
				<table border="3" bordercolor="red">
					<tr>
						<th><h3>Subject</h3></th>
						<th><h3>School Class</h3></th>
						<th><h3>Operations</h3></th>
					</tr>
					<c:forEach var="subject" items="${subjectList}">
						<tr>
							<td><h4>${subject.subject.name}</h4></td>
							<td><h4>${subject.schoolClass.name}</h4></td>
							<td><c:url var="editGradeURL"
									value="/teacher/grade/${subject.id}/edit" /> <form:form
									method="GET" action="${editGradeURL}">
									<input type="submit" value="Edit" />
								</form:form></td>
						</tr>
					</c:forEach>
				</table>


			</div>

		</c:otherwise>
	</c:choose>


</body>
</html>