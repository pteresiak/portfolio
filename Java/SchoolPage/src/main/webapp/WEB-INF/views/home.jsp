<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<body>

	<c:url value="admin/account/manager" var="accountListUrl" />
	<c:url value="/secretariat/personalData/manager"
		var="personalManagerURL" />
	<c:url value="/secretariat/class/manager" var="schoolClassManagerURL" />
	<c:url value="/secretariat/subject/manager" var="subjectManagerURL" />
	<c:url value="/teacher/class/view" var="classViewURL" />
	<c:url value="/teacher/grade/manager" var="gradeManagerURL" />
	<c:url value="/student/grade/view" var="gradeViewURL" />
	<c:url value="/logout" var="logoutUrl" />
	<h2>Hello : ${login} !</h2>
	<h3>Choose action:</h3>

	<ul>

		<li><a href="${accountListUrl}">Account Manager</a></li>
		<li><a href="${personalManagerURL}">Personal Manager </a></li>
		<li><a href="${schoolClassManagerURL}">Class Manager </a></li>
		<li><a href="${subjectManagerURL}">Subject Manager </a></li>
		<li><a href="${classViewURL}">School Class View</a></li>
		<li><a href="${gradeManagerURL}">Grade Manager</a></li>
		<li><a href="${gradeViewURL}">Grade View</a></li>
		<li><form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form></li>
	</ul>

</body>
</html>