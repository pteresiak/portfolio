<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Manager</title>
</head>
<body>

	<h1>${login}</h1>
	<h1>Edit Account</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>

	<table border="6">
		<tr>
			<th><h3>ID</h3></th>
			<th><h3>LOGIN</h3></th>
			<th><h3>PASSWORD</h3></th>
			<th><h3>ACTIVE</h3></th>
			<th><h3>STATUS</h3></th>
			<th><h3>PERSON ID</h3></th>
		</tr>
		<tr>

			<td colspan="6"><h3>Actual Settings</h3></td>
		</tr>
		<tr>
			<th><h3>${account.id}</h3></th>
			<th><h3>${account.login}</h3></th>
			<th><c:choose>
					<c:when test="${'ROLE_SUPERADMIN'==status}">
						<h4>${account.password}</h4>
					</c:when>
					<c:otherwise>
						<h4>**************</h4>
					</c:otherwise>
				</c:choose></th>


			<th><h3>${account.activeAccount}</h3></th>
			<th><h3>${account.role.status}</h3></th>
			<th><h3>${account.personalData.id}</h3></th>

		</tr>
		<tr>

			<td colspan="6"><h3>New Settings</h3></td>
		</tr>
		<spring:url value="/admin/account/edit" var="editAccountURL" />
		<form:form method="POST" modelAttribute="newAccount"
			action="${editAccountURL}">
			<tr>
				<td><h4>
						<form:hidden path="id" value="${account.id}" />
						${account.id}
					</h4></td>
				<td><h4>
						<form:input value="${account.login}" path="login" />
					</h4></td>
				<td><h4>
						Generate New Password
						<form:checkbox value="true" path="password" />
					</h4></td>
				<td><h4>
						true
						<form:checkbox path="activeAccount" />
					</h4></td>
				<td><h4>
						<form:select path="role.id">
							<form:options items="${roles}" />
						</form:select>
					</h4></td>
				<td><h4>
						<form:button type="submit">Update</form:button>

					</h4></td>
			<tr>
		</form:form>

	</table>

</body>
</html>