<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Manager</title>
</head>
<body>


	<h1>${login}</h1>
	<h1>Account Manager</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li><form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form></li>
	</ul>
	<table bordercolor="red" border="3">

		<tr>
			<th><h3>ID</h3></th>
			<th><h3>LOGIN</h3></th>
			<th><h3>PASSWORD</h3></th>
			<th><h3>ACTIVE</h3></th>
			<th><h3>STATUS</h3></th>
			<th><h3>PERSON</h3></th>
			<th><h3>OPERATIONS</h3></th>
		</tr>
		<spring:url value="/admin/account/add" var="addAccountURL" />
		<form:form method="POST" modelAttribute="account"
			action="${addAccountURL}">
			<tr>
				<td><h4>X</h4></td>
				<td><h4>
						<form:input value="New Login" path="login" />
					</h4></td>


				<td><c:choose>
						<c:when test="${'ROLE_SUPERADMIN'==status}">
							<form:input value="New Password" path="password" />
						</c:when>
						<c:otherwise>
						
						Auto Generate New Password
						<form:hidden value="true" path="password" />
						</c:otherwise>
					</c:choose></td>



				<td><h4>
						true
						<form:checkbox path="activeAccount" />
					</h4></td>
				<td><h4>
						<form:select path="role.id">

							<form:options items="${roles}" />

						</form:select>
					</h4></td>
				<td><h4>
						<form:select path="personalData.id">
							<form:options items="${persons}" />
						</form:select>
					</h4></td>


				<td><h4>
						<ul>
							<li><form:button type="submit">Add</form:button></li>
						</ul>



					</h4></td>
			<tr>
		</form:form>
		<c:forEach var="account" items="${accounts}">
			<tr>
				<td>
					<h4>${account.id}</h4>
				</td>
				<td>
					<h4>${account.login}</h4>
				</td>
				<td><c:choose>
						<c:when test="${'ROLE_SUPERADMIN'==status}">
							<h4>${account.password}</h4>
						</c:when>
						<c:otherwise>
							<h4>**************</h4>
						</c:otherwise>
					</c:choose></td>
				<td>
					<h4>${account.activeAccount}</h4>
				</td>
				<td>
					<h4>${account.role.status}</h4>
				</td>
				<td><c:choose>
						<c:when test="${account.personalData!=null}">
							<h4>${account.personalData.id}</h4>
						</c:when>
						<c:otherwise>
							<h4>NULL</h4>
						</c:otherwise>
					</c:choose></td>
				</td>


				<td><h4>
						<ul>
							<c:if
								test="${!status.equals(account.role.status) && 'ROLE_SUPERADMIN'!=account.role.status}">

								<spring:url value="/admin/account/${account.id}/editView"
									var="accountEditURL" />
								<li><form:form method="get" action="${accountEditURL}">

										<input type="submit" value="Edit" />
									</form:form></li>


								<spring:url value="/admin/account/${account.id}/delete"
									var="accountDeleteURL" />
								<li><form:form method="get" action="${accountDeleteURL}">

										<input type="submit" value="Delete" />
									</form:form></li>

							</c:if>
						</ul>
					</h4></td>
			</tr>




		</c:forEach>
	</table>
</body>
</html>