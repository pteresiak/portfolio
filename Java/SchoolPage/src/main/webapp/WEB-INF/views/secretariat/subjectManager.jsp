<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Subject Manager</title>
</head>
<body>
	<h1>${login}</h1>
	<h1>Subject Manager</h1>
	<c:url value="/" var="homeAction" />
	<c:url value="/secretariat/subject/addView" var="subjectListViewURL" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>

		<li>
			<h2>
				<a href="${homeAction}">Home</a>
			</h2>
		</li>
		<li><h2>
				<a href="${subjectListViewURL}">Add Subject</a>
			</h2></li>

		<li>

			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>

		</li>

	</ul>


	<div>
		<table border="3" bordercolor="red">
			<tr>
				<th><h4>Subject</h4></th>
				<th><h4>Teacher</h4></th>
				<th><h4>Class</h4></th>
				<th><h4>Operations</h4></th>
			</tr>

			<tr>
				<c:url value="/secretariat/subject/addActualSubject"
					var="addActualSubjectURL"></c:url>
				<form:form method="POST" action="${addActualSubjectURL}"
					modelAttribute="newActualSubject">
					<td><form:select path="subject.id">
							<form:options items="${subjectMap}" />
						</form:select></td>
					<td><form:select path="teacher.id">
							<form:options items="${teacherMap}" />
						</form:select></td>
					<td><form:select path="schoolClass.id">
							<form:options items="${schoolClassMap}" />
						</form:select></td>
					<td><form:button type="submit">Add</form:button></td>
				</form:form>

			</tr>


			<c:forEach var="actualSubject" items="${actualSubjectList}">
				<tr>
					<td><h3>${actualSubject.subject.name}</h3></td>
					<td><h3>
							${actualSubject.teacher.surname} &nbsp; ${actualSubject.teacher.name}</h3></td>
					<td><h3>${actualSubject.schoolClass.name}</h3></td>
				</tr>

			</c:forEach>

		</table>


	</div>


</body>
</html>