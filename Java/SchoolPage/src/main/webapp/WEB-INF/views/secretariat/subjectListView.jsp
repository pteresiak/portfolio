<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Subject Manager</title>
</head>
<body>
	<h1>${login}</h1>
	<h1>Subject Manager</h1>
	<c:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<c:url value="/secretariat/subject/manager" var="subjectManagerUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>

		<li><h2>
				<a href="${subjectManagerUrl}">Back</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>

	<div>
		<table border="6">
			<tr>
				<th>
					<h4>Subject name</h4>
				</th>
				<th>
					<h4>Operations</h4>
				</th>
			</tr>

			<tr>
				<spring:url value="/secretariat/subject/add" var="addSubjectURL"></spring:url>
				<form:form modelAttribute="newSubject" method="POST"
					action="${addSubjectURL }">
					<td><h4>
							<form:input path="name" />
						</h4></td>
					<td><h4>
							<form:button type="submit">Add</form:button>
						</h4></td>
				</form:form>
			</tr>

			<c:forEach var="subject" items="${subjects}">
				<tr>
					<td><h4>${subject.name}</h4></td>
					<td></td>

				</tr>
			</c:forEach>



		</table>
	</div>
</body>
</html>