<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Manager</title>
</head>
<body>

	<h1>${login}</h1>
	<h1>PersonalData Manager</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>

	<table bordercolor="red" border="3">
		<tr>
			<th><h3>Id</h3></th>
			<th><h3>Name</h3></th>
			<th><h3>Surname</h3></th>
			<th><h3>E-mail</h3></th>
			<th><h3>Status</h3></th>
			<th><h3>Operations</h3></th>
		</tr>

		<spring:url value="/secretariat/personalData/add"
			var="addPersonalDataURL" />
		<form:form method="POST" modelAttribute="newPersonalData"
			action="${addPersonalDataURL}">
			<tr>
				<td><h4>X</h4></td>
				<td><h4>
						<form:input value="New Name" path="name" />
					</h4></td>
				<td><h4>
						<form:input value="New Surname" path="surname" />
					</h4></td>
				<td><h4>
						<form:input value="exapmle@email.com" path="email" />
					</h4></td>

				<td>
					<h4>
						<form:select path="personalStatus.id">

							<form:options items="${personalStatusMap}" />

						</form:select>
					</h4>

				</td>

				<td><h4>
						<ul>
							<li><form:button type="submit">Add</form:button></li>
						</ul>
					</h4></td>
			<tr>
		</form:form>


		<c:forEach var="personalData" items="${personalDatas}">
			<tr>
				<td><h4>${personalData.id}</h4></td>
				<td><h4>${personalData.name}</h4></td>
				<td><h4>${personalData.surname}</h4></td>
				<td><h4>${personalData.email}</h4></td>
				<td><c:choose>
						<c:when test="${personalData.personalStatus!=null}">
							<h4>${personalData.personalStatus.status}</h4>
						</c:when>
						<c:otherwise>
							<h4>NULL</h4>
						</c:otherwise>
					</c:choose></td>
				<td><h4>
						<ul>
							<spring:url
								value="/secretariat/personalData/${personalData.id}/editView"
								var="personalDataEditURL" />
							<li><form:form method="get" action="${personalDataEditURL}">

									<input type="submit" value="Edit" />
								</form:form></li>
							<c:forEach var="id" items="${cantDelete}">
								<c:if test="${id == personalData.id || personalData.schoolClass!=null }">
									<c:set var="CanDeleteFlag" value="false">

									</c:set>

								</c:if>

							</c:forEach>

							<c:if test="${CanDeleteFlag.equals('true') }">

								<spring:url
									value="/secretariat/personalData/${personalData.id}/delete"
									var="personalDataDeleteURL" />
								<li><form:form method="get"
										action="${personalDataDeleteURL}">

										<input type="submit" value="Delete" />
									</form:form></li>
							</c:if>
							<c:set var="CanDeleteFlag" value="true">
							</c:set>


						</ul>
					</h4></td>
			</tr>


		</c:forEach>

	</table>


</body>
</html>