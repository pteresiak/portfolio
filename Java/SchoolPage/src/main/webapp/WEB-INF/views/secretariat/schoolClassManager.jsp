<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Class Manager</title>
</head>
<body>
	<h1>${login}</h1>
	<h1>Class Manager</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>

	<table border="3" bordercolor="red">
		<tr>
			<th><h3>Name</h3></th>
			<th><h3>Specialization</h3></th>
			<th><h3>Educator</h3></th>
			<th><h3>Numbers of students</h3></th>
			<th><h3>Info</h3></th>
			<th><h3>Operations</h3></th>

		</tr>

		<spring:url value="/secretariat/class/add" var="addClassURL" />
		<form:form method="POST" modelAttribute="newSchoolClass"
			action="${addClassURL}">
			<tr>

				<td><h4>
						<form:input value="0A" path="name" />
					</h4></td>
				<td><h4>
						<form:input value="New Specialization" path="specialization" />
					</h4></td>
				<td>Empty</td>
				<td>0</td>

				<td><h4>
						<form:textarea value="" path="info" />
					</h4></td>
				<td><h4>
						<ul>
							<li><form:button type="submit">Add</form:button></li>
						</ul>
					</h4></td>
			<tr>
		</form:form>







		<c:forEach var="schoolClassView" items="${classes}">
			<tr>
				<td><h3>${schoolClassView.schoolClass.name}</h3></td>
				<td><h3>${schoolClassView.schoolClass.specialization}</h3></td>
				<td><h3>${schoolClassView.educator.name} &nbsp; ${schoolClassView.educator.surname}</h3></td>
				<td><h3>${schoolClassView.numberOfStudents}</h3></td>
				<td><h3>${schoolClassView.schoolClass.info}</h3></td>
				<td><h4>
						<ul>
							<spring:url
								value="/secretariat/class/${schoolClassView.schoolClass.id}/editView"
								var="classEditURL" />
							<li><form:form method="get" action="${classEditURL}">

									<input type="submit" value="Edit" />
								</form:form></li>
							<c:forEach var="id" items="${cantDelete}">
								<c:if test="${id == schoolClassView.schoolClass.id}">
									<c:set var="CanDeleteFlag" value="false">

									</c:set>

								</c:if>

							</c:forEach>

							<c:if test="${CanDeleteFlag.equals('true') }">

								<spring:url
									value="/secretariat/class/${schoolClassView.schoolClass.id}/delete"
									var="classDeleteURL" />
								<li><form:form method="get"
										action="${classDeleteURL}">

										<input type="submit" value="Delete" />
									</form:form></li>
							</c:if>
							<c:set var="CanDeleteFlag" value="true">
							</c:set>


						</ul>
					</h4></td>
			</tr>


		</c:forEach>

	</table>



</body>
</html>