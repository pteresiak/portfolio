<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Class</title>
</head>
<body>
	<h1>${login}</h1>
	<h1>Edit SchoolClass</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>
	<div style="float: left">
		<table border="6">
			<tr>

				<th><h3>Name</h3></th>
				<th><h3>Specialization</h3></th>
				<th><h3>Educator</h3></th>
				<th><h3>Numbers of students</h3></th>
				<th><h3>Info</h3></th>
				<th><h3>Operations</h3></th>
			</tr>
			<tr>

				<td colspan="6"><h3>Actual Settings</h3></td>
			</tr>
			<tr>

				<td><h4>${schoolClassView.schoolClass.name}</h4></td>
				<td><h4>${schoolClassView.schoolClass.specialization}</h4></td>
				<td><h4>${schoolClassView.educator.name}&nbsp;
						${schoolClassView.educator.surname}</h4></td>
				<td><h4>${schoolClassView.numberOfStudents}</h4></td>
				<td><h4>${schoolClassView.schoolClass.info}</h4></td>
				<td>
				<c:if test="${schoolClassView.educator.id!=null}">
					<h4>
						Educator
						<ul>
							<li><spring:url value="/secretariat/class/person/delete"
									var="deletePersonURL" /> <form:form method="POST"
									modelAttribute="personPOJO" action="${deletePersonURL}">
									<form:hidden path="idClass"
										value="${schoolClassView.schoolClass.id }" />
									<form:hidden value="${schoolClassView.educator.id}"
										path="idOldPerson" />
									<form:button type="submit">Delete</form:button>
								</form:form></li>
						</ul>
					</h4>
					</c:if>
				</td>



			</tr>
			<tr>

				<td colspan="6"><h3>New Settings</h3></td>
			</tr>
			<tr>
				<spring:url value="/secretariat/class/update" var="updateClassURL" />
				<form:form method="POST" modelAttribute="newSchoolClass"
					action="${updateClassURL}">

					<form:hidden value="${schoolClassView.schoolClass.id}" path="id" />
					<td><h4>
							<form:input value="${schoolClassView.schoolClass.name}"
								path="name" />
						</h4></td>
					<td><h4>
							<form:input value="${schoolClassView.schoolClass.specialization}"
								path="specialization" />
						</h4></td>



					<td><h4>${schoolClassView.educator.name}&nbsp;
							${schoolClassView.educator.surname}</h4></td>

					<td><h4>${schoolClassView.numberOfStudents}</h4></td>

					<td><h4>
							<form:textarea value="${schoolClassView.schoolClass.info}"
								path="info" />
						</h4></td>
					<td><h4>SchoolClass
							<ul>
								<li><form:button type="submit">Update</form:button></li>
							</ul>
						</h4></td>
				</form:form>





			</tr>

		</table>
	</div>



	<div style="float: right; margin-right: 50px;">
		<table border="6">
			<tr>
				<th><h4>Educator</h4></th>
				<th><h4>Operations</h4></th>
			</tr>
			<tr>

				<c:choose>
					<c:when test="${schoolClassView.educator.id==null}">
						<spring:url value="/secretariat/class/person/add"
								var="addFreeEducatorURL" /> <form:form method="POST"
								modelAttribute="personPOJO" action="${addFreeEducatorURL}">
								<form:hidden path="idClass"
									value="${schoolClassView.schoolClass.id }" />
								<td><form:select path="idNewPerson">
										<form:options items="${freeEducator}" />
									</form:select></td>
								<td><ul>
										<li><form:button type="submit">Add</form:button></li>
									</ul></td>
							</form:form>
					</c:when>
					<c:otherwise>
						<spring:url value="/secretariat/class/person/update"
							var="updateFreeEducatorURL" />
						<form:form method="POST" modelAttribute="personPOJO"
							action="${updateFreeEducatorURL}">
							<form:hidden path="idClass"
								value="${schoolClassView.schoolClass.id }" />
							<form:hidden path="idOldPerson"
								value="${schoolClassView.educator.id }" />
							<td><form:select path="idNewPerson">
									<form:options items="${freeEducator}" />
								</form:select></td>
							<td>
								<ul>
									<li><form:button type="submit">Update</form:button></li>
								</ul>
							</td>
						</form:form>


					</c:otherwise>
				</c:choose>

			</tr>
			<tr>
				<th><h4>Students</h4></th>
				<th><h4>Operations</h4></th>
			</tr>
			<tr>
				<spring:url value="/secretariat/class/person/add"
					var="addFreeStudentsURL" />
				<form:form method="POST" modelAttribute="personPOJO"
					action="${addFreeStudentsURL}">

					<form:hidden path="idClass"
						value="${schoolClassView.schoolClass.id }" />
					<td><form:select path="idNewPerson">
							<form:options items="${freeStudents}" />
						</form:select></td>
					<td><form:button type="submit">Add</form:button></td>
				</form:form>


				<c:forEach var="student" items="${listOfStudents}">
					<tr>
						<td>${student.surname}&nbsp;${student.name}</td>
						<td><h4>
								<spring:url value="/secretariat/class/person/delete"
									var="deleteStudentURL" />
								<form:form method="POST" modelAttribute="personPOJO"
									action="${deleteStudentURL}">
									<form:hidden path="idClass"
										value="${schoolClassView.schoolClass.id }" />
									<form:hidden value="${student.id}" path="idOldPerson" />
									<form:button type="submit">Delete</form:button>
								</form:form>
							</h4></td>
					</tr>
				</c:forEach>
		</table>
	</div>
	<br clear="all">
</body>
</html>