<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Manager</title>
</head>
<body>

	<h1>${login}</h1>
	<h1>Edit PersonalData</h1>
	<spring:url value="/" var="homeAction" />
	<c:url value="/logout" var="logoutUrl" />
	<ul>
		<li><h2>
				<a href="${homeAction}">Home</a>
			</h2></li>
		<li>
			<form action="${logoutUrl}" method="post" id="logoutForm">
				<input type="submit" value="Logout"></input>
			</form>
		</li>
	</ul>

	<table border="6">
		<tr>
			<th><h3>Id</h3></th>
			<th><h3>Name</h3></th>
			<th><h3>Surname</h3></th>
			<th><h3>E-mail</h3></th>
			<th><h3>Status</h3></th>
			<th><h3>Operations</h3></th>
		</tr>
		<tr>

			<td colspan="6"><h3>Actual Settings</h3></td>
		</tr>
		<tr>
			<td><h4>${personalData.id}</h4></td>
			<td><h4>${personalData.name}</h4></td>
			<td><h4>${personalData.surname}</h4></td>
			<td><h4>${personalData.email}</h4></td>
			<td><c:choose>
					<c:when test="${personalData.personalStatus!=null}">
						<h4>${personalData.personalStatus.status}</h4>
					</c:when>
					<c:otherwise>
						<h4>NULL</h4>
					</c:otherwise>
				</c:choose></td>

		</tr>
		<tr>

			<td colspan="6"><h3>New Settings</h3></td>
		</tr>
		<spring:url value="/secretariat/personalData/edit"
			var="editPersonalDataURL" />
		<form:form method="POST" modelAttribute="newPersonalData"
			action="${editPersonalDataURL}">
			<tr>
				<td><h4>
						<form:hidden path="id" value="${personalData.id}" />
						${personalData.id}
					</h4></td>
				<td><h4>
						<form:input value="${personalData.name}" path="name" />
					</h4></td>
				<td><h4>
						<form:input value="${personalData.surname}" path="surname" />
					</h4></td>
				<td><h4>
						<form:input value="${personalData.email}" path="email" />
					</h4></td>

				<td>
					<h4>
						<form:select path="personalStatus.id">

							<form:options items="${personalStatusMap}" />

						</form:select>
					</h4>

				</td>
				<td><h4>
						<form:button type="submit">Update</form:button>

					</h4></td>
			<tr>
		</form:form>

	</table>

</body>
</html>