
create sequence role_seq;
INSERT INTO role(id, status) values (0,'ROLE_SUPERADMIN');
INSERT INTO role(id, status) values (nextval('role_seq'),'ROLE_ADMIN');
INSERT INTO role(id, status) values (nextval('role_seq'),'ROLE_DIRECTOR');
INSERT INTO role(id, status) values (nextval('role_seq'),'ROLE_SECRETARIAT');
INSERT INTO role(id, status) values (nextval('role_seq'),'ROLE_TEACHER');
INSERT INTO role(id, status) values (nextval('role_seq'),'ROLE_STUDENT');

create sequence subject_seq;
INSERT INTO subject(id, name) values (nextval('subject_seq'),'Physics games');
INSERT INTO subject(id, name) values (nextval('subject_seq'),'Algorithms');
INSERT INTO subject(id, name) values (nextval('subject_seq'),'Programming languages');
INSERT INTO subject(id, name) values (nextval('subject_seq'),'Discrete Mathematics');

create sequence personal_status_seq;
INSERT INTO personal_status(id, status) values (1,'OFFICE WORKER');
INSERT INTO personal_status(id, status) values (2,'TEACHER');
INSERT INTO personal_status(id, status) values (3,'STUDENT');

create sequence type_of_grade_seq;
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Answer1');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Answer2');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Answer3');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Answer4');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Test1');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Test2');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Test3');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Test4');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Project1');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Project2');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Project3');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Project4');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Egzam1');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Egzam2');
INSERT INTO type_of_grade(id, type_grade) values (nextval('type_of_grade_seq'),'Egzam3');

create sequence school_class_seq;
INSERT INTO school_class(id, name, specialization, info) values (nextval('school_class_seq'),'1A', 'Level Design','Max 30 Students' );
INSERT INTO school_class(id, name, specialization, info) values (nextval('school_class_seq'),'1B', 'Engine Programing', 'Info');
INSERT INTO school_class(id, name, specialization, info) values (nextval('school_class_seq'),'1C', 'Animation', '');


create sequence personal_seq;
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Adam','Teacher','adam_Zelenka@sga.com',2,2);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Piotr','Teresiak','piotr_teresiaka@sga.com',2,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Kamil','Teacher','ctsg@gildiagraczy.pl',2,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Bartek','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Slawek','Teacher','adam_Zelenka@sga.com',2,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Lukasz','Teacher','adam_Zelenka@sga.com',2,3);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Ania','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Marta','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Julka','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Michal','Student','adam_Zelenka@sga.com',3,2);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Przemek','Student','adam_Zelenka@sga.com',3,2);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Maciej','Student','adam_Zelenka@sga.com',3,3);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Patrycja','Student','adam_Zelenka@sga.com',3,3);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Ewa','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Marcin','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Krzysztof','Student','adam_Zelenka@sga.com',3,2);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Aneta','Student','adam_Zelenka@sga.com',3,2);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Krystian','Student','adam_Zelenka@sga.com',3,3);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Pawel','Office','adam_Zelenka@sga.com',1,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Cezary','Student','adam_Zelenka@sga.com',3,1);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Paula','Teacher','adam_Zelenka@sga.com',2,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Jadwiga','Student','adam_Zelenka@sga.com',3,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Natalia','Student','adam_Zelenka@sga.com',3,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Marek','Student','adam_Zelenka@sga.com',3,null);
INSERT INTO personal_data(id, name, surname, email,status,class) values (nextval('personal_seq'),'Kamila','Student','adam_Zelenka@sga.com',3,3);


create sequence actual_subject_seq
INSERT INTO actual_subject(id, subject,teacher,class) values (nextval('actual_subject_seq'),1,2,1);
INSERT INTO actual_subject(id, subject,teacher,class) values (nextval('actual_subject_seq'),2,3,2);
INSERT INTO actual_subject(id, subject,teacher,class) values (nextval('actual_subject_seq'),3,3,3);
INSERT INTO actual_subject(id, subject,teacher,class) values (nextval('actual_subject_seq'),4,2,1);

create sequence account_seq
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'admin','god',true,0,null);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'admin2','god',true,1,2);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'dyrektor','dyrektor',true,2,null);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'sekretariat','sekretariat',true,3,null);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'piotr','piotr',true,4,2);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'nauczyciel2','nauczyciel2',true,4,3);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'nauczyciel3','nauczyciel3',true,4,1);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'student1','student1',true,5,4);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'student2','student2',false,5,4);
INSERT INTO account(id, login, password, active_account, status,person) values(nextval('account_seq'),'student3','student3',true,5,4);


commit;