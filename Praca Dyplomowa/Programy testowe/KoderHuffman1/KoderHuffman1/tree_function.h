#pragma once
#include <fstream>
#include "tree.h"
using namespace std;

void wczytanie_z_pliku(fstream&, leaf** ,int);

void test_liczb(int , float );

void sortowanie_galezi(branch **, int );

void budowa_galezi(branch** , int);

void zerowanie_tablic(branch** , leaf** ,int  );

void zapis_tab_huffmana_do_pliku(leaf** ,int ,string ="tablica_huffmana.txt");

void budowa_drzewa(leaf** , branch** ,int );

;