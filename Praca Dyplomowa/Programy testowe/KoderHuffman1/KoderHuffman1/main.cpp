#include <iostream>
using namespace std;
#include <fstream>
#include <string>
#include "tree.h"
#include "tree_function.h"
//--------------------------------------------------------------

//------------------------------------------------------------

int leaf::licznik=0;
int branch::licznik=0;

//**********************************************************
//            M A I N . C P P
//*************************************************************
int main()
{
	char znak='0';
	const int wielkosc_bufora=10000;
	leaf *lista_lisci[wielkosc_bufora];	
	branch *lista_galezi[wielkosc_bufora];
//inicjalizacja tablic

zerowanie_tablic(lista_galezi,lista_lisci,wielkosc_bufora);



//obsluga pliku-----------------------------------
fstream file_in( "dane_wej.txt",ios::in);

int index=0;

if(file_in.good())
	{
wczytanie_z_pliku(file_in, lista_lisci,wielkosc_bufora);

//-----------------------------------------------
//wyswietlenie zawartosci
while (! lista_lisci[index]==0 && index<wielkosc_bufora)
{
	lista_lisci[index]->pokaz_zawartosc();
	index++;
}

// wartosci wczytane <liscie> teraz przypisanie liscia do galezi, a potem z galezi budowa drzewa

budowa_drzewa(lista_lisci, lista_galezi, wielkosc_bufora);

zapis_tab_huffmana_do_pliku(lista_lisci,wielkosc_bufora);
zapis_tab_huffmana_do_pliku(lista_lisci,wielkosc_bufora,"tablica_huffmana.ods");

}
else
{

cout<<"Brak pliku";
}

cout<<"\n\n";
cin>>znak;


return 0; 
}