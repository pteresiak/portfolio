#include "tree_function.h"
#include "tree.h"
#include <iostream>
using namespace std;

void wczytanie_z_pliku(fstream &file_in, leaf** lista_lisci,int wielkosc_bufora)
{
	float bufor1,bufor2,bufor0;
int licznik=0,index=0;
while(!file_in.eof())
{

	file_in>>bufor0;
	if(file_in.eof()) break;

	licznik++;
if(licznik%2==1)
{
bufor1=bufor0;
//cout<<bufor1<<"\n";
}
else
{
bufor2=bufor0;
//cout<<bufor2<<"\n";

lista_lisci[index]= new leaf(bufor1,bufor2);
index++;
if(index>=wielkosc_bufora)
{
cout<<"\n\n\t!! B U F O R    Z A      M A L Y !!\n\n";
file_in.flush();
file_in.close();
break;
}
}

}
file_in.flush();
file_in.close();


};

void test_liczb(int x, float b)
{
cout<<"\n\n******************************";
cout<<"\nWartosc int wynosi\t"<<x<<"\n";
cout<<"\nWartosc float wynosi\t"<<b<<"\n";
cout<<"********************************\n\n";

};

void sortowanie_galezi(branch **galezie, int wielkosc_tablicy)
{
	branch *temp;
	//sortowanie tab galezi na podstawie wartosci sumy procentow od najwyzszej do najmniejszej
	if(galezie[1]==0 || wielkosc_tablicy==1) {return;} // sprawdza czy jeden element w tablicy, jak tak to po co sortowac
	else
	{
	for(int i=0; i<wielkosc_tablicy-1; i++)
	{
			if (galezie[i]==0)
		{
			break;
		}
	for(int j=i+1; j<wielkosc_tablicy; j++)
	{
		if (galezie[j]==0)
		{
			break;
		}
		if(galezie[i]->pokaz_suma_proc() <galezie[j]->pokaz_suma_proc())
		{
		//jesli nastepna jest wieksza liczba zamien
		temp=galezie[i];
		galezie[i]=galezie[j];
		galezie[j]=temp;
		
		}
	
	}
	
	}
	}

};

void budowa_galezi(branch** galaz, int wielkosc_tab)
{
if(galaz[1]==0 || wielkosc_tab==1) {return;} // jesli nie ma galezi nie ma z czego budowac juz
//zalozenie ze tablica posortowana, szukam ostatniego najmniejszego elementu
int index=0;
while (!galaz[index]==0 && index<wielkosc_tab)
{
	//cout<<"adres galezi\t"<<galaz[index]<<"\t wartosc index\t"<<index<<"\n";
	index=index+1;
}

index--;
branch *temp= new branch(galaz[index],galaz[index-1]);

galaz[index]=0;
galaz[index-1]=temp;


//cout<<"\nIndex wynosi\t"<<index<<"\n\n";;



return;
};

void zerowanie_tablic(branch** galezie, leaf** liscie,int  wielkosc_tab)
{
for(int index=0; index<wielkosc_tab; index++)
{
galezie[index]=0;
liscie[index]=0;
}

};

void zapis_tab_huffmana_do_pliku(leaf** tab_liczb,int wielkosc_tab,string naza_pliku)
{
	fstream file_out(naza_pliku,ios::out);
	file_out<<"//Tablica kodow huffmana \t\tautor: Piotr Teresiak\n"
			<<"\n"
			<<"wartosc\t\t\twaga\t\t\tbinarnie\t\t\tkod\n"
			<<"       \t\t\t    \t\t\t        \t\t\t    \n";

	for(int i=0;i<wielkosc_tab;i++)
	{
		if(tab_liczb[i]==0){break;};
		tab_liczb[i]->podpiecie->licz_kod_huffmana();
		file_out<<tab_liczb[i]->wartosc_out()<<"\t\t\t";
		file_out<<tab_liczb[i]->procent_out()<<"\t\t\t";
		file_out<<"'"<<tab_liczb[i]->binarnie_to()<<"'"<<"\t\t\t";
		file_out<<"'"<<tab_liczb[i]->podpiecie->pokaz_kod()<<"'"<<"\n";
	
	}
	file_out.flush();
	file_out.close();

}

void budowa_drzewa(leaf** lista_lisci, branch** lista_galezi,int wielkosc_bufora)
{
int index=0;
while (!lista_lisci[index]==0 && index<wielkosc_bufora)
{
	lista_galezi[index]=new branch(lista_lisci[index]);
	index++;
}
index=0;
//Podpinanie galezi
while (!lista_galezi[1]==0)
{

//Sortuje
sortowanie_galezi(lista_galezi,wielkosc_bufora);

index=0;

budowa_galezi(lista_galezi,wielkosc_bufora);

cout<<".";
}


};