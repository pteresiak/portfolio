#include <fstream>
#include <string>
#include "kwantowanie.hpp"
#include "function.h"
#include <iostream>
#include <math.h>
#include "Histogram.h"
#include "metoda_wyboru_3_kwant.hpp"
#include "metoda_wyboru_5_kwant.hpp"
#include "metoda_wyboru_7_kwant.hpp"
#include "metoda_wyboru_9_kwant.hpp"
#include "metoda_wyboru_3_kwant2.hpp"
#include "metoda_wyboru_5_kwant2.hpp"
#include "metoda_wyboru_7_kwant2.hpp"
#include "metoda_wyboru_9_kwant2.hpp"
using namespace std;



int wybor_predykcji4(short *bufor_akt,short * bufor_poprzedni,const int wielkosc_bufora,int index,int *tabela_piorytet_pred,int nr_tabkwant=0)  //test od -10 do 79
{
int choose=2;


	if(index>=0&&index<wielkosc_bufora){
	

	//wpisywane do tabliocy
	short tab[4];
	if(index==0)
	{
		tab[1]=128; //gl
	}
	else
	{	tab[1]=bufor_poprzedni[index-1];
	}
	if(index==0){tab[0]=128;} //w
	else {tab[0]=bufor_akt[index-1];}
	tab[2]=bufor_poprzedni[index]; //g
	if(index!=wielkosc_bufora-1){tab[3]=bufor_poprzedni[index+1];} //gp
	else {tab[3]=128;};
	short gradA=tab[0]-tab[1];//wgl
	short gradB=tab[1]-tab[2];//glg
	short gradC=tab[2]-tab[3];//ggp
	short gradD=tab[0]-tab[2];//wg
	
	//int nr_kwant=9;	
	
	gradA=kwantowanie(gradA,nr_tabkwant);
	gradB=kwantowanie(gradB,nr_tabkwant);
	gradC=kwantowanie(gradC,nr_tabkwant);
	gradD=kwantowanie(gradD,nr_tabkwant);
	
	tabela_piorytet_pred[10];
	switch (nr_tabkwant)
	{
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:choose=funkcja_wybierajaca_pred_3_kwant(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 4:choose=funkcja_wybierajaca_pred_3_kwant2(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 5:choose=funkcja_wybierajaca_pred_5_kwant(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 6:choose=funkcja_wybierajaca_pred_5_kwant2(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 7:choose=funkcja_wybierajaca_pred_7_kwant(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 8:choose=funkcja_wybierajaca_pred_7_kwant2(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 9:choose=funkcja_wybierajaca_pred_9_kwant(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	case 10:choose=funkcja_wybierajaca_pred_9_kwant2(gradA,gradB,gradC,gradD,tabela_piorytet_pred);
		break;
	default:
		choose=1;
		break;
	}
	
	

		return choose;
}
		else
	{
	cout<<"\n\tPoza zakresem tablicy\n";
	return 0;
	}
}






void sortowanie_delty(MyClass **galezie, int wielkosc_tablicy)
{
	MyClass *temp;
	//sortowanie na podstawie moduly z delty od najmniejszej
	if(galezie[1]==0 || wielkosc_tablicy==1) {return;} // sprawdza czy jeden element w tablicy, jak tak to po co sortowac
	else
	{
	for(int i=0; i<wielkosc_tablicy-1; i++)
	{
			if (galezie[i]==0)
		{
			break;
		}
	for(int j=i+1; j<wielkosc_tablicy; j++)
	{
		if (galezie[j]==0)
		{
			break;
		}
		if(abs(galezie[i]->delta) >abs(galezie[j]->delta))
		{
		//jesli nastepna jest wieksza liczba zamien
		temp=galezie[i];
		galezie[i]=galezie[j];
		galezie[j]=temp;
		
		}
	
	}
	
	}
	}
	

};


int main()
{
	long int sumaryczny_blad_pred[10]={0};
	long int sumaryczny_blad_pred_abs[10]={0};
	string nazwa_tabel_kwant[11];
	nazwa_tabel_kwant[0]="brak tabeli kwantyzacji";
	nazwa_tabel_kwant[1]="brak tabeli kwantyzacji";
	nazwa_tabel_kwant[3]="tabela kwantyzacji [-128 0 128]";
	nazwa_tabel_kwant[4]="tabela kwantyzacji [-32 0 32]";
	nazwa_tabel_kwant[5]="tabela kwantyzacji [-128 -64 0 64 128 ]";
	nazwa_tabel_kwant[6]="tabela kwantyzacji [-32 -8 0 8 32]";
	nazwa_tabel_kwant[7]="tabela kwantyzacji [-128 -64 -32 0 32 64 128]";
	nazwa_tabel_kwant[8]="tabela kwantyzacji [-64 -32 -8 0 8 32 64]";
	nazwa_tabel_kwant[9]="tabela kwantyzacji [-128 -64 -32 -16 0 16 32 64 128]";
	nazwa_tabel_kwant[10]="tabela kwantyzacji [-64 -32 -16 -8 0 8 16 32 64]";
Histogram histo(-600,600);


//	for(int nr_predyktora=1; nr_predyktora<=10;nr_predyktora++)
//	{
/*	for(int nr_tab_kwantowej=8; nr_tab_kwantowej<=10;nr_tab_kwantowej++)
	/*{

	/*for(int offset=0; offset<=10;offset=offset+2)
	{
	*/

	const int ilosc_predyktorow=10;
	char beep=7;
	cout<<beep;
	
	histo.Czys_bufor();
cout<<"Wybor Predykcji\t\tAutor: Piotr Teresiak"
	<<"\n\n\tPodaj nazwe pliku do skompresowania\n\n:>\t";
string nazwa_file_in[6];

//fstream file_tab_wyb("tab_trafienie__pred_z_tab_kwant_"+to_string(nr_tab_kwantowej)+"_offset_"+to_string(offset)+".txt",ios::out );
fstream file_tab_wyb("tab_trafienie__5_pred_nalepszych.txt",ios::out );




//fstream file_if_text("if.txt",ios::out);
//fstream file_out_probki("probki.txt",ios::out );
//file_tab_wyb<<nazwa_tabel_kwant[nr_tab_kwantowej]<<"\n";
file_tab_wyb<<"test";
for(int zd=1;zd<ilosc_predyktorow+1;zd++)
{
file_tab_wyb<<"\tPred"<<zd;
}
file_tab_wyb<<"\t"<<"max_indesx"<<"\t"<<"wybor";
file_tab_wyb<<"\n";	
	
//for(int test=-10 ;test<80;test++) // wybor pred1
	//for(int test=-10 ;test<45;test++) // wybor pred2
//{

//wybor pakeitu plikow

nazwa_file_in[0]="Cactusorg.yuv_1_ramka.yuv";
nazwa_file_in[1]="station2org.yuv_1_ramka.yuv";
nazwa_file_in[2]="Tennisorg.yuv_1_ramka.yuv";
nazwa_file_in[3]="toys_and_calendarorg.yuv_1_ramka.yuv";
nazwa_file_in[4]="vintage_carorg.yuv_1_ramka.yuv";
nazwa_file_in[5]="test.yuv_1_ramka.yuv";

/*
nazwa_file_in[0]="Cactusorg.yuv";
nazwa_file_in[1]="station2org.yuv";
nazwa_file_in[2]="Tennisorg.yuv";
nazwa_file_in[3]="toys_and_calendarorg.yuv";
nazwa_file_in[4]="vintage_carorg.yuv";
nazwa_file_in[5]="test.yuv";
*/

const int szerokosc_bufora=1920;
const int ilosc_wierszy=1620;

char bufor[szerokosc_bufora];

	short *temp=0;
	 short bufor_przeliczony[szerokosc_bufora];
	 short bufor_przeliczony_poprzedni[szerokosc_bufora];

	short* wsk_bufor_przeliczony=bufor_przeliczony;
	short* wsk_bufor_przeliczony_poprzedni=bufor_przeliczony_poprzedni;
	int zlicz5mniej=0;
	int zlicz5=0;
	int zlicz10=0;
	int zlicz15=0;
	int zlicz20=0;
	int zlicz50=0;
	int zlicz100=0;
	int tabela_piorytet_pred[10];
	const 	int wiersze=ilosc_predyktorow+1;
	int tabela_modyfikator_zlicz[11]={0};

	//char* bufor_ramka=new char[szerokosc_bufora*ilosc_wierszy];

	int tabela_wynikow[wiersze][ilosc_predyktorow];
for(int i=0;i<wiersze;i++)
{
for( int j=0;j<ilosc_predyktorow;j++)
{
tabela_wynikow[i][j]=0;

}
}
//fstream *file_gradient= new fstream[ilosc_predyktorow];

/*for (int jkl=1;jkl<=ilosc_predyktorow;jkl++)
{
	file_gradient[jkl-1]=  fstream("Gradient Predyktora "+to_string(jkl)+".txt",ios::out | ios::binary);
	file_gradient[jkl-1]<<"Gradient Predyktora "+to_string(jkl)+"\n";
	file_gradient[jkl-1]<<"wgl \t glg \t ggp \t wg \n";


}*/


//po kazdym pliku
for( int z=0;z<5;z++)
{
fstream file_in(nazwa_file_in[z],ios::in| ios::binary );






if(file_in.good())
{








cout<<"\n\n\n\n\n";
		cout<<"\n_________________________________________________________________________";
		cout<<"\n|\tPrzetwarzam plik\t" +nazwa_file_in[z]+"\t\t|\n|\tmetoda\t\t\t";
		
	for(int c=0;c<szerokosc_bufora;c++){wsk_bufor_przeliczony_poprzedni[c]=128;}; // przygotowanie bufora
//for(int k=0; k<(1080+1080/2);k++)
	int wiersz_obrazi=0;
while(!file_in.eof())
{
	
			
		
		
	

		//cout<<"\n\nTest1\n\n";
	
	/*	file_in.read(bufor_ramka ,szerokosc_bufora*ilosc_wierszy);//odczyt danych po ramce
for(int ki=0; ki<ilosc_wierszy;ki++)
{
	
	for(int kz=0;kz<szerokosc_bufora;kz++)
	{
	bufor[kz]=bufor_ramka[kz+ki*1920];
	
	}
	*/
	file_in.read(bufor ,szerokosc_bufora);//odczyt danych po wierszu
wiersz_obrazi++;
	cout<<"\n wiersz obrazu\t"<<wiersz_obrazi<<endl;
	
		for(int i=0; i<szerokosc_bufora;i++)
		{
		wsk_bufor_przeliczony[i]=(bufor[i]<0?256+bufor[i]:bufor[i]);
		
		}
	//	cout<<"b ";
	/*	for(int index1=0;index1<szerokosc_bufora;index1++)
{

	file_out_probki<<wsk_bufor_przeliczony[index1]<<"\t";
		}
		file_out_probki<<"\n";
		*/
		
		for(int index1=0;index1<szerokosc_bufora;index1++)
{
/* do wyboru pred1
	short a,b,c;

	if(index1==0){a=0;}
	else{a=wsk_bufor_przeliczony_poprzedni[index1-1]-wsk_bufor_przeliczony[index1-1];};  //gl-w
	
	if(index1==0){b=wsk_bufor_przeliczony_poprzedni[index1]-128;}
	else{b=wsk_bufor_przeliczony_poprzedni[index1]-wsk_bufor_przeliczony_poprzedni[index1-1];} //g-gl

if(index1==szerokosc_bufora-1){c=128-wsk_bufor_przeliczony_poprzedni[index1];}
	else{c=wsk_bufor_przeliczony_poprzedni[index1+1]-wsk_bufor_przeliczony_poprzedni[index1];}//gp-g
	

int wybor=wybor_predykcji(a,b,c,test);
*/
	
//MyClass *wyniki_delty;
int modyfikator_predykcj=0;
//int wybor=wybor_predykcji4(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1,tabela_piorytet_pred,nr_tab_kwantowej);

//modyfikator_predykcj=modyfikator_predykcji(tabela_piorytet_pred,10,wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1,offset);
//tabela_modyfikator_zlicz[modyfikator_predykcj]++;
//wybor=tabela_piorytet_pred[modyfikator_predykcj];
int wybor=1;
//testowanie ktory pedyktor najlepszy
MyClass *wyniki_delty[ilosc_predyktorow];

//int wybor=nr_predyktora;

//cout<<"h ";
/*switch(wybor)
{
case 1:
wyniki_delty=new MyClass(1,wsk_bufor_przeliczony[index1]-Predyktor1(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//1
break;
case 2:
wyniki_delty=new MyClass(2,wsk_bufor_przeliczony[index1]-Predyktor2(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//2
break;
case 3:
wyniki_delty=new MyClass(3,wsk_bufor_przeliczony[index1]-Predyktor3(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//9
break;
case 4:
wyniki_delty=new MyClass(4,wsk_bufor_przeliczony[index1]-Predyktor4(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//15
break;
case 5:
wyniki_delty=new MyClass(5,wsk_bufor_przeliczony[index1]-Predyktor5(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//5

break;
case 6:
	wyniki_delty=new MyClass(6,wsk_bufor_przeliczony[index1]-Predyktor8(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 )); //19
break;
case 7:
	wyniki_delty=new MyClass(7,wsk_bufor_przeliczony[index1]-Predyktor19(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//18
break;
case 8:
	wyniki_delty=new MyClass(8,wsk_bufor_przeliczony[index1]-Predyktor22(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
break;
case 9:
	wyniki_delty=new MyClass(9,wsk_bufor_przeliczony[index1]-Predyktor23(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
break;
case 10:
	wyniki_delty=new MyClass(10,wsk_bufor_przeliczony[index1]-Predyktor24(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
break;

default:
	wyniki_delty=new MyClass(23,1000);
		
}*/

//cout<<"i ";

/*
//pred podobne z suma bledow pred
wyniki_delty[0]=new MyClass(1,wsk_bufor_przeliczony[index1]-Predyktor1(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[1]=new MyClass(2,wsk_bufor_przeliczony[index1]-Predyktor2(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[2]=new MyClass(3,wsk_bufor_przeliczony[index1]-Predyktor3(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[3]=new MyClass(4,4000);
wyniki_delty[4]=new MyClass(5,wsk_bufor_przeliczony[index1]-Predyktor5(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[5]=new MyClass(6,wsk_bufor_przeliczony[index1]-Predyktor8(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[6]=new MyClass(7,7000);
wyniki_delty[7]=new MyClass(8,8000);
wyniki_delty[8]=new MyClass(9,9000);
wyniki_delty[9]=new MyClass(10,1000);
*/
//pred podobne z najlepszym uzyciem
wyniki_delty[0]=new MyClass(1,wsk_bufor_przeliczony[index1]-Predyktor1(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[1]=new MyClass(2,wsk_bufor_przeliczony[index1]-Predyktor2(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[2]=new MyClass(3,wsk_bufor_przeliczony[index1]-Predyktor3(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2
wyniki_delty[3]=new MyClass(4,4000);
wyniki_delty[4]=new MyClass(5,5000);
wyniki_delty[5]=new MyClass(6,6000);
wyniki_delty[6]=new MyClass(7,7000);
wyniki_delty[7]=new MyClass(8,wsk_bufor_przeliczony[index1]-Predyktor22(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
wyniki_delty[8]=new MyClass(9,9000);
wyniki_delty[9]=new MyClass(10,wsk_bufor_przeliczony[index1]-Predyktor8(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2



//wybranepredyktory
//wyniki_delty[0]=new MyClass(1,wsk_bufor_przeliczony[index1]-Predyktor1(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//1
/*
wyniki_delty[1]=new MyClass(2,wsk_bufor_przeliczony[index1]-Predyktor2(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//2

wyniki_delty[2]=new MyClass(3,wsk_bufor_przeliczony[index1]-Predyktor3(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//9

wyniki_delty[3]=new MyClass(4,wsk_bufor_przeliczony[index1]-Predyktor4(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//15

wyniki_delty[4]=new MyClass(5,wsk_bufor_przeliczony[index1]-Predyktor5(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//5


	wyniki_delty[5]=new MyClass(6,wsk_bufor_przeliczony[index1]-Predyktor8(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1); //19

	wyniki_delty[6]=new MyClass(7,wsk_bufor_przeliczony[index1]-Predyktor19(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//18

	wyniki_delty[7]=new MyClass(8,wsk_bufor_przeliczony[index1]-Predyktor22(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//21

	wyniki_delty[8]=new MyClass(9,wsk_bufor_przeliczony[index1]-Predyktor23(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//21

	wyniki_delty[9]=new MyClass(10,wsk_bufor_przeliczony[index1]-Predyktor24(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ),wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1);//21

	//wyniki_delty[10]=new MyClass(11,wsk_bufor_przeliczony[index1]-Predyktor7(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	*/




/*
wyniki_delty[0]=new MyClass(1,wsk_bufor_przeliczony[index1]-Predyktor1(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//1

wyniki_delty[1]=new MyClass(2,wsk_bufor_przeliczony[index1]-Predyktor2(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//2

wyniki_delty[2]=new MyClass(3,wsk_bufor_przeliczony[index1]-Predyktor3(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//9

wyniki_delty[3]=new MyClass(4,wsk_bufor_przeliczony[index1]-Predyktor4(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//15

wyniki_delty[4]=new MyClass(5,wsk_bufor_przeliczony[index1]-Predyktor5(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//5


	wyniki_delty[5]=new MyClass(6,wsk_bufor_przeliczony[index1]-Predyktor6(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 )); //19

	wyniki_delty[6]=new MyClass(7,wsk_bufor_przeliczony[index1]-Predyktor7(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//18

	wyniki_delty[7]=new MyClass(8,wsk_bufor_przeliczony[index1]-Predyktor8(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[8]=new MyClass(9,wsk_bufor_przeliczony[index1]-Predyktor9(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[9]=new MyClass(10,wsk_bufor_przeliczony[index1]-Predyktor10(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[10]=new MyClass(11,wsk_bufor_przeliczony[index1]-Predyktor11(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[11]=new MyClass(12,wsk_bufor_przeliczony[index1]-Predyktor12(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[12]=new MyClass(13,wsk_bufor_przeliczony[index1]-Predyktor13(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[13]=new MyClass(14,wsk_bufor_przeliczony[index1]-Predyktor14(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21


	wyniki_delty[14]=new MyClass(15,wsk_bufor_przeliczony[index1]-Predyktor15(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[15]=new MyClass(16,wsk_bufor_przeliczony[index1]-Predyktor16(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[16]=new MyClass(17,wsk_bufor_przeliczony[index1]-Predyktor17(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[17]=new MyClass(18,wsk_bufor_przeliczony[index1]-Predyktor18(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[18]=new MyClass(19,wsk_bufor_przeliczony[index1]-Predyktor19(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[19]=new MyClass(20,wsk_bufor_przeliczony[index1]-Predyktor20(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[20]=new MyClass(21,wsk_bufor_przeliczony[index1]-Predyktor21(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21

	wyniki_delty[21]=new MyClass(22,wsk_bufor_przeliczony[index1]-Predyktor22(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
	wyniki_delty[22]=new MyClass(23,wsk_bufor_przeliczony[index1]-Predyktor23(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
	wyniki_delty[23]=new MyClass(24,wsk_bufor_przeliczony[index1]-Predyktor24(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
	wyniki_delty[24]=new MyClass(25,wsk_bufor_przeliczony[index1]-Predyktor25(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21
	*/
//wyniki_delty[7]=new MyClass(8,wsk_bufor_przeliczony[index1]-Predyktor21(wsk_bufor_przeliczony,wsk_bufor_przeliczony_poprzedni,szerokosc_bufora,index1 ));//21


/*
wyniki_delty[0]=new MyClass(1,wsk_bufor_przeliczony[index1]-0 );
wyniki_delty[1]=new MyClass(2,wsk_bufor_przeliczony[index1]-wsk_bufor_przeliczony[index1-1] );
wyniki_delty[2]=new MyClass(3,wsk_bufor_przeliczony[index1]-wsk_bufor_przeliczony_poprzedni[index1-1]);
wyniki_delty[3]=new MyClass(4,wsk_bufor_przeliczony[index1]-wsk_bufor_przeliczony_poprzedni[index1]) ;
wyniki_delty[4]=new MyClass(5,wsk_bufor_przeliczony[index1]-wsk_bufor_przeliczony_poprzedni[index1+1]);
wyniki_delty[5]=new MyClass(6,wsk_bufor_przeliczony[index1]-0);
wyniki_delty[6]=new MyClass(7,wsk_bufor_przeliczony[index1]-0);
wyniki_delty[7]=new MyClass(8,wsk_bufor_przeliczony[index1]-0 );
*/
//sortowanie
sortowanie_delty(wyniki_delty, ilosc_predyktorow);
int index2;
//tabela wynikow

if(abs(wyniki_delty[0]->delta)>histo.show_Zakres_max()) {cout<<"\n delta po za zakresem";
cout<<wyniki_delty[0]->nr_predyktora<<"\t"<<wyniki_delty[0]->delta;}
else { index2=wyniki_delty[0]->delta;
		index2=index2-histo.show_Zakres_min();
		histo.bufor[index2]++;
		
//		sumaryczny_blad_pred[nr_predyktora-1]=sumaryczny_blad_pred[nr_predyktora-1]+wyniki_delty[0]->delta;
	//	sumaryczny_blad_pred_abs[nr_predyktora-1]=sumaryczny_blad_pred_abs[nr_predyktora-1]+abs(wyniki_delty[0]->delta);
}
//cout<<"ss ";
//histo.bufor[(int (wyniki_delty[0]->delta)+ histo.show_Zakres_min())]++;
if(abs(wyniki_delty[0]->delta)==0) zlicz5mniej++;
if(abs(wyniki_delty[0]->delta)>5) zlicz5++;
if(abs(wyniki_delty[0]->delta)>10) zlicz10++;
if(abs(wyniki_delty[0]->delta)>15) zlicz15++;
if(abs(wyniki_delty[0]->delta)>20) zlicz20++;
if(abs(wyniki_delty[0]->delta)>50) zlicz50++;
if(abs(wyniki_delty[0]->delta)>100) zlicz100++;

tabela_wynikow[wybor-1][(wyniki_delty[0]->nr_predyktora)-1]++;
		


//file_gradient[wyniki_delty[0]->nr_predyktora-1]<<wyniki_delty[0]->wgl<<"\t"<<wyniki_delty[0]->glg<<"\t"<<wyniki_delty[0]->ggp<<"\t"<<wyniki_delty[0]->wg<<endl;

	
	//usuniecie 
//delete wyniki_delty;	

for (int i=0;i<ilosc_predyktorow;i++)
{
	 delete wyniki_delty[i];
}
		}
		






temp=wsk_bufor_przeliczony;
wsk_bufor_przeliczony=wsk_bufor_przeliczony_poprzedni;
wsk_bufor_przeliczony_poprzedni=temp;		
		
		
		
		
		
		
		}
	


file_in.close();
}

else
{
cout<<"\nBrak pliku\t"<<nazwa_file_in[z];

}

}/*
	cout<<"Tabela wynikow wyboru predykcji\n\n";

	cout<<"\tP1\tP2\tP3\tP4\tP5\tP6\tP7\tP8\n\n";
	cout<<"\t--\t--\t--\t--\t--\t--\t--\t--\n\n";
	for(int ik=0;ik<21;ik++)
	{
	cout<<"p"<<ik+1<<"\t";
	for (int il=0;il<21;il++)
	{
	cout<<tabela_wynikow[ik][il]<<"\t";
		}

	//funkcja liczaca makx

	cout<<"\n\n";
	}*/
	cout<<"\n\n Ilsoc delty pon 5\t\t"<<zlicz5mniej<<"\n";
	cout<<"\n\n Ilsoc delty pow 5\t\t"<<zlicz5<<"\n";
	cout<<"\n\n Ilsoc delty pow 10\t\t"<<zlicz10<<"\n";
	cout<<"\n\n Ilsoc delty pow 15\t\t"<<zlicz15<<"\n";
	cout<<"\n\n Ilsoc delty pow 20\t\t"<<zlicz20<<"\n";
	cout<<"\n\n Ilsoc delty pow 50\t\t"<<zlicz50<<"\n";
	cout<<"\n\n Ilsoc delty pow 100\t\t"<<zlicz100<<"\n";
	if(file_tab_wyb.good())
{

/*
for (int jkl=1;jkl<ilosc_predyktorow;jkl++)
{
	file_gradient[jkl-1].close();



}
*/


/*
for(int dane=0;dane<50-2;dane++)
{
	
		
file_if_text<<"if(gradB>="<<tab_if[dane+2]<<"){\n";
	


//file_tab_wyb<<test;
for(int i=0;i<wiersze/50;i++)
{
//	file_tab_wyb<<test;
	file_tab_wyb<<"p"<<i+1;
	for(int k=0;k<21;k++)
{
file_tab_wyb<<"\t"<<tabela_wynikow[i+dane*50][k];
}
//poczatek pliku
*/
//if(/*dane+1>=3&&dane<wiersze-1&&*/i+1>=3&&i<wiersze/50){  
//file_if_text<<"\tif(gradC>="<<tab_if[i]<<"){choose=";
/*
}
int liczba=tabela_wynikow[i+dane*50][0];
int index_tab =0;

for(int z=0;z<21;z++)
{
if(tabela_wynikow[i+dane*50][z]>liczba)
{
liczba=tabela_wynikow[i+dane*50][z];
index_tab=z;
}

}


//dopisac index i reszte do pliku txt*/
//if(/*dane+1>=3&&dane<wiersze-1&&*+1>=3&&i<wiersze/50){ 
/*	file_if_text<<(index_tab+1)<<";}\n";}
file_tab_wyb<<"\t"<<(index_tab+1)<<"\t"<<"p"<<i+1;
	//fynk
file_tab_wyb<<"\n";
}*/
//if/*(dane+1>=3&&dane<wiersze-1*/){ 
//file_if_text<<"}\n\n";
//}
/*}*/
	}


//zapis tableki do pliku

for(int i=0;i<wiersze;i++)
{
file_tab_wyb<<"p"<<i+1;
for(int k=0;k<ilosc_predyktorow;k++)
{
file_tab_wyb<<"\t"<<tabela_wynikow[i][k];
}

int liczba=tabela_wynikow[i][0];
int index_tab =0;



for(int z=0;z<ilosc_predyktorow;z++)
{
if(tabela_wynikow[i][z]>liczba)
{
liczba=tabela_wynikow[i][z];
index_tab=z;
}
}
file_tab_wyb<<"\t"<<(index_tab+1)<<"\t"<<"p"<<i+1;
	
file_tab_wyb<<"\n";

}
/*
fstream file_histo_offset("histogram_offset__z_tab_kwant_"+to_string(nr_tab_kwantowej)+"_offset_"+to_string(offset)+".txt",ios::out );

if(file_histo_offset.good())
{
	file_histo_offset<<nazwa_tabel_kwant[nr_tab_kwantowej]<<"\n";
	file_histo_offset<<"offset\tilosc\n";
	for(int i=0;i<10;i++)
	{ 
	file_histo_offset<<i<<"\t"<<tabela_modyfikator_zlicz[i]<<endl;
	}
	file_histo_offset.close();
}
*/

//fstream file_histo("histogram__pred_5_suma_bledu"+to_string(nr_tab_kwantowej)+"_offset_"+to_string(offset)+".txt",ios::out );
fstream file_histo("histogram__pred_5_najlepsze_uzycie.txt",ios::out );

if(file_histo.good())
{
//file_histo<<nazwa_tabel_kwant[nr_tab_kwantowej]<<"\n";
file_histo<<"lp\tindex\tilsoc\t\n";
for(int i=0;i<histo.wielkosc_bufora;i++)
{
file_histo<<i<<"\t"<<(i+histo.show_Zakres_min())<<"\t"<<histo.bufor[i]<<"\n";
}
}
file_histo.flush();
file_histo.close();

//histo.Pokaz_bufor();
	//file_out_probki.flush();
	//file_out_probki.close();
	file_tab_wyb.flush();
file_tab_wyb.close();
cout<<beep;


//}
//}

/*
fstream file_histo_offset("Suamryczny_blad_predykcji_dal_1_pred.txt",ios::out );

if(file_histo_offset.good())
{
	file_histo_offset<<"nr_pred\t suma_bledu \t suma_bledu_abs \n";
	for(int i=1;i<=10;i++)
	{ 
	file_histo_offset<<i<<"\t"<<sumaryczny_blad_pred[i-1]<<"\t"<<sumaryczny_blad_pred_abs[i-1]<<endl;
	}
	file_histo_offset.close();
}
*/









cout<<"\n\nKoniec\n";

char znak;
cin>>znak;

return 0;
}