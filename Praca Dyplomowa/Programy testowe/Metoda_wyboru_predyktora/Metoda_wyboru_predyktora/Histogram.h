#pragma once
#include <string>

class Histogram
{
private:
	int zakres_min;
	int zakres_max;
	long *ptr_bufor;
public:
	long *bufor;
	int wielkosc_bufora;
	Histogram(int , int);
	~Histogram(void);
	void Przywroc_wsk(void);
	void Czys_bufor(void);
	void Pokaz_bufor(void);
	long suma_buf(void);
	long max_element(void);
	int index_gdzie_max(void);
	std::string Naglowek(void);
	void zlicz(int);
	int show_Zakres_min(void){return zakres_min;};
	int show_Zakres_max(void){return zakres_max;};
};

