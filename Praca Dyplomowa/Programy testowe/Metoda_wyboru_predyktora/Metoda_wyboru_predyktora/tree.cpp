#include "tree.h"
#include <iostream>
#include <math.h>

void leaf::pokaz_zawartosc(void)
{
	std::cout<<"Nr klasy\t"<<ID_klasy<<"\tZmienne\t"<<wartosc<<"\t"<<procent<<"\n";


}


leaf::leaf(int index,float proc ): wartosc(index), procent(proc) 
{
	ID_klasy=licznik++;
	podpiecie=0;
	konwersja_wartosc_do_binary();
}


leaf::~leaf(void)
{



}



void leaf::konwersja_wartosc_do_binary(void)
{
	//cout<<"\nweszlem1\t"<<Id_binernie_ze_znakiem;
int temp=abs(wartosc);
if(wartosc<0){
wartosc_binernie_ze_znakiem='1';
}
else
{
wartosc_binernie_ze_znakiem='0';
}
do
{
	//cout<<"\nweszlem2\t"<<Id_binernie_ze_znakiem;
wartosc_binernie_ze_znakiem+=('0'+(temp&1));

}while(temp>>=1);
//cout<<"\nweszlem3\t"<<Id_binernie_ze_znakiem;
};

	void leaf::korekta_escape_praw(string ELSE)
		{
			if(this->wartosc_out()<=1/pow(2,9)) {podpiecie->ustaw_kod_huffmana(ELSE);};
		
		};





//--------------------------------------CLASS branch -------------------------------------------
	branch::branch(leaf *listek):  lisc(listek)
	{
ID_klasy=licznik++;
	lisc->podpiecie=this;
	lewy=0;
	prawy=0;
	gora=0;
	kod_huffmana=" ";
	suma_procent=(lisc->procent_out());
		
	}

	branch::branch(branch*l,branch* p) : lewy(l), prawy(p)
	{
		ID_klasy=licznik++;;
	suma_procent=(lewy->pokaz_suma_proc()) + (prawy->pokaz_suma_proc());
	lewy->gora=this;
	prawy->gora=this;
	gora=0;
	lisc=0;
	//ustalanie wag dzieci lewy = 0 prawy = 1;
	lewy->waga_wezla='0';
	prawy->waga_wezla='1';
	kod_huffmana=" ";

	
	};



	branch::~branch(void)
	{

	};

	void branch::spacer_do_gory(void)
	{
		if(gora==0){
			std::cout<<"RootID\t"<<this->pokaz_ID();
			return;};
		gora->spacer_do_gory();
		std::cout<<"\t->\t"<<this->pokaz_ID();

		return;
	
	};


		int branch::spacer_do_gory_licznik(void)
	{
		int licznik=0;
		if(gora==0){
			//std::cout<<"RootID\t"<<this->pokaz_ID();
			return 1;};
		licznik=gora->spacer_do_gory_licznik()+1;
		return licznik;


	
	};

		void branch::licz_kod_huffmana(void)
		{
		if(this->gora==0)return ;
		gora->licz_kod_huffmana();
		kod_huffmana=gora->kod_huffmana+waga_wezla;
		return ;


		
		
		}

		void branch::korekta_kodu()
{
	string temp_kod;
		int i=0;

	while (!kod_huffmana[i]==0)
	{
		if(kod_huffmana[i]=='1') {temp_kod+='1';};
		if(kod_huffmana[i]=='0') {temp_kod+='0';};
	i++;
	}
	kod_huffmana=temp_kod;
}

		void branch::korekta_escape(string esc)
		{
		if(kod_huffmana.length()>18) kod_huffmana=esc;
		
		};