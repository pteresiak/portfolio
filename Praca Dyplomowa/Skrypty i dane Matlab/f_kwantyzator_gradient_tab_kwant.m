%funkcja do kwantyzacji gradientow
function y=f_kwantyzator_gradient_tab_kwanty(x, tab_kwant);


dlugosc_tab=length(tab_kwant);

for i=1:dlugosc_tab
  
    if tab_kwant(i)<0
       if x<=tab_kwant(i)
           y=tab_kwant(i);
           break;
       end
    elseif i==dlugosc_tab
            y=tab_kwant(dlugosc_tab);
            break
    elseif x<tab_kwant(i+1)
                y=tab_kwant(i);
                break;
    end
        
end


end
    
