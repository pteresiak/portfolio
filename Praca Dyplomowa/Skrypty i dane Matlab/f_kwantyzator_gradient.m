%funkcja do kwantyzacji gradientow
function y=f_kwantyzator_gradient(x);


if (x<=-128)
  y=-128;
elseif (x<=-64)
    y=-64;
% elseif (x<=-32)
%     y=-32;
% elseif (x<=-16)
%     y=-16;
% elseif (x<16)
%     y=0;
% elseif (x<32)
%     y=16;
elseif (x<64)
%     y=32;
y=0;
elseif (x<128)
    y=64;
else
    y=128;
end
