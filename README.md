#Krótki opis projektów:#

##Java##
Nazwa projektu: 
### SchoolPage ###

**Technologie:**

* HTML/CSS
* Java EE
* Spring Framework(MVC, Security)
* Hibernate
* JSTL
* JSP
* Maven

**Opis:**

* Portal szkolny z elektronicznym dziennikiem ucznia. 
Strona wyświetla ogólne informacje z aktualnościami z bazy danych (przykładowa Baza Danych tworzona w pamięci).  
Na stronie jest opcja logowania z zabezpieczeniem "Spring Security"
Po zalogowaniu w zależności od konta dostępne są  różne możliwości.

***SUPERADMIN***

* Może wszytko przeglądać
* Nie można usunąć
* Nie można przypisać  konta do osoby
* Może przy tworzeniu nowego konta ustawić hasło
* Może dodawać  i usuwać  wszystkie konta oprócz swojego

***ADMIN***

* Może tylko zarządzać kontami
* Generuje hasła
* Wstawia nowe aktualności na stronę (w produkcji)
* Może dodawać  i usuwać  wszystkie konta oprócz swojego i SUPERADMIN

***DIRECTOR***

* Robi wszytko co SECRATARIAT
* Może generować raporty (w  produkcji)

***SECRATARIAT***

* Zarządza danymi osobowymi w bazie danych (dodaje , usuwa, edytuje, przegląda)
* Tworzy klasy szkolne
* Przydziela uczniów do klas oraz wychowawców
* Tworzy i przydziela nauczycieli do przedmiotów szkolnych

***TEACHER***

* Przegląda dane personalne uczniów jeśli jest ich wychowawcą
* Wystawia oceny dla uczniów z przedmiotu który prowadzi

***STUDENT***

* Może przejrzeć  swoje oceny(w produkcji)

-------------------------------------------------------------
##Java##
Nazwa projektu: 
### SpaceShip ###

**Technologie:**

* Java SE
* JavaFx
* Maven

**Opis:**

* Gra zręcznościowa gdzie sterujemy statkiem kosmicznym próbując uniknąć generowanych losowo Asteroidów.
Gracz posiada 5 żyć, po ominięciu asteroidy punkty są zliczane.
Gra posiada jeden nieskończony poziom
Gra się kończy kiedy Gracz straci wszystkie życia
Podczas rozgrywki towarzyszą nam efekty dźwiękowe
Projekt robiony zespołowo.

**Screenshot**

![SpaceShipo-screen.jpg](https://bitbucket.org/repo/kapXB6/images/1536751570-SpaceShipo-screen.jpg)

-------------------------------------------------------------

##C++##
Nazwa projektu: 
### Symulacja Cyfrowa ###

**Technologie:**

* C++

**Opis:**

* Program wykonany podczas studiów.
Program konsolowy który symuluję dany system sieci WLAN
W pliku "Raport.pdf" jest opisany treść zadania, pomiary i kod programu

-------------------------------------------------------------

##C++##
Nazwa projektu: 
### Arkanoid ###

**Technologie:**

* C++
* SFML

**Opis:**

* Gra zręcznościowa typu Arkanoid. Poziomy gry generowane automatycznie w zależności od poziomu gry. 
Bloczki mają różną wytrzymałość w zależności od koloru. Aktualny rekord zapisywany jest w pliku i po ponownym odpaleniu wczytywany jest do programu.

**Screenshot**

![Menu.jpg](https://bitbucket.org/repo/kapXB6/images/596680587-Menu.jpg)

-------------------------------------------------------------

## C# ##
Nazwa projektu: 
###Słuchometr###

**Technologie:**

* C#
* Windows Form

**Opis:**

* Aplikacja przygotowana jako projekt zaliczeniowy przedmiotu "Sygnały biomedyczne"
Projekt zespołowy. Projekt służy do badania słuchu ludzkiego.
Po odpaleniu programu podaje się dane pacjenta. Po wpisaniu poprawnych danych przechodzi się do programu głównego. 
Gdzie po rozpoczęciu testu, puszczane są tony o różnej częstotliwości i wysokości amplitudy. Użytkownik ma wcisnąć przycisk kiedy usłyszy dany dźwięk.
Po przeprowadzeniu testu jest wyrysowany wykres słuchu ludzkiego tzw audiogram. A po zakończeniu Aplikacji dane są zapisywane w odpowiednim katalogu.


**Screenshot**

![Słuchometr Screenshot.jpg](https://bitbucket.org/repo/kapXB6/images/3565302180-S%C5%82uchometr%20Screenshot.jpg)

-------------------------------------------------------------

## C - Atmega ##
Nazwa projektu: 
###Wyświetlacz LED 5x6 -ruchomy napis###

**Technologie:**

* C
* Atmega32

**Opis:**

* Zbudowany układ elektroniczny i zaprogramowany uControler Atmega32.
Program wyświetla ruchomy napis za pomocą diod LED 5x6 po podłączeniu do zasilania

**Screenshot**

![20160816_155139.jpg](https://bitbucket.org/repo/kapXB6/images/347657709-20160816_155139.jpg)

**Video**

[Film pokazujący działanie układu](https://youtu.be/offlg-cPGlY)