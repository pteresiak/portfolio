﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Formularz : Form
    {
        Formularz dane;
        public Formularz()
        {
            dane = this;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool flaga_blad_imienia = false;
            bool flaga_blad_nazwiska = false;
            bool flaga_blad_PESELu = false;

            for (int i = 0; i < textBoxImie.Text.Length; ++i)
            {
                if (!char.IsLetter(textBoxImie.Text.ToString(), i))
                {
                    flaga_blad_imienia = true;
                    break;
                }
            }

            for (int i = 0; i < textBoxNazwisko.Text.Length; ++i)
            {
                if (!char.IsLetter(textBoxNazwisko.Text.ToString(), i))
                {
                    flaga_blad_nazwiska = true;
                    break;
                }
            }

            for (int i = 0; i < textBoxPESEL.Text.Length; ++i)
            {
                if (!char.IsNumber(textBoxPESEL.Text.ToString(), i))
                {
                    flaga_blad_PESELu = true;
                    break;
                }
            }

            // SPRAWDZANIE - IMIE

            if (textBoxImie.Text.Length < 2) // CZY WPISANO IMIĘ
            {
                MessageBox.Show("Nieprawidłowo Wypełnione Imię", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (flaga_blad_imienia)
            {
                MessageBox.Show("Nieprawidłowo Wypełnione Imię", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // SPRAWDZANIE - NAZWISKO

            else if (textBoxNazwisko.Text.Length < 2) // CZY WPISANO NAZWISKO
            {
                MessageBox.Show("Nieprawidłowo Wypełnione Nazwisko", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (flaga_blad_nazwiska)
            {
                MessageBox.Show("Nieprawidłowo Wypełnione Nazwisko", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // SPRAWDZANIE - WIEK

            else if (textBoxWiek.Text.Length == 0/* || textBoxWiek.Text.Length > 3*/) // CZY WPISANO WIEK
            {
                MessageBox.Show("Nieprawidłowo Wypełniony Wiek", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (textBoxWiek.Text[0].ToString() == "0") // CZY WIEK NIE ZACZYNA SIĘ OD 0
            {
                MessageBox.Show("Nieprawidłowo Wypełniony Wiek", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (textBoxWiek.Text.Length == 1 && !char.IsNumber(textBoxWiek.Text.ToString(), 0)) // CZY LICZBA LAT MA W SOBIE TYLKO CYFRY
            {
                MessageBox.Show("Nieprawidłowo Wypełniony Wiek", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (textBoxWiek.Text.Length == 2 && (!char.IsNumber(textBoxWiek.Text.ToString(), 0) || // CZY LICZBA LAT MA W SOBIE TYLKO CYFRY
                                                      !char.IsNumber(textBoxWiek.Text.ToString(), 1))
                                                     )
            {
                MessageBox.Show("Nieprawidłowo Wypełniony Wiek", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (textBoxWiek.Text.Length == 3 && (!char.IsNumber(textBoxWiek.Text.ToString(), 0) || // CZY LICZBA LAT MA W SOBIE TYLKO CYFRY
                                                      !char.IsNumber(textBoxWiek.Text.ToString(), 1) ||
                                                      !char.IsNumber(textBoxWiek.Text.ToString(), 2))
                                                     )
            {
                MessageBox.Show("Nieprawidłowo Wypełniony Wiek", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            else if (Convert.ToInt32(textBoxWiek.Text.ToString()) > 160) // CZY NIE PODANO ZA DUZY WIEK
            {
                MessageBox.Show("No chyba jesteś za stary. I tak nic nie szłyszysz ;)", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
            // SPRAWDZANIE - PESEL

            else if (textBoxPESEL.Text.Length != 11) // CZY PESEL JEST NA PEWNO PESELEM -> MA 11 LICZB
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> ZŁA DŁUGOSC", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (flaga_blad_PESELu)
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> gdzieś litera", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (Convert.ToInt32(textBoxPESEL.Text[2].ToString() + textBoxPESEL.Text[3].ToString()) > 12 || // CZY PRAWIDLOWA LICZBA MIESIĄCA
                     Convert.ToInt32(textBoxPESEL.Text[2].ToString() + textBoxPESEL.Text[3].ToString()) < 1)
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> Zły miesiąc", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (Convert.ToInt32(textBoxPESEL.Text[4].ToString() + textBoxPESEL.Text[5].ToString()) < 1) // CZY LICZBA DNI NIE JEST 00
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> Zły dzień", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if ((Convert.ToInt32(textBoxPESEL.Text[2].ToString() + textBoxPESEL.Text[3].ToString()) % 2) == 0 && // MIESIĄCE PARZYSTE
                      Convert.ToInt32(textBoxPESEL.Text[4].ToString() + textBoxPESEL.Text[5].ToString()) > 30)
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> Zły dzień", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if ((Convert.ToInt32(textBoxPESEL.Text[2].ToString() + textBoxPESEL.Text[3].ToString()) % 2) != 0 && // MIESIĄCE NIEPARZYSTE
                      Convert.ToInt32(textBoxPESEL.Text[4].ToString() + textBoxPESEL.Text[5].ToString()) > 31)
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> Zły dzień", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (((Convert.ToInt32(textBoxPESEL.Text[0].ToString() + textBoxPESEL.Text[1].ToString()) % 4) == 0) && // ROK PRZESTĘPNY 
                       Convert.ToInt32(textBoxPESEL.Text[2].ToString() + textBoxPESEL.Text[3].ToString()) == 2 && // LUTY
                       Convert.ToInt32(textBoxPESEL.Text[4].ToString() + textBoxPESEL.Text[5].ToString()) > 29
                    )
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> Zły dzień", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else if (((Convert.ToInt32(textBoxPESEL.Text[0].ToString() + textBoxPESEL.Text[1].ToString()) % 4) != 0) && // ROK NIEPRZESTĘPNY 
                       Convert.ToInt32(textBoxPESEL.Text[2].ToString() + textBoxPESEL.Text[3].ToString()) == 2 && // LUTY
                       Convert.ToInt32(textBoxPESEL.Text[4].ToString() + textBoxPESEL.Text[5].ToString()) > 28
                    )
            {
                MessageBox.Show("Nieprawidłowo Wypełniony PESEL -> Zły dzień", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                Form1 okno = new Form1(this);
                okno.Owner = this;
                okno.Show();
                dane.Show();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Formularz_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBoxWiek_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\t\tSłuchometr®\n\n\n" +
                            "\tVersion:\t\t\t1.02 Beta\n\n" +
                            "\tSygnały Biomedyczne\t2013/14\n\n" +
                            "\tAksamit Filip\t\t93784\n" +
                            "\tTeresiak Piotr\t\t83468\n\n" +
                            "\tCreated By:\tFilip&Piotr Company©",
                            "O Programie",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dziękujemy za użytkowanie :)", "Koniec", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            this.Close();
        }

        private void textBoxPESEL_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBoxImie.Text = "";
            textBoxNazwisko.Text = "";
            textBoxPESEL.Text = "";
            textBoxWiek.Text = "";
        }
    }
}
