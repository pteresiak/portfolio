﻿namespace WindowsFormsApplication3
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Wyniki = new System.Windows.Forms.Button();
            this.OProgramie = new System.Windows.Forms.Button();
            this.Koniec = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Wyniki
            // 
            this.Wyniki.Location = new System.Drawing.Point(90, 125);
            this.Wyniki.Name = "Wyniki";
            this.Wyniki.Size = new System.Drawing.Size(105, 23);
            this.Wyniki.TabIndex = 0;
            this.Wyniki.Text = "Wyniki";
            this.Wyniki.UseVisualStyleBackColor = true;
            this.Wyniki.Visible = false;
            this.Wyniki.Click += new System.EventHandler(this.button1_Click);
            // 
            // OProgramie
            // 
            this.OProgramie.Location = new System.Drawing.Point(90, 198);
            this.OProgramie.Name = "OProgramie";
            this.OProgramie.Size = new System.Drawing.Size(105, 23);
            this.OProgramie.TabIndex = 1;
            this.OProgramie.Text = "O Programie";
            this.OProgramie.UseVisualStyleBackColor = true;
            this.OProgramie.Click += new System.EventHandler(this.OProgramie_Click);
            // 
            // Koniec
            // 
            this.Koniec.Location = new System.Drawing.Point(90, 227);
            this.Koniec.Name = "Koniec";
            this.Koniec.Size = new System.Drawing.Size(105, 23);
            this.Koniec.TabIndex = 2;
            this.Koniec.Text = "Zakończ";
            this.Koniec.UseVisualStyleBackColor = true;
            this.Koniec.Click += new System.EventHandler(this.Koniec_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(90, 96);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(105, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Nowy Pacjent";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.Koniec);
            this.Controls.Add(this.OProgramie);
            this.Controls.Add(this.Wyniki);
            this.Name = "Start";
            this.Text = "Słuchometr 1.0 Beta";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Wyniki;
        private System.Windows.Forms.Button OProgramie;
        private System.Windows.Forms.Button Koniec;
        private System.Windows.Forms.Button button4;
    }
}