﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// Ogólne informacje o zestawie są kontrolowane poprzez następujący 
// zbiór atrybutów. Zmień wartości tych atrybutów by zmodyfikować informacje
// powiązane z zestawem.
[assembly: AssemblyTitle("Słuchometr 1.02 Beta")]
[assembly: AssemblyDescription("PUT")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Filip&Piotr Company")]
[assembly: AssemblyProduct("Słuchometr")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("d^_^b")]
[assembly: AssemblyCulture("")]

// Ustawienie wartości ComVisible na false sprawia, że typy w tym zestawie nie będą widoczne 
// dla składników COM.  Jeśli potrzebny jest dostęp do typu w tym zestawie z 
// COM, ustaw wartość ComVisible na true, dla danego typu.
[assembly: ComVisible(false)]

// Następujący GUID jest dla ID typelib jeśli ten projekt jest dostępny dla COM
[assembly: Guid("a8bc281e-da9c-4bf2-9ade-60a5dfa35e71")]

// Informacje o wersji zestawu zawierają następujące cztery wartości:
//
//      Wersja główna
//      Wersja pomocnicza 
//      Numer kompilacji
//      Rewizja
//
// Można określać wszystkie wartości lub używać domyślnych numerów kompilacji i poprawki 
// poprzez użycie '*', jak pokazane jest poniżej:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("0.2.0.0")]
[assembly: NeutralResourcesLanguageAttribute("pl")]
