﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Start : Form
    {
        Start start;
        
        public Start()
        {
            InitializeComponent();
            start = this;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void OProgramie_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Version:\t\t1.0 Alpha\n\n\nCreated By:\tFilip&Piotr Company", "O Programie", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Koniec_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dziekujemy za uzytkowanie :)", "Koniec", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            start.Close();
        }
    }
}
