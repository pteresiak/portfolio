﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using NAudio.Wave;


namespace WindowsFormsApplication3
{    
    public partial class Form1 : Form
    {
        Formularz danePersonalne;

        public Form1(Formularz danePersonalne)
        {
            this.danePersonalne = danePersonalne;
            InitializeComponent();
            textBoxamp.Text = "0,2";
            danePersonalne.Hide();
            label6.Text = Convert.ToString(max_liczba_prob);
            labelImie.Text = danePersonalne.textBoxImie.Text;
            labelNazwisko.Text = danePersonalne.textBoxNazwisko.Text;
            labelWiek.Text = danePersonalne.textBoxWiek.Text;
            labelPESEL.Text = danePersonalne.textBoxPESEL.Text;
            dataczas = new System.DateTime(Convert.ToInt32(System.DateTime.Now.Year),
                                           Convert.ToInt32(System.DateTime.Now.Month),
                                           Convert.ToInt32(System.DateTime.Now.Day),
                                           Convert.ToInt32(System.DateTime.Now.Hour),
                                           Convert.ToInt32(System.DateTime.Now.Minute),
                                           Convert.ToInt32(System.DateTime.Now.Second));
            labelData.Text = dataczas.ToString("dd.MM.yyyy");
            labelGodzina.Text = dataczas.ToString("HH:mm:ss");
            Xn = X0;
            buttonStop.Enabled = false;
            label5.Visible = false;
            label6.Visible = false;
        }

        //----Maksymalna Liczba Prób----//
        const int max_liczba_prob = 20;
        
        int freq;
        int mincz = 10; // Minimalna częstotliwość
        int maxcz = 20000; // Maksymalna częstotliwość
        double amp;
        int liczba_wynikow = 0;
        
        string bufor;
       
        bool flagaPetla = true;
        bool flagaZakoncz = true;
        bool flagaPrzyciskNew = true;
        
        Wyniki[] tabWyniki = new Wyniki[max_liczba_prob];
        
        System.DateTime dataczas;
        
        private DirectSoundOut output = null;
        private BlockAlignReductionStream stream = null;

        //----Generator Równomierny----//

        double X0 = Convert.ToDouble(System.DateTime.Now.Millisecond); //jądro dla generatora
        double c = 12347;  // stała
        double m = (Math.Pow(2.0, 31) - 1);  // okres
        double a = 1103515245;  // mnożnik
        double Xn; // nastepna wartosc jadra

        private double Rownomierny(double min, double max)
        {
            Xn = (a * Xn + c) % m;

            return ((Xn / m) * (max - min) + min);
        }

        //----Sortowanie Tablicy Wynikow----//

        private void sort()
        {
            Wyniki temp=null;
            for (int i = 0; i < liczba_wynikow - 1; i++)
            {
                for (int j = i + 1; j < liczba_wynikow; j++)
                {
                    if (tabWyniki[i].frequency > tabWyniki[j].frequency)
                    {
                        temp = tabWyniki[i];
                        tabWyniki[i] = tabWyniki[j];
                        tabWyniki[j] = temp;
                    }
                }
            }
        }

        //----Wykresik----//

        private void button1_Click(object sender, EventArgs e)
        {
            chart1.ChartAreas["ChartArea1"].AxisX.Title = "Czas [s]";
            chart1.ChartAreas["ChartArea1"].AxisY.Title = "Amplituda";
            chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = false;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0.0;
            chart1.Series["Wynik"].Points.Clear();
            chart1.Series["Wynik"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            
            freq = Convert.ToInt32(textBoxfreq.Text);
            amp = Convert.ToDouble(textBoxamp.Text);
            chart1.ChartAreas["ChartArea1"].AxisX.Maximum =  (1.0 / (freq));

            for (int x =0; x <= 1000; x=x+1)
            {
                chart1.Series["Wynik"].Points.AddXY(x / (freq * 1000.0), amp * Math.Sin((x * 2.0 * Math.PI) / 1000));                
            }        
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {  
            if (output != null) output.Stop();

            freq = Convert.ToInt32(textBoxfreq.Text);
            amp = Convert.ToDouble(textBoxamp.Text);

            WaveTone tone = new WaveTone(freq, amp);
            stream = new BlockAlignReductionStream(tone);

            output = new DirectSoundOut();
            output.Init(stream);
            output.Play();

            MessageBox.Show("Odtwarzam dźwięk", "Dźwięk", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (output != null) output.Stop();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label5.Visible = true;
            label6.Visible = true;
            flagaZakoncz = true;
            
            buttonStart.Enabled = false;
            buttonWykresik.Enabled = false;
            buttonTone.Enabled = false;
            buttonKasuj.Enabled = false;
            buttonLosuj.Enabled = false;
            textBoxamp.Enabled = false;
            textBoxfreq.Enabled = false;
            buttonNew.Text = "Przerwij";
            flagaPrzyciskNew = false;
            buttonStop.Enabled = true;
            chart1.ChartAreas["ChartArea1"].AxisX.Title = "Częstotliwość [Hz]";
            chart1.ChartAreas["ChartArea1"].AxisY.Title = "Amplituda";
            
            if (output != null) output.Stop();
           
            do
            {
                MessageBox.Show("Kiedy usłyszysz dźwięk naciśnij przycisk STOP", "Info", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                label6.Text = Convert.ToString(max_liczba_prob);
                
                chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = false;

                liczba_wynikow = 0;
                for (int i = 0; i < max_liczba_prob; i++) tabWyniki[i] = null; // Czyszczenie Tablicy Wyników

                chart1.Series["Wynik"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
                chart1.Series["Wynik"].Points.Clear();
                chart1.ChartAreas["ChartArea1"].AxisX.Maximum = maxcz;
                chart1.ChartAreas["ChartArea1"].AxisX.Minimum = mincz;

                bufor = null;
                
                for (int nr_proba = 1; nr_proba <= max_liczba_prob; nr_proba++)
                {
                    freq = Convert.ToInt32(-5000 * Math.Log(Rownomierny(0, 1)));
                    if (freq > maxcz) freq = maxcz;
                    if (freq < mincz) freq = mincz;

                    textBoxfreq.Text = Convert.ToString(freq);
                    Random zegaramp = new Random(Convert.ToInt32(freq) + 
                                                 Convert.ToInt32(Math.Sin(System.DateTime.Now.Millisecond / freq) * 1000));
                    amp = zegaramp.Next(0, 200) * 0.001;
                    textBoxamp.Text = Convert.ToString(amp);
                    if (output != null) output.Stop();

                    freq = Convert.ToInt32(textBoxfreq.Text);
                    textBoxfreq.Text = Convert.ToString(freq);

                    flagaPetla = true;
                    System.Threading.Thread.Sleep(1000);

                    double amp_nastepny = 0;
                    amp = 0.00000001;

                    for (int x = 0; x <= 34; x++)
                    {
                        if (x == 0) amp_nastepny = (Math.Pow(1.5, x) - 0.999999999) / 1000000;
                        else amp_nastepny = (Math.Pow(1.5, x) - 1) / 1000000;
                            
                        textBoxamp.Text = Convert.ToString(amp_nastepny);
                        WaveTone tone = new WaveTone(freq, amp_nastepny);
                        stream = new BlockAlignReductionStream(tone);

                        output = new DirectSoundOut();
                        output.Init(stream);

                        output.Play();
                        System.Threading.Thread.Sleep(1000);    // Odtwarzanie Dźwięku o danej amplitudzie przez 1s
                        if (output != null) output.Stop();
                        System.Threading.Thread.Sleep(200);     // Czas między danymi wysokościami amplitudy
                            
                        Application.DoEvents();
                            
                        if (!flagaPetla) break;
                        if (!flagaZakoncz) return;
                        
                        labelwartosc.Text = "No";
                        amp = amp_nastepny;
                     }

                    if (labelwartosc.Text == "Yes") // Zapisywanie wyników do tablicy
                    {
                        tabWyniki[liczba_wynikow] = new Wyniki(freq, amp);
                        liczba_wynikow++;
                        
                        chart1.Series["Wynik"].Points.AddXY(freq, amp);
                    }
                    
                    chart1.Update();

                    label6.Text = Convert.ToString(max_liczba_prob-nr_proba);
                    
                    bufor = bufor + Convert.ToString(nr_proba) + 
                            "\t\t" + 
                            Convert.ToString(freq) + 
                            "\t\t" + 
                            Convert.ToString(amp) + 
                            "\t\t" + 
                            labelwartosc.Text + 
                            "\n";   
            }

            String pacjent = danePersonalne.textBoxPESEL.Text.ToString() +
                            "_" + 
                            danePersonalne.textBoxImie.Text.ToString() + 
                            "_" + 
                            danePersonalne.textBoxNazwisko.Text.ToString();
            
            String wyniki_koncowe = "Date:\t\t" +
                                    dataczas.ToString("dd.MM.yyyy") +
                                    "\nTime:\t\t" +
                                    dataczas.ToString("HH:mm:ss") +
                                    "\n\nName:\t\t" + 
                                    danePersonalne.textBoxImie.Text.ToString() + 
                                    "\nSurname:\t\t" + 
                                    danePersonalne.textBoxNazwisko.Text.ToString() + 
                                    "\nAge:\t\t" + 
                                    danePersonalne.textBoxWiek.Text.ToString() + 
                                    "\nPESEL:\t\t" + 
                                    danePersonalne.textBoxPESEL.Text.ToString() + 
                                    "\n\n" + 
                                    "No.\t\tFrequency\t\tAmplitude\t\tAnswer\n" +
                                    bufor;
           
            //----Katalogi I Pliki----//

            if (!Directory.Exists("Wyniki"))
                Directory.CreateDirectory("Wyniki");

            if (!Directory.Exists("Wyniki\\" + pacjent))
                Directory.CreateDirectory("Wyniki\\" + pacjent);

            //----Rysowanie Wykresu----//

            chart1.Series["Wynik"].Points.Clear();
            chart1.ChartAreas["ChartArea1"].AxisX.Maximum = maxcz;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = mincz;
            chart1.Series["Wynik"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            sort();

            for (int x = 0; x <liczba_wynikow; x++)
            {
                chart1.Series["Wynik"].Points.AddXY(tabWyniki[x].frequency, tabWyniki[x].amplitude);
            }

            if (liczba_wynikow > 0)
            {
                chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY.Title = "Poziom Natężenia Dźwięku [dB]";
            }

            chart1.ChartAreas["ChartArea1"].AxisY.IsLogarithmic = false;

            //----Zapisywanie Plików----//

            chart1.SaveImage("Wyniki\\" + 
                            pacjent + 
                            "\\" + 
                            dataczas.ToString("yyyy.MM.dd_HH.mm.ss") + 
                            ".png",
                            System.Drawing.Imaging.ImageFormat.Png);

            File.WriteAllText(@"Wyniki\\" + 
                             pacjent + 
                             "\\" + 
                             dataczas.ToString("yyyy.MM.dd_HH.mm.ss") + 
                             ".txt",
                             wyniki_koncowe);
            
            }
            while("Yes" ==  Convert.ToString(MessageBox.Show("Powtórzyć test?",
                                                            "Powtórzenie",
                                                            MessageBoxButtons.YesNo,
                                                            MessageBoxIcon.Question,
                                                            MessageBoxDefaultButton.Button2)));
            
            MessageBox.Show("Test zakończony", "Koniec", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            
            buttonStart.Enabled = false;
            buttonStop.Enabled = false;
            buttonWykresik.Enabled = true;
            buttonTone.Enabled = true;
            buttonKasuj.Enabled = true;
            buttonLosuj.Enabled = true;
            textBoxamp.Enabled = true;
            textBoxfreq.Enabled = true;
            
            flagaPrzyciskNew = true;
            
            label5.Visible = false;
            label6.Visible = false;
            
            buttonNew.Text = "Nowy Pacjent";
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Random zegarfq = new Random(System.DateTime.Now.Millisecond);
            textBoxfreq.Text = Convert.ToString(zegarfq.Next(0, 20000));
            Random zegaramp = new Random(2 * System.DateTime.Now.Millisecond + Convert.ToInt32(textBoxfreq.Text));           
            textBoxamp.Text = Convert.ToString(zegaramp.Next(0, 200) * 0.001);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            labelfreq.Visible = !labelfreq.Visible;
            labelamp.Visible = !labelamp.Visible;
            labelodp.Visible = !labelodp.Visible;
            labelwartosc.Visible = !labelwartosc.Visible;
            textBoxfreq.Visible = !textBoxfreq.Visible;
            textBoxamp.Visible = !textBoxamp.Visible;
            buttonLosuj.Visible = !buttonLosuj.Visible;
            buttonTone.Visible = !buttonTone.Visible;
            buttonWykresik.Visible = !buttonWykresik.Visible;
            buttonKasuj.Visible = !buttonKasuj.Visible;

        }

        private void buttonKasuj_Click(object sender, EventArgs e)
        {
            chart1.Series["Wynik"].Points.Clear();
        }

        private void labelImie_Click(object sender, EventArgs e)
        {

        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            flagaZakoncz = false;
            
            if (flagaPrzyciskNew)
            {
                
                danePersonalne.textBoxImie.Text = "";
                danePersonalne.textBoxNazwisko.Text = "";
                danePersonalne.textBoxPESEL.Text = "";
                danePersonalne.textBoxWiek.Text = "";
                if (output != null) output.Stop();
                
                this.Close();
            }
            
            else
            {   
                buttonNew.Text = "Nowy Pacjent";
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
                buttonTone.Enabled = true;
                buttonKasuj.Enabled = true;
                buttonLosuj.Enabled = true;
                buttonWykresik.Enabled = true;
                textBoxamp.Enabled = true;
                textBoxfreq.Enabled = true;

                label5.Visible = false;
                label6.Visible = false;
                
                flagaPrzyciskNew = true;
            }   
        }

        private void buttonPetla_Click(object sender, EventArgs e)
        {
            flagaZakoncz = false;
            buttonStop.Enabled = false;
            MessageBox.Show("Dziękujemy za użytkowanie :)", "Koniec", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            this.Close();
            danePersonalne.Close();   
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonStop_click(object sender, EventArgs e)
        {
            
        }

        private void buttonStop_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void buttonStop_Click_1(object sender, EventArgs e)
        {
            flagaPetla = false;
            labelwartosc.Text = "Yes";
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Challenge_Click(object sender, EventArgs e)
        {

        }
     }

    public class Wyniki
    {
        public int frequency;
        public double amplitude;

        public Wyniki(double fr, double am)
        {
            frequency = Convert.ToInt32(fr);
            amplitude = 10 * Math.Log10(Math.Pow(am,2)/Math.Pow(10,-12));
        }    
    };

     public class WaveTone : WaveStream
     {
        private double frequency;
        private double amplitude;
        private double time;

        public WaveTone(double f, double a)
        {
            this.time = 0;
            this.frequency = f;
            this.amplitude = a;
        }

        public override long Position
        {
            get;
            set;
        }

        public override long Length
        {
            get { return long.MaxValue; }
        }

        public override WaveFormat WaveFormat
        {
            get { return new WaveFormat(44100, 16, 1); }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int samples = count / 2;
            for (int i = 0; i < samples; i++)
            {
                double sine = amplitude * Math.Sin(Math.PI * 2 * frequency * time);
                time += 1.0 / 44100;
                short truncated = (short)Math.Round(sine * (Math.Pow(2, 15) - 1));
                buffer[i * 2] = (byte)(truncated & 0x00ff);
                buffer[i * 2 + 1] = (byte)((truncated & 0xff00) >> 8);
            }

            return count;
        }
    }
}

