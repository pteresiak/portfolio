﻿namespace WindowsFormsApplication3
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonWykresik = new System.Windows.Forms.Button();
            this.buttonTone = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.textBoxfreq = new System.Windows.Forms.TextBox();
            this.labelfreq = new System.Windows.Forms.Label();
            this.buttonLosuj = new System.Windows.Forms.Button();
            this.textBoxamp = new System.Windows.Forms.TextBox();
            this.labelamp = new System.Windows.Forms.Label();
            this.labelwartosc = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelodp = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelImie = new System.Windows.Forms.Label();
            this.labelNazwisko = new System.Windows.Forms.Label();
            this.labelWiek = new System.Windows.Forms.Label();
            this.labelPESEL = new System.Windows.Forms.Label();
            this.labelData = new System.Windows.Forms.Label();
            this.labelGodzina = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Challenge = new System.Windows.Forms.PictureBox();
            this.Zaawansowane = new System.Windows.Forms.CheckBox();
            this.buttonKasuj = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonPetla = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Challenge)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.AxisX.ArrowStyle = System.Windows.Forms.DataVisualization.Charting.AxisArrowStyle.Triangle;
            chartArea1.AxisX.MajorGrid.Interval = 1000D;
            chartArea1.AxisX.MinorGrid.Interval = 10D;
            chartArea1.AxisX.Title = "Częstotliwość [Hz]";
            chartArea1.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisY.Title = "Poziom Natężenia Dźwięku [dB]";
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(11, 11);
            this.chart1.Margin = new System.Windows.Forms.Padding(2);
            this.chart1.Name = "chart1";
            series1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDot;
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.Color = System.Drawing.Color.Green;
            series1.CustomProperties = "EmptyPointValue=Zero";
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series1.Name = "Wynik";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(788, 285);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // buttonWykresik
            // 
            this.buttonWykresik.Location = new System.Drawing.Point(315, 547);
            this.buttonWykresik.Margin = new System.Windows.Forms.Padding(2);
            this.buttonWykresik.Name = "buttonWykresik";
            this.buttonWykresik.Size = new System.Drawing.Size(110, 30);
            this.buttonWykresik.TabIndex = 10;
            this.buttonWykresik.Text = "Wykres Tonu";
            this.buttonWykresik.UseVisualStyleBackColor = true;
            this.buttonWykresik.Visible = false;
            this.buttonWykresik.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonTone
            // 
            this.buttonTone.Location = new System.Drawing.Point(451, 512);
            this.buttonTone.Margin = new System.Windows.Forms.Padding(2);
            this.buttonTone.Name = "buttonTone";
            this.buttonTone.Size = new System.Drawing.Size(110, 30);
            this.buttonTone.TabIndex = 9;
            this.buttonTone.Text = "Start Tonu";
            this.buttonTone.UseVisualStyleBackColor = true;
            this.buttonTone.Visible = false;
            this.buttonTone.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStart.Location = new System.Drawing.Point(17, 388);
            this.buttonStart.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(110, 50);
            this.buttonStart.TabIndex = 1;
            this.buttonStart.Text = "Rozpocznij Test";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBoxfreq
            // 
            this.textBoxfreq.Location = new System.Drawing.Point(99, 519);
            this.textBoxfreq.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxfreq.Name = "textBoxfreq";
            this.textBoxfreq.Size = new System.Drawing.Size(169, 20);
            this.textBoxfreq.TabIndex = 6;
            this.textBoxfreq.Text = "10";
            this.textBoxfreq.Visible = false;
            this.textBoxfreq.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelfreq
            // 
            this.labelfreq.AutoSize = true;
            this.labelfreq.Location = new System.Drawing.Point(23, 523);
            this.labelfreq.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelfreq.Name = "labelfreq";
            this.labelfreq.Size = new System.Drawing.Size(71, 13);
            this.labelfreq.TabIndex = 1;
            this.labelfreq.Text = "Częstotliwość";
            this.labelfreq.Visible = false;
            // 
            // buttonLosuj
            // 
            this.buttonLosuj.Location = new System.Drawing.Point(315, 511);
            this.buttonLosuj.Name = "buttonLosuj";
            this.buttonLosuj.Size = new System.Drawing.Size(110, 30);
            this.buttonLosuj.TabIndex = 8;
            this.buttonLosuj.Text = "Losuj";
            this.buttonLosuj.UseVisualStyleBackColor = true;
            this.buttonLosuj.Visible = false;
            this.buttonLosuj.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBoxamp
            // 
            this.textBoxamp.Location = new System.Drawing.Point(99, 552);
            this.textBoxamp.Name = "textBoxamp";
            this.textBoxamp.Size = new System.Drawing.Size(169, 20);
            this.textBoxamp.TabIndex = 7;
            this.textBoxamp.Visible = false;
            this.textBoxamp.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // labelamp
            // 
            this.labelamp.AutoSize = true;
            this.labelamp.Location = new System.Drawing.Point(23, 555);
            this.labelamp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelamp.Name = "labelamp";
            this.labelamp.Size = new System.Drawing.Size(53, 13);
            this.labelamp.TabIndex = 1;
            this.labelamp.Text = "Amplituda";
            this.labelamp.Visible = false;
            // 
            // labelwartosc
            // 
            this.labelwartosc.AutoSize = true;
            this.labelwartosc.Location = new System.Drawing.Point(187, 481);
            this.labelwartosc.Name = "labelwartosc";
            this.labelwartosc.Size = new System.Drawing.Size(0, 13);
            this.labelwartosc.TabIndex = 9;
            this.labelwartosc.Visible = false;
            this.labelwartosc.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(13, 340);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(217, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "Ilość pozostałych tonów:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(246, 314);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 55);
            this.label6.TabIndex = 1;
            this.label6.Text = "666";
            // 
            // labelodp
            // 
            this.labelodp.AutoSize = true;
            this.labelodp.Location = new System.Drawing.Point(23, 481);
            this.labelodp.Name = "labelodp";
            this.labelodp.Size = new System.Drawing.Size(108, 13);
            this.labelodp.TabIndex = 0;
            this.labelodp.Text = "Udzielona odpowiedz";
            this.labelodp.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(823, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 16);
            this.label8.TabIndex = 1;
            this.label8.Text = "Imię";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(823, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "Nazwisko";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(823, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Wiek";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(823, 182);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 16);
            this.label11.TabIndex = 1;
            this.label11.Text = "PESEL";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(823, 207);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 16);
            this.label12.TabIndex = 1;
            this.label12.Text = "Data";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(823, 232);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 16);
            this.label13.TabIndex = 1;
            this.label13.Text = "Godzina";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // labelImie
            // 
            this.labelImie.AutoSize = true;
            this.labelImie.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelImie.Location = new System.Drawing.Point(958, 107);
            this.labelImie.Name = "labelImie";
            this.labelImie.Size = new System.Drawing.Size(30, 16);
            this.labelImie.TabIndex = 1;
            this.labelImie.Text = "Jan";
            this.labelImie.Click += new System.EventHandler(this.labelImie_Click);
            // 
            // labelNazwisko
            // 
            this.labelNazwisko.AutoSize = true;
            this.labelNazwisko.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelNazwisko.Location = new System.Drawing.Point(958, 132);
            this.labelNazwisko.Name = "labelNazwisko";
            this.labelNazwisko.Size = new System.Drawing.Size(61, 16);
            this.labelNazwisko.TabIndex = 1;
            this.labelNazwisko.Text = "Kowalski";
            this.labelNazwisko.Click += new System.EventHandler(this.buttonStop_Click_1);
            // 
            // labelWiek
            // 
            this.labelWiek.AutoSize = true;
            this.labelWiek.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelWiek.Location = new System.Drawing.Point(958, 157);
            this.labelWiek.Name = "labelWiek";
            this.labelWiek.Size = new System.Drawing.Size(22, 16);
            this.labelWiek.TabIndex = 1;
            this.labelWiek.Text = "69";
            // 
            // labelPESEL
            // 
            this.labelPESEL.AutoSize = true;
            this.labelPESEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelPESEL.Location = new System.Drawing.Point(958, 182);
            this.labelPESEL.Name = "labelPESEL";
            this.labelPESEL.Size = new System.Drawing.Size(73, 16);
            this.labelPESEL.TabIndex = 1;
            this.labelPESEL.Text = "Szatan 666";
            this.labelPESEL.Click += new System.EventHandler(this.buttonStop_Click_1);
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelData.Location = new System.Drawing.Point(958, 207);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(56, 16);
            this.labelData.TabIndex = 1;
            this.labelData.Text = "13.13.13";
            this.labelData.Click += new System.EventHandler(this.buttonStop_click);
            // 
            // labelGodzina
            // 
            this.labelGodzina.AutoSize = true;
            this.labelGodzina.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGodzina.Location = new System.Drawing.Point(958, 232);
            this.labelGodzina.Name = "labelGodzina";
            this.labelGodzina.Size = new System.Drawing.Size(39, 16);
            this.labelGodzina.TabIndex = 1;
            this.labelGodzina.Text = "13:13";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(874, 64);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 24);
            this.label20.TabIndex = 1;
            this.label20.Text = "Pacjent";
            // 
            // Challenge
            // 
            this.Challenge.Image = ((System.Drawing.Image)(resources.GetObject("Challenge.Image")));
            this.Challenge.InitialImage = null;
            this.Challenge.Location = new System.Drawing.Point(640, 301);
            this.Challenge.Name = "Challenge";
            this.Challenge.Size = new System.Drawing.Size(441, 297);
            this.Challenge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Challenge.TabIndex = 28;
            this.Challenge.TabStop = false;
            this.Challenge.Click += new System.EventHandler(this.Challenge_Click);
            // 
            // Zaawansowane
            // 
            this.Zaawansowane.AutoSize = true;
            this.Zaawansowane.Location = new System.Drawing.Point(948, 12);
            this.Zaawansowane.Name = "Zaawansowane";
            this.Zaawansowane.Size = new System.Drawing.Size(133, 17);
            this.Zaawansowane.TabIndex = 5;
            this.Zaawansowane.Text = "Opcje Zaawansowane";
            this.Zaawansowane.UseVisualStyleBackColor = true;
            this.Zaawansowane.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // buttonKasuj
            // 
            this.buttonKasuj.Location = new System.Drawing.Point(451, 547);
            this.buttonKasuj.Name = "buttonKasuj";
            this.buttonKasuj.Size = new System.Drawing.Size(110, 30);
            this.buttonKasuj.TabIndex = 11;
            this.buttonKasuj.Text = "Czyść Wykres";
            this.buttonKasuj.UseVisualStyleBackColor = true;
            this.buttonKasuj.Visible = false;
            this.buttonKasuj.Click += new System.EventHandler(this.buttonKasuj_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonNew.Location = new System.Drawing.Point(406, 388);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(110, 50);
            this.buttonNew.TabIndex = 3;
            this.buttonNew.Text = "Nowy Pacjent";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonPetla
            // 
            this.buttonPetla.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonPetla.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPetla.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonPetla.ForeColor = System.Drawing.Color.DarkRed;
            this.buttonPetla.Location = new System.Drawing.Point(522, 388);
            this.buttonPetla.Name = "buttonPetla";
            this.buttonPetla.Size = new System.Drawing.Size(110, 50);
            this.buttonPetla.TabIndex = 4;
            this.buttonPetla.Text = "Zakończ";
            this.buttonPetla.UseVisualStyleBackColor = false;
            this.buttonPetla.Click += new System.EventHandler(this.buttonPetla_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStop.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonStop.Location = new System.Drawing.Point(132, 388);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(268, 50);
            this.buttonStop.TabIndex = 2;
            this.buttonStop.Text = "S T O P";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1093, 610);
            this.ControlBox = false;
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonPetla);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.buttonKasuj);
            this.Controls.Add(this.Zaawansowane);
            this.Controls.Add(this.Challenge);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.labelGodzina);
            this.Controls.Add(this.labelData);
            this.Controls.Add(this.labelPESEL);
            this.Controls.Add(this.labelWiek);
            this.Controls.Add(this.labelNazwisko);
            this.Controls.Add(this.labelImie);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelodp);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.labelwartosc);
            this.Controls.Add(this.textBoxamp);
            this.Controls.Add(this.buttonLosuj);
            this.Controls.Add(this.labelamp);
            this.Controls.Add(this.labelfreq);
            this.Controls.Add(this.textBoxfreq);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonTone);
            this.Controls.Add(this.buttonWykresik);
            this.Controls.Add(this.chart1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TransparencyKey = System.Drawing.Color.MintCream;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Challenge)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button buttonWykresik;
        private System.Windows.Forms.Button buttonTone;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.TextBox textBoxfreq;
        private System.Windows.Forms.Label labelfreq;
        private System.Windows.Forms.Button buttonLosuj;
        private System.Windows.Forms.TextBox textBoxamp;
        private System.Windows.Forms.Label labelamp;
        private System.Windows.Forms.Label labelwartosc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelodp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelImie;
        private System.Windows.Forms.Label labelNazwisko;
        private System.Windows.Forms.Label labelWiek;
        private System.Windows.Forms.Label labelPESEL;
        private System.Windows.Forms.Label labelData;
        private System.Windows.Forms.Label labelGodzina;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox Challenge;
        private System.Windows.Forms.CheckBox Zaawansowane;
        private System.Windows.Forms.Button buttonKasuj;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonPetla;
        private System.Windows.Forms.Button buttonStop;
    }
}

